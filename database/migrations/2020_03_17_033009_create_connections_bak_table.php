<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateConnectionsBakTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('connections_bak', function(Blueprint $table)
		{
			$table->integer('id')->unsigned()->default(0);
			$table->string('user_id');
			$table->string('phone');
			$table->string('seller_id');
			$table->string('package_id');
			$table->boolean('is_active')->default(0);
			$table->string('customer_id')->nullable();
			$table->string('invoice_id')->nullable();
			$table->dateTime('next_payment_date')->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->enum('status', array('active','ending','inactive'))->default('active');
			$table->decimal('price', 10, 3)->nullable();
			$table->boolean('notified')->nullable()->default(0);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('connections_bak');
	}

}
