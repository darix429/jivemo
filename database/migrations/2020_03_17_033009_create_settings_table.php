<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSettingsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('settings', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('page_title')->default('JiveMo');
			$table->string('page_description')->default('JiveMo');
			$table->string('page_metatag')->nullable();
			$table->string('copyright')->nullable();
			$table->string('logo')->default('/images/logo.png');
			$table->string('admin_logo')->default('/images/logo-admin.png');
			$table->decimal('msg_fee', 10, 3)->default(0.035);
			$table->decimal('connection_fee', 10, 3)->default(2.500);
			$table->string('admin_phone')->nullable();
			$table->string('admin_phone2')->nullable();
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('settings');
	}

}
