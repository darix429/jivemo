<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTransactionsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('transactions', function(Blueprint $table)
		{
			$table->increments('id');
			$table->integer('user_id')->unsigned()->index('transactions_user_id_foreign');
			$table->integer('foreign_id')->unsigned();
			$table->integer('transaction_type_id')->unsigned();
			$table->string('class');
			$table->float('amount');
			$table->text('description')->nullable();
			$table->text('admin_notes', 16777215)->nullable();
			$table->boolean('payout')->default(0);
			$table->timestamps();
			$table->softDeletes();
			$table->string('invoice_id')->nullable()->default('12');
			$table->integer('payout_id')->unsigned()->nullable()->default(12);
			$table->integer('connection_id')->unsigned()->nullable()->default(12);
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('transactions');
	}

}
