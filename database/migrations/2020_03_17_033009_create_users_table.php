<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('first_name')->nullable();
			$table->string('last_name')->nullable();
			$table->string('username');
			$table->string('tagline')->nullable();
			$table->string('email');
			$table->dateTime('email_verified_at')->nullable();
			$table->boolean('is_email_confirmed')->default(0);
			$table->boolean('is_active')->default(0);
			$table->string('password')->nullable();
			$table->text('about_me', 16777215)->nullable();
			$table->boolean('profile_privacy')->nullable()->default(1);
			$table->integer('content_category_id')->nullable();
			$table->text('talk_about', 16777215)->nullable();
			$table->string('profile_image')->nullable();
			$table->boolean('is_influencer')->default(0);
			$table->boolean('is_varified_influencer')->default(0);
			$table->string('signup_ip')->nullable();
			$table->string('last_login_ip')->nullable();
			$table->dateTime('last_logged_in_time')->nullable();
			$table->string('last_four')->nullable();
			$table->string('stripe_id')->nullable();
			$table->boolean('stripe_active')->default(0);
			$table->float('balance')->default(0.00);
			$table->string('remember_token', 100)->nullable();
			$table->timestamps();
			$table->softDeletes();
			$table->string('payout_method', 100)->nullable();
			$table->string('payout_email', 200)->nullable();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('users');
	}

}
