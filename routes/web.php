<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes(['verify' => true]);
Route::post("/registeruser", "Auth\RegisterController@registeruser")->name("register-user");
Route::post("/verifyuser", "Auth\RegisterController@verifyuser")->name("verify-user");
Route::post("/loginuser", "Auth\LoginController@loginuser")->name("login-user");
Route::post("/resend-code", "Auth\LoginController@sendVerificationCode")->name("resend-code");
Route::get("/admin-login", "Admin\AdminLoginController@index")->name("admin-login");

Route::get("/runtest", "Admin\AdminTestController@index")->name("stripe-details");
Route::group(['prefix' => 'admin',  'middleware' => 'admin'], function () {
    Route::get('/', 'AdminController@admin')->name('admin');
    Route::get('/setting', 'Admin\AdminSetting@index')->name('setting');
    Route::post('/setting', 'Admin\AdminSetting@update')->name('setting');

    //payout controller
    Route::get('/payout', 'Admin\PayoutController@index')->name('admin-payouts');
    Route::get('get-admin-payout', ['as'=>'get.payout','uses'=>'Admin\PayoutController@getData']);
    Route::get('/payout/create/{id}', 'Admin\PayoutController@create')->name('admin-payout-create');
    Route::post('/payout/store', 'Admin\PayoutController@store')->name('admin-payout-store');
    Route::get('/payout/edit2/{id}', 'Admin\PayoutController@edit')->name('admin-payout-edit');
    Route::post('/payout/edit/{id}', 'Admin\PayoutController@update')->name('admin-payout-update');
    Route::get('/payout/history', 'Admin\PayoutController@history')->name('admin-payouts-history');
    Route::get('/payout/info/{id}/{date}', 'Admin\PayoutController@info')->name('admin-payout-info');
    Route::get('/payout/history/info/{id}', 'Admin\PayoutController@historyInfo')->name('admin-payout-history-info');
    
    //admin user 
    Route::match(['get', 'post'],'/users', 'Admin\AdminUserController@index')->name('admin-users');
    //get influencer 
    Route::get('/users/influencer', 'Admin\AdminUserController@influencer')->name('admin-users-influencer');
    Route::get('get_influencer', ['as'=>'get.influencers','uses'=>'Admin\AdminUserController@getInfluencer']);

    //get buery  
    Route::get('/users/buyer', 'Admin\AdminUserController@buyer')->name('admin-users-buyer');
    Route::get('get_buyer', ['as'=>'get.buyer','uses'=>'Admin\AdminUserController@getBuyer']);
    
    Route::get('/users/{username}', 'Admin\AdminUserController@show')->name('admin-userdetails');
    Route::post('/users/{username}', 'Admin\AdminUserController@phoneUpdate')->name('admin-phone-update');
    Route::delete('/delete-user/{id}', 'Admin\AdminUserController@destroy')->name('admin-user-destroy');
    Route::delete('/users/{username}', 'Admin\AdminUserController@deletePhone')->name('admin-phone-delete');
    Route::get('get-data-my-datatables', ['as'=>'get.userlist','uses'=>'Admin\AdminUserController@getData']);

    //
    Route::get('/usersdetails/{id}', 'Admin\AdminUserController@userview')->name('admin-user-view');
    Route::get('/add-user/', 'Admin\AdminUserController@create')->name('admin-add-user');
    Route::post('/add-user/', 'Admin\AdminUserController@createPost')->name('add-admin-user');

    Route::post('/admin-sec/users/{id}', 'Admin\AdminUserController@update')->name('admin-user-update');

    //transactions log
    Route::get('/transactions', 'Admin\AdminTransactionController@index')->name('admin-transactions');
    Route::get('/transactions/create/{username}', 'Admin\AdminTransactionController@create')->name('admin-transaction-create');
    Route::post('/transactions/create/{username}', 'Admin\AdminTransactionController@store')->name('admin-transaction-create');
    Route::get('get-admin-transactions', ['as'=>'get.transactions','uses'=>'Admin\AdminTransactionController@getData']);
    //admin 
    Route::post('/users/package', 'AdminController@packageEdit')->name('package-edit');

    //packages
    Route::get('/packages', 'Admin\AdminPackageController@index')->name('admin-packages');
    Route::get('get-admin-packages', ['as'=>'get.packages','uses'=>'Admin\AdminPackageController@getData']);
    Route::get('/packages/{id}', 'Admin\AdminPackageController@show')->name('admin-package-show');

    Route::get('/packages/new/{id}', 'Admin\AdminPackageController@add_package')->name('admin-add-package');
    Route::post('/packages/new/', 'Admin\AdminPackageController@store')->name('admin-add-new-package');

    Route::post('/packages/{id}', 'Admin\AdminPackageController@update')->name('admin-package-update');
    Route::delete('/delete-package/{id}', 'Admin\AdminPackageController@destroy')->name('admin-package-destroy');


    //connections
    Route::get('/connections', 'Admin\AdminConnectionController@index')->name('admin-connections');

    //transactions log
    Route::get('/connections', 'Admin\AdminConnectionController@index')->name('admin-connections');
    // Route::delete('/connections', 'Admin\AdminConnectionController@destroy')->name('admin-connection-destroy');
    Route::get('get-admin-connections', ['as'=>'get.connections','uses'=>'Admin\AdminConnectionController@getData']);
    Route::delete('/connections/{id}/', 'Admin\AdminConnectionController@destroy')->name('admin-connection-destroy');
    Route::put('/connections/{id}/', 'Admin\AdminConnectionController@update')->name('admin-connection-status');

    //phone log
    Route::get('/phonelogs', 'Admin\AdminPhoneLogsConroller@index')->name('admin-phonelogs');
    Route::get('get-admin-phonelogs', ['as'=>'get.phonelogs','uses'=>'Admin\AdminPhoneLogsConroller@getData']);

    //influencer
    Route::get('/influencer-review', 'Admin\AdminInfluencerApplicationController@index')->name('admin-influencer-review');
    Route::get('get-admin-influencer', ['as'=>'get.influencer','uses'=>'Admin\AdminInfluencerApplicationController@getData']);
    Route::post('/influencer-review/{id}', 'Admin\AdminUserController@approveInfluencer')->name('admin-influencer-review-update');

    // templates
    Route::get('/templates', 'Admin\AdminTemplatesController@index')->name('templates');
    Route::get('/templates/new/sms', 'Admin\AdminTemplatesController@addSmsTemplate')->name('add-sms-template');
    Route::get('/templates/new/email', 'Admin\AdminTemplatesController@addEmailTemplate')->name('add-email-template');

    Route::get('/templates/edit-email/{id}', 'Admin\AdminTemplatesController@editEmailTemplate')->name('edit-email-template');
    Route::get('/templates/edit/{id}', 'Admin\AdminTemplatesController@editTemplate')->name('edit-template');

    Route::get('/templates/delete/{id}', 'Admin\AdminTemplatesController@deleteTemplate')->name('delete-template');
    Route::post('/templates', 'Admin\AdminTemplatesController@saveTemplate')->name('save-template');

    
    Route::get('/templates/send', 'Admin\AdminTemplatesController@sendTemplateView')->name('send-template-view');
    Route::post('/templates/send', 'Admin\AdminTemplatesController@sendTemplate')->name('send-template');
});

/** frontend */
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('{path}', 'HomeController@index')->where('path', '([A-z\-\d_\/.]+)?' );
