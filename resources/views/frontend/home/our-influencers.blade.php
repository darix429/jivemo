<!-- Our Influencers-->
<div class="jivemo-our-influcers">
    <div class="jivemo-our-influcers-outer-wrapper">
        <div class="influcers-container">
            <div class="jivemo-our-influcers-inner-wrapper">
                <div class="heading">
                    <h4>{{ __('Our Influencers') }}</h4>
                </div>
                <div class="row">
                        <?php
                        $ourInfulernser = DB::table('users')
                                                ->where('is_active', '=', 1)
                                                ->where('is_influencer', '=', 1)
                                                ->limit(4)->get(); ?>

                                                
                        @forelse ($ourInfulernser as $infulernser) 
                            //dd($infulernser);
                       
                        <div class="col-lg-3 col-md-6 col-sm-6 col-xs-12">
                            <div class="profile">
                                <div class="profile-img">
                                    @if ( empty($infulernser->profile_image) )
                                        <img src="images/user-icons.png" alt="profile">
                                    @else
                                        <img src="{{ Storage::url($infulernser->profile_image) }} " alt="profile">
                                    @endif
                                </div>
                                <div class="about-profile">
                                    <strong>{{ $infulernser->first_name }} {{ $infulernser->last_name }}</strong>
                                    <span>{{ $infulernser->tagline }}</span>
                                    <a href="{{ route('profile') }}/{{$infulernser->username}}">{{ __(' See Bio') }}</a>
                                </div>
                            </div>
                        </div>
                    @empty
                    @endforelse
                    

                </div>
            </div>
        </div>
    </div>
</div>