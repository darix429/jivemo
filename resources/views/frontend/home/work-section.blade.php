<div class="jivemo-work">
    <div class="jivemo-work-outer-wrapper">
        <div class="work-container">
            <div class="jivemo-work-inner-wrapper">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12">
                        <div class="jivemo-work-lft">
                            <div class="heading">
                                <h2>How it works?</h2>
                            </div>
                            <div class="jivemo-work-lft-single-section">
                                <ol>
                                    <li class="circle">
                                        <h4>Choose a Package</h4>
                                        <a href="#">Choose the texting package you want...</a>
                                    </li>
                                    <li class="circle">
                                        <h4>Verify Your Phone</h4>
                                        <a href="#">We check your phone number and create a new number that allows only you to text who you choose.</a>
                                    </li>
                                    <li class="circle">
                                        <h4>Start Texting</h4>
                                        <a href="#">Use your phone or the web interface to start texting the influencer. Your are charged the monthly access fee and for each message you send (no fee for received massagess.)</a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
                        <div class="jivemo-img-ryt">
                            <img src="images/group-6.png" alt="jivemo">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>