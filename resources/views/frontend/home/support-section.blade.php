<div class="jivemo-support margin-top">
    <div class="jivemo-support-outer-wrapper">
        <div class="support-container">
            <div class="jivemo-support-inner-wrapper">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="jivemo-support-single-section">
                            <div class="jivemo-support-single-section-wrapper">
                                <div class="image">
                                    <img src="images/support1.png" alt="support1">
                                </div>
                                <div class="jivemo-support-details">
                                    <h4>Get Paid to Text</h4>
                                    <p>Fans pay to text you. Charge a recuring monthly fee or charge per message</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="jivemo-support-single-section">
                            <div class="jivemo-support-single-section-wrapper">
                                <div class="image">
                                    <img src="images/support2.png" alt="support2">
                                </div>
                                <div class="jivemo-support-details">
                                    <h4>Privacy Built In</h4>
                                    <p>New phone numbers are created for you & fans so your phone is never shared.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="jivemo-support-single-section">
                            <div class="jivemo-support-single-section-wrapper">
                                <div class="image">
                                    <img src="images/support3.png" alt="support3">
                                </div>
                                <div class="jivemo-support-details">
                                    <h4>Message Control</h4>
                                    <p>Choose how many fans can text & how much it will cost.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
                        <div class="jivemo-support-single-section">
                            <div class="jivemo-support-single-section-wrapper">
                                <div class="image">
                                    <img src="images/support4.png" alt="support4">
                                </div>
                                <div class="jivemo-support-details">
                                    <h4>Simple To Use</h4>
                                    <p>No searate app to download. Simple text from your phone or use the.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>