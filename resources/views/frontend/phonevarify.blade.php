@extends('layouts.app')

@section('content')

    <div class="hero-image-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
		<div class="mx-auto row">
			<div class="jivemo-profile-bottom package-page phone-page pb-4">
				<div class="jivemo-profile-bottom-outer-wrapper phone-page mx-auto col-11">
					<div class="profile-bottom-container">
						<div class="jivemo-profile-bottom-inner-wrapper pb-4">
							<div class="row">
								<div class="col-lg-7 col-md-7 col-sm-12 col-12">
									<div class="message-packages">
										<div class="middle phone-page pt-4">
											<h4>Verify Your Phone to Starting Texting</h4>
											<form>
												<span>Cell Phone Number</span>
												<input type="text" placeholder="Amount to add to your balance  ">
												<p>Once your phone has been verified you’ll be able to send & receive text messages using your cell phone. Your real phone number is kept private and a new number will be created for texting purposes.</p>
												<input type="submit" class="vfc-code" value="Send Verification Code">
											</form>
										</div>
										<div class="middle phone-page">
											<form>
												<span>Enter Verification Code</span>
												<input type="text" placeholder="Verification Code">
												<input type="submit" class="submit-code" value="Submit Code">
											</form>
										</div>
									</div>
								</div>
								<div class="col-lg-4 offset-lg-1 col-md-5 col-sm-12 col-12 phone-right">
									<div class="how-it-works p-4 phone-page">
										<div class="how-it-works-single-section">
											<h5>Texting Info</h5>
											<p>Once your phone has been verified you’ll be able to send & receive text messages using your cell phone. Your real phone number is kept private and a new number will be created for texting purposes.</p>
											<a href="#">View  terms  of  use  &  rules</a>
										</div>
									</div>
									<div class="how-it-works p-4 phone-page btm">
										<h4>How it works?</h4>
										<div class="how-it-works-single-section">
											<h5>Frequent Questions?</h5>
											<p>I’ll keep you posted on all my projects and you’ll have 24-7 acccess to me. Just text me whenever. Since it’s premium only opening it up to 10 lucky people. Looking forward to connecting  :)</p>
										</div>
										<div class="how-it-works-single-section">
											<h5>What is the per message fee?</h5>
											<p>Once you are connected to the influencer in order  to  send a message you will be charged a fee. You are not charged for receiving messages only for sending  them.</p>
										</div>
										<div class="how-it-works-single-section">
											<h5>What is the “balance”?</h5>
											<p>In order to send messages you need to fund your account with money. When you send messages your balance will be decreased.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
