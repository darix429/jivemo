<div class="jivemo-nav">
    <div class="nav-container">
        <div class="row">
        
            <div class="col-lg-12">
                <nav class="navbar navbar-expand-lg navbar-light">
                    <div class="logo">
                    <a href="{{ url('/') }}">
                        <img src="/images/logo.png" alt="logo">
                    </a>
                    </div>
                    <div class="small-size d-none">
                        
                        <div class="top-right links">
                            @auth
                                <a href="#" class="text-white">{{ __('Dashboard') }}</a>
                            @else
                                <a href="{{ route('login') }}">Login/Signup</a>
                            @endauth
                            </div>
                        
                    </div>
                    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                    <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                        <li class="active">
                        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
                        </li>
                        <li>
                        <a class="nav-link" href="/get-paid-to-text">Get Paid to Text</a>
                        </li>
                        {{--  <li>
                            <a class="nav-link" href="#">Explore</a>
                        </li>  --}}
                        <li>
                            <div class="large-size">
                                
                                <div class="top-right">
                                    @auth
                                        <router-link :to="{name:'dashboard' }" class="text-white">{{ __('Dashboard') }}</router-link>
                                    @else
                                        <a class="btn btn-success text-white" href="{{ route('login') }}">Login / Signup </a>
                                    @endauth
                                </div>
                                
                            </div>
                        </li>
                    </ul>
                    
                    </div>
                </nav>
            </div>
        </div>
    </div>
</div>