@extends('layouts.app')

@section('content')
    <div class="hero-image-wrapper pt-5">   
		<div class="mx-auto row">
			<div class="jivemo-profile-bottom package-page phone-page pb-4">
				<div class="jivemo-profile-bottom-outer-wrapper message-page col-11 mx-auto">
					<div class="profile-bottom-container">
						<div class="jivemo-profile-bottom-inner-wrapper pb-4">
							<div class="row">
								<div class="col-lg-2 col-md-2 col-sm-2 col-2">
									<div class="message-packages">
										<ul class="tabs message">
									      <li class="active"><a href="#tab1">Connections</a></li>
									      <li class=""><a href="#tab2">Blocked Users</a></li>
									      <li class=""><a href="#tab3">Balance</a></li>
										</ul>
									</div>
								</div>
								<div class="col-lg-6 col-md-10 col-sm-10 col-12">
									<div class="message-packages">
										<div class="middle msg-page">
											<h5>Messages With Jonathon Doe</h5>
											<form>
												<span>Cell  Phone  Number</span>
												<textarea>												Mobile  phone  number
												</textarea>
												<input type="submit" class="msg" value="Send Messages">
											</form>
										</div>
										<div class="middle msg-page">
											<div class="older-msg">
												<div class="top">
													<a href="#">You</a> <strong>-$0.08</strong>
													<span>15 mins ago</span>
													<div>
														<p>Nothing much. Just watching season 2 of power rangers</p>
													</div>
												</div>
											</div>
											<div class="older-msg">
												<div class="top mid">
													<a href="#">Jonathon Doe</a>
													<span>30 mins ago</span>
													<div>
														<p>Thanks for signing up to message me! What you up to now?</p>
													</div>
												</div>
											</div>
											<div class="older-msg">
												<div class="top">
													<a href="#">JiveMo  Admin</a>
													<span>1 hour ago</span>
													<div>
														<p>[Connection  created  to  kayvictoriac  for  $19.99.  Next  chage  will  be  on  3/12/2019.]</p>
													</div>
												</div>
											</div>
											<form>
												<input type="submit" class="msg btm" value="See Older Messages">
											</form>
										</div>
									</div>
								</div>
								<div class="col-lg-4 col-md-12 col-sm-12 col-12">
									<div class="how-it-works phone-page right msg-page">
										<div class="how-it-works-single-section msg-page">
											<div class="image msg-page">
												<img src="images/group.png" alt="man">
											</div>
											<div class="prf-detail">
												<span>Jhon Doe</span>
												<strong>818-222-3406</strong>
												<p>Use the above phone number to message the user or use web interface to send a message.</p>
												<div class="block-user">
													<span>Report User</span>
													<!-- <span>Block User1</span> -->
												</div>
											</div>
										</div>
									</div>
									<div class="how-it-works ryt-btm p-4">
										<h4>How it works?</h4>
										<div class="how-it-works-single-section">
											<h5>Choose a Package</h5>
											<p>Choose the texting package you want...</p>
										</div>
										<div class="how-it-works-single-section">
											<h5>Verify Your Phone</h5>
											<p>We check your phone number and create a new number that allows only you to text who you choose.</p>
										</div>
										<div class="how-it-works-single-section">
											<h5>Start Texting</h5>
											<p>Use your phone or the web interface to start texting the influencer. Your are charged the monthly access fee and for each message you send (no fee for received massagess.)</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
