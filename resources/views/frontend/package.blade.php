@extends('layouts.app')

@section('content')

    <div class="hero-image-wrapper pt-5">
		<div class="mx-auto row">
			<div class="jivemo-profile-inner-wrapper col-11 mx-auto">
				<div class="jivemo-profile-top">
					<div class="row ryt">
						<div class="col-lg-2 col-md-3 col-sm-12 col-12">
							<div class="image small mx-auto">
								<img src="images/man.png" class="" alt="man">
							</div>
						</div>
						<div class="col-lg-10 col-md-9 col-sm-12 col-12">
							<p class="inner-title pt-4">Confirm Texting  Package  to  Start  Texting  <strong>Johan Lenson</strong></p>
						</div>
					</div>
				</div>
			</div>

			<!-- jivemo-profile-bottom -->
			<div class="jivemo-profile-bottom package-page col-11 mx-auto mt-5 pb-5">
				<div class="jivemo-profile-bottom-outer-wrapper">
					<div class="profile-bottom-container">
						<div class="jivemo-profile-bottom-inner-wrapper pt-5 pb-5">
							<div class="row">
								<div class="col-lg-7  col-md-7  col-sm-12 col-12">
									<div class="message-packages">
                                        <form  method="post" action="{{ route('confirm') }}" role="form" data-toggle="validator">
											@csrf

                                            <input type="hidden" name="seller" value="alinacena" checked>
											<input type="hidden" name="user_id" value="1" checked>

                                            <div class="middle p-4">
                                                <span class="heading">
													<input type="radio" name="jivemo_package" value="pacakage-1" checked>
													<i classs="ml-1">$19.99  per  month  +  $0.08  per  message  sent</i>
												</span>
                                                <strong>Kay's VIP List **EXCLUSIVE**</strong>

                                                <p>I’ll keep you posted on all my projects and you’ll have 24-7 acccess to me. Just text me whenever. Since it’s premium only opening it up to 10 lucky people. Looking forward to connecting :)
                                                </p>
                                                <span>Limited  to  10  People 5  remaining  (5/10)</span>
                                                

                                                <input id="add-your-balance" name="add-your-balance" type="text" placeholder="Amount to add to your balance  ">
                                                <input type="submit" value="Continue">
                                                
                                            </div>

                                            
                                            <div class="middle p-4 mt-4">
                                                <span class="heading">
													<input type="radio" name="jivemo_package" value="pacakage-2"> 
													<i>$3  PER  MONTH  +  $0.35  PER  MESSAGE  SENT</i>
												</span>
                                                <strong>Kay's VIP List **EXCLUSIVE**</strong>
                                                <span>Limited  to  10  People 5  remaining  (5/10)</span>

                                                <span>Amount  to  add  to  your  balance  (used  for  sending  messages  &amp;  tips)</span>
                                                
                                                <input id="add-your-balance" name="add-your-balance"  type="text" placeholder="Amount to add to your balance  ">
                                                <input type="submit" value="Continue">
                                            </div>
                                            
                                        </form> 
									</div>
								</div>
								<div class="col-lg-4 offset-lg-1 col-md-5 col-sm-12 col-12">
									<div class="how-it-works confirm p-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
										<h4>Frequent questions</h4>
										<div class="how-it-works-single-section">
											<h5>What’s Next?</h5>
											<p>Once your charge has been approved you will then verify your number and then can start texting kayvctoriac.</p>
										</div>
									</div>
									<div class="how-it-works confirm btm p-4">
										<h4>How it works?</h4>
										<div class="how-it-works-single-section">
											<h5>Choose a Package</h5>
											<p>Choose the texting package you want...</p>
										</div>
										<div class="how-it-works-single-section">
											<h5>Verify Your Phone</h5>
											<p>We check your phone number and create a new number that allows only you to text who you choose.</p>
										</div>
										<div class="how-it-works-single-section">
											<h5>Start Texting</h5>
											<p>Use your phone or the web interface to start texting the influencer. Your are charged the monthly access fee and for each message you send (no fee for received massagess.)</p>
										</div>
									</div> 
								</div>

								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection
