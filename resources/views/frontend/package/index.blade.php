@extends('layouts.app')

@section('content')
<div class="hero-image-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
    <div class="mx-auto">
        <div class="jivemo-profile-bottom package-page phone-page pb-4">
            <div class="jivemo-profile-bottom-outer-wrapper connection-page col-11 mx-auto">
                <div class="profile-bottom-container">
                    <div class="jivemo-profile-bottom-inner-wrapper pb-4">
                        <div class="row">
                            @include('users_tabs')
                            <div class="col-lg-9 col-md-9 col-sm-12 col-12 pt-lg-5 pt-md-5 pt-sm-3 pt-3">
                                <div class="tab_container1" id="ajax-content">
                                    

                                    <div id="packages" class="" style="">
                                        <div class="manage-packages-section">
                                            <div class="manage-title"><h4 class="">Manage Packages</h4></div>
                                            <div class="create-new-package"><a href="{{route('package_create') }}" class="ajax-load" data-replace="ajax-content">CREATE NEW PACKAGE</a></div>
                                        </div>
                                        
                                        <div class="message-packages">
                                            <div class="middle p-4">
                                                <span class="heading">$19.99  per  month  +  $0.08  per  message  sent</span>
                                                <strong>Kay's VIP List **EXCLUSIVE**</strong>

                                                <p>I’ll keep you posted on all my projects and you’ll have 24-7 acccess to me. Just text me whenever. Since it’s premium only opening it up to 10 lucky people. Looking forward to connecting :)
                                                </p>
                                                <span>Limited  to  10  People 5  remaining  (5/10)</span>

                                                <div class="package-manage">
                                                    <div class="package-edit">
                                                        <a href="" class="package-edit">Edit Package</a>
                                                    </div>
                                                    |
                                                    <div class="package-edit">
                                                        <a href="" class="package-edit">View Subscribers</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="middle p-4">
                                                <span class="heading">$19.99  per  month  +  $0.08  per  message  sent</span>
                                                <strong>Kay's VIP List **EXCLUSIVE**</strong>

                                                <p>I’ll keep you posted on all my projects and you’ll have 24-7 acccess to me. Just text me whenever. Since it’s premium only opening it up to 10 lucky people. Looking forward to connecting :)
                                                </p>
                                                <span>Limited  to  10  People 5  remaining  (5/10)</span>

                                                <div class="package-manage">
                                                    <div class="package-edit">
                                                        <a href="" class="package-edit">Edit Package</a>
                                                    </div>
                                                    |
                                                    <div class="package-edit">
                                                        <a href="" class="package-edit">View Subscribers</a>
                                                    </div>
                                                </div>
                                            </div>


                                        </div>


                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
@section('footer')
<script>
    jQuery(document).ready(function(){
        var homePageTitle = document.title;
        $('body').on('click33', 'a.ajax-load', function(e){
            e.preventDefault();
            var id = $(this).data('replace')
            var link = $(this).attr('href');
            var title = jQuery(this).text();

            $("#"+id).load(link +" #"+id);
            document.title =  title+ " | " + homePageTitle.split("|")[0];
	        window.history.pushState(document.title, document.title, link);

        })

    })
</script>
@endsection