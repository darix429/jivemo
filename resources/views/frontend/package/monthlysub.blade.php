@extends('layouts.app')

@section('content')
<div class="hero-image-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
    <div class="mx-auto">
        <div class="jivemo-profile-bottom package-page phone-page pb-4">
            <div class="jivemo-profile-bottom-outer-wrapper connection-page col-11 mx-auto">
                <div class="profile-bottom-container">
                    <div class="jivemo-profile-bottom-inner-wrapper pb-4">
                        <div class="row">
                            <div class="col-lg-3 col-md-3 col-sm-12 col-12">
                                <div class="message-packages">
                                    <ul class="tabs  pt-lg-5 pt-md-5 pt-sm-3 pt-3">
                                        <li class=""><a href="#tab1">Connections</a></li>
                                        <li class=""><a href="#tab2">Blocked Users</a></li>
                                        <li class="active"><a href="#packages">Packages</a></li>
                                        <li class=""><a href="#tab3">Balance</a></li>
                                    </ul>
                                </div>
                            </div>
                            <div class="col-lg-9 col-md-9 col-sm-12 col-12 pt-lg-5 pt-md-5 pt-sm-3 pt-3">
                                <div class="tab_container1">
                                    

                                    <div id="packages" class="" style="">
                                        <div class="manage-packages-section">
                                            <div class="manage-title"><h4 class="">Create New Package</h4></div>
                                        </div>
                                        

                                        <div class="middle signup package-create">
                                            <form id="JimevoRegister" method="POST" action="{{ route('monthly-subscription') }}" role="form" data-toggle="validator">
                                                @csrf

                                                <!--  package name -->
                                                <div class="form-group">
                                                    <label for="name">Name</label>
                                                    <input type="text" name="name" class="form-control" id="name" placeholder="What do you want to call this package?" data-error="Bruh, that email address is invalid" value="{{ old('name') }}" required>
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('name') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>


                                                 <!--  package Description -->
                                                 <div class="form-group">
                                                    <label for="description">Description</label>
                                                    <textarea name="description" id="description" cols="30" rows="10">What will users get for subscribing to this package?</textarea>
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('description') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>

                                                <!--  package Description -->
                                                 <div class="form-group">
                                                    <label for="description">Number of Messages Included Per Month</label>
                                                    <p>This includes sent & received messages between you and the subscriber</p>
                                                </div>

                                                <!--  package Description -->
                                                <div class="form-group">
                                                    <label for="description">100 Messages / Month <a href="">Edit</a> </label>
                                                    <p>JiveMo Management Fee is $3.99 Month/User</p>
                                                </div>

                                                <!--  package Description -->
                                                <div class="form-group">
                                                    <label for="description">Monthly Subscription Fee Per User</label>
                                                    <p>How much you will charge each user per month. Must be greater than $3.99</p>
                                                    
                                                    <div class="row">
                                                        <div class="col">
                                                            <input type="text" class="form-control" placeholder="$19.99">
                                                        </div>
                                                        <div class="col">
                                                            <label>Month / User</label>
                                                        </div>
                                                        <div class="col">

                                                        </div>
                                                    </div>
</div>


                                                

                                                <!--  package Description -->
                                                <div class="form-group">
                                                    <label for="description">Maximum Number of People Who Can Subscribe</label>
                                                    <p>How many people you want to allow to subscribe to this package</p>
                                                    
                                                    <div class="row">
                                                        <div class="col">
                                                            <input type="text" class="form-control" placeholder="10 ">
                                                        </div>
                                                        <div class="col">
                                                            <label for="description">Users</label>
                                                            <input type="submit" value="NEXT">
                                                        </div>
                                                        
                                                    </div>
                                                </div>

                                                
                                            </form>	
                                        </div>
                                       




                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
