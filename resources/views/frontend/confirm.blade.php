@extends('layouts.app')
 
@section('content')
    <div class="hero-image-wrapper pt-5">
		<div class="mx-auto">
			<div class="jivemo-profile-inner-wrapper col-11 mx-auto">
				<div class="jivemo-profile-top">
					<div class="row ryt">
						<div class="col-lg-2 col-md-3 col-sm-12 col-12">
							<div class="image small mx-auto">
								<img src="images/man.png" class="" alt="man">
							</div>
						</div>
						<div class="col-lg-10 col-md-9 col-sm-12 col-12">
							<p class="inner-title pt-4">Confirm Texting  Package  to  Start  Texting  <strong>Johan Lenson</strong></p>
						</div>
					</div>
				</div>
			</div>

			<!-- jivemo-profile-bottom -->
			<div class="jivemo-profile-bottom package-page py-5">
				<div class="jivemo-profile-bottom-outer-wrapper confirm col-11 mx-auto pb-5">
					<div class="profile-bottom-container">
						<div class="jivemo-profile-bottom-inner-wrapper">
							<div class="row pt-5">
								<div class="col-lg-7 col-md-7 col-sm-12 col-12">
									<div class="message-packages">
										<div class="middle confirm p-4">
											<h5>Texting Package</h5>
											<div class="table-responsive">
												<table class="table confirm"> 
													<tbody>
														<tr>
															<td>Monthly Fee</td>
															<td>$19-99(recurring-each month)</td>
														</tr>
														<tr>
															<td>Per-Message-Fee</td>
															<td>0-08-per-sent-message</td>
														</tr>
														<tr>
															<td>Addition-to-Balance</td>
															<td>$10  (your  current  balance  $5.85)</td>
														</tr>
														<tr>
															<td>Promotion</td>
															<td>$3  Extra  Added  to  Balance  (code  A123FB)</td>
														</tr>
														<tr>
															<td>Total  to  Be  Charged</td>
															<td>$29.99  (next  $19.99  charge  10/28/18)</td>
														</tr>
													</tbody>
												</table>
											</div>
											<form>
												<input type="text" placeholder="Amount to add to your balance  ">
												<input type="submit" value="Continue">
											</form>
										</div>
										<div class="middle confirm-page">
											<form>
												<span>Card Number</span>
												<input type="text" placeholder="Credit Card Number">
												<span>Cardholder Name</span>
												<input type="text" placeholder="Name  on  Credit  Card">
												<span>Expiration</span>
												<input type="date" placeholder="Zip  Code  Associated  with  Card">
												<span>Security  Code</span>
												<input type="password" placeholder="CVC">
												<div class="form-row">
												   <div class="form-group col-md-6">
												    <span>Expiration</span>
												     <input type="date" class="form-control">
												   </div>
												   <div class="form-group col-md-6">
												     <span>Security  Code</span>
												     <input type="password" class="form-control" placeholder="CVC">
												   </div>
												</div>
												<span>Zip  Code</span>
												<input type="text" placeholder="Zip  Code  Associated  with  Card">
												
												<div class="form-group">
												    <div class="form-check">
												      <input class="form-check-input" type="checkbox" id="gridCheck">
												     <span>  I  agree  to  the  monthly  recurring  charge  of  $19.99</span>
												    </div>
												  </div>
												<input type="submit" value="Purchase this Package">
											</form>
										</div>
									</div>
								</div>
								<div class="col-lg-4 offset-lg-1 col-md-5 col-sm-12 col-12">
									<div class="how-it-works confirm p-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
										<h4>Frequent questions</h4>
										<div class="how-it-works-single-section">
											<h5>What’s Next?</h5>
											<p>Once your charge has been approved you will then verify your number and then can start texting kayvctoriac.</p>
										</div>
									</div>
									<div class="how-it-works confirm btm p-4">
										<h4>How it works?</h4>
										<div class="how-it-works-single-section">
											<h5>Choose a Package</h5>
											<p>Choose the texting package you want...</p>
										</div>
										<div class="how-it-works-single-section">
											<h5>Verify Your Phone</h5>
											<p>We check your phone number and create a new number that allows only you to text who you choose.</p>
										</div>
										<div class="how-it-works-single-section">
											<h5>Start Texting</h5>
											<p>Use your phone or the web interface to start texting the influencer. Your are charged the monthly access fee and for each message you send (no fee for received massagess.)</p>
										</div>
									</div> 
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
