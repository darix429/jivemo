<!-- footer -->
<div class="footer">
    <div class="footer-outer-wrapper">
        <div class="footer-container">
            <div class="footer-inner-wrapper">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-10 mx-auto">
                        <div class="left">
                            <ul>
                                <li>
                                    <router-link to="/terms-and-condition">Terms and Condition</router-link>
                                </li>
                                <li>
                                    <router-link  to="/privacy-policy">Privacy Policy</router-link>
                                </li>
                                <li>
                                    <router-link  to="/faqs">FAQs</router-link>
                                </li>
                                <!-- <li>
                                    <a href="{{ route('logout') }}"
                                        onclick="event.preventDefault();
                                                        document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </li> -->
                            </ul>
                        </div>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-10 mx-auto">
                        <div class="right">
                            <ul>
                                <li>
                                    <span>{{ config('site_setting.copyright', 'Copyright Jivemo.com©2019') }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>