@extends('layouts.app')

@section('content')
<div class="hero-image-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
    <div class="row">
        <div class="jivemo-profile-bottom package-page phone-page pb-4 col">
            <div class="jivemo-profile-bottom-outer-wrapper phone-page signup mx-auto col-11 pb-lg-5 pb-md-4 pb-sm-4 pb-3 ">
                <div class="profile-bottom-container">
                    <div class="jivemo-profile-bottom-inner-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
                        <div class="row">
                            
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mx-auto">
                                <div class="not-varify-email">
                                    <div class="card-header text-white">{{ __('Verify Your Email Address') }}</div>

                                        <div class="card-body">
                                            @if (session('resent'))
                                                <div class="alert alert-success" role="alert">
                                                    {{ __('A fresh verification link has been sent to your email address.') }}
                                                </div>
                                            @endif

                                            {{ __('Before proceeding, please check your email for a verification link.') }}
                                            {{ __('If you did not receive the email') }}, <a href="{{ route('verification.resend') }}">{{ __('click here to request another') }}</a>.
                                        </div>
                                    </div>
                                </div>



                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="before-footer"></div>
@endsection
