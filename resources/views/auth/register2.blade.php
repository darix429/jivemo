@extends('layouts.app')

@section('content')
    <div class="hero-image-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
        <div class="row">
            <div class="jivemo-profile-bottom package-page phone-page pb-4 col">
                <div class="jivemo-profile-bottom-outer-wrapper phone-page signup mx-auto col-11 pb-lg-5 pb-md-4 pb-sm-4 pb-3 ">
                    <div class="profile-bottom-container">
                        <div class="jivemo-profile-bottom-inner-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
                            <div class="row">

                            
                            
								<div class="col mx-auto">
                                    <h3 class="text-dark col mx-auto">Sign Up</h3>
                                    <div class="message-packages signup">
                                        <div class="middle signup right p-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">

                                            <p>Your phone has been verified. Please fill up the fields below to complete your registration.</p>
                                            <form action="{{ route('register-user') }}" method="post">
                                            @csrf
                                            @if ($errors->any())
                                                @foreach ($errors->all() as $error)
                                                    <div class="alert alert-danger" role="alert">{{$error}}</div>
                                                @endforeach
                                            @endif
                                            <div class="row">
                                                <div class="col">
                                                    <div class="form-group">
                                                        <input type="text" name="firstname" id="firstname" placeholder="first name" minlength="2" maxlength="50" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col">
                                                    <div class="form-group">
                                                        <input type="text" name="lastname" id="lastname" placeholder="last name" minlength="2" maxlength="50" class="form-control">
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="form-group"><input type="text" name="username" id="username" minlength="6" maxlength="30" class="form-control" placeholder="Username" required></div>
                                            <div class="form-group"><input type="email" name="email" id="email" minlength="6" maxlength="64" class="form-control" placeholder="Email"></div>
                                            
                                            <div class="form-group"><button type="submit" class="btn btn-primary">Save</button></div>

                                            </form>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="col">
                                <h3 class="text-dark col mx-auto">Info About JiveMo</h3>
                                <div class="div">
                                In order to use JiveMo to text your favorite Influencer you must use a valid US or Canadian phone number that can receive text messages. Phone numbers are NEVER sold or shared and are only used to setup text messaging. Your number is ALWAYS kept PRIVATE.
                                </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="before-footer"></div>
@endsection


@section('scripts')
<script>
    $(document).ready(function(){
        document.title = "Sign Up - JiveMo";;
        console.log(document.title);
    })
</script>

@endsection