@extends('layouts.app')
@section('title')
  Dashboard
@endsection
@section('content')

    <div class="hero-image-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
        <div class="row">
            <div class="jivemo-profile-bottom package-page phone-page pb-4 col">
                <div class="jivemo-profile-bottom-outer-wrapper phone-page signup mx-auto col-11 pb-lg-5 pb-md-4 pb-sm-4 pb-3 ">
                    <div class="profile-bottom-container">
                        <div class="jivemo-profile-bottom-inner-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
                            <div class="row">
                                
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mx-auto">
                                    <h3 class="text-dark col mx-auto">{{ __('Login') }} or Sign Up via Cell Phone</h3>
                                    <div class="message-packages signup">
                                        <div class="middle signup right p-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                                            <form id="login-form" action="{{ route('login-user') }}" method="post">
                                            @csrf
                                            <input type="hidden" name="redirect" value="{{ app('request')->input('redirect') }}">
                                            
                                            @if ($errors->any())
                                                @foreach ($errors->all() as $error)
                                                    <div class="alert alert-danger" role="alert">{{$error}}</div>
                                                @endforeach
                                            @endif

                                            @if(!session('data')['country'])
                                            <div class="form-group">
                                                <select name="country" id="country" class="form-control" required>
                                                    <option value="" selected disabled>Country</option>
                                                    @foreach ($countries as $code=>$name)
                                                        <option value="{{$code}}" {{ $code=="US"? "selected": ""}}>{{$name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                            @endif
                                            <div class="form-group">
                                                <input type="number" name="phone_no" id="phone_no" class="form-control" value="{{ session('data')['phone_no'] }}" placeholder="Cell Phone Number" required {{ session('data')['phone_no']? 'readonly': '' }} maxlength="20">
                                            </div>
                                            @if(session('data')['code'])
                                                <div class="form-group">
                                                    <!-- <input type="number" name="code" id="code" class="form-control" placeholder="Code {{ session('data')['code'] }}" required  maxlength="5"> -->
                                                    <input type="number" name="code" id="code" class="form-control" placeholder="Code" required  maxlength="5">
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success">Verify code</button>
                                                </div>
                                            @else
                                                <div class="form-group">
                                                    <div class="row justify-content-around align-items-start">
                                                        <div class="g-recaptcha" data-sitekey="6Lef3NUUAAAAAMvtlJyhSMCkJnGxL43COzrGdSzS"></div>
                                                        <button type="submit" class="btn btn-primary">Send verification code</button>
                                                    </div>
                                                </div>
                                            @endif
                                            <input type="hidden" name="recaptcha" id="recaptcha">
                                            </form>

                                            @if(session('data')['code'])
                                            <div>
                                                <a href="/resend-code" class="resend-code-btn" phone="{{ session('data')['phone_no'] }}">Resend code</a><br>
                                                <a href="">Use another phone number</a>
                                            </div>
                                            @endif

                                        </div>
                                    </div>
                                </div>

                                <div class="col">
                                <h3 class="text-dark col mx-auto" style="padding: 0px; margin: 0px; text-indent: 0px;">Info About JiveMo</h3>
                                <div class="div">
                                <p>In order to use JiveMo to text your favorite Influencer you must use a valid US or Canadian phone number that can receive text messages. Phone numbers are NEVER sold or shared and are only used to setup text messaging. Your number is ALWAYS kept PRIVATE.</p>
                                
                                <p>Login or sign up for Jivemo by using your cell number.</p>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection


@section('scripts')
<script>
    $(document).load(function(){
        document.title = "JiveMo - Login or Register";
    })
    $(document).on("click", ".resend-code-btn", function(e){
        e.preventDefault();
        $("input#code").remove();
        $("form#form").submit();
    });
</script>
@if(!session('data')['country'])
<script>
    $(document).on("submit", "#login-form", function(e){
        e.preventDefault();
        var validCountries = ["US", "CA"];
        var country = $("#country").children("option:selected").val();
        if (validCountries.indexOf(country) == -1) {
            alert("We only accept phone numbers from US and Canada");
        } else {
            $(this)[0].submit();
        }
    })
</script>
@endif
@endsection