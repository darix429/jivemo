@extends('layouts.app')

@section('content')


    <div class="hero-image-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
        <div class="row">
            <div class="jivemo-profile-bottom package-page phone-page pb-4 col">
                <div class="jivemo-profile-bottom-outer-wrapper phone-page signup mx-auto col-11 pb-lg-5 pb-md-4 pb-sm-4 pb-3 ">
                    <div class="profile-bottom-container">
                        <div class="jivemo-profile-bottom-inner-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
                            <div class="row">

                            
								<div class="col mx-auto">
                                    <h3 class="text-dark col mx-auto">Sign Up</h3>
                                    <div class="message-packages signup">
                                        <div class="middle signup right p-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">

                                            <form action="{{ route('verify-user') }}" method="post">
                                            @csrf
                                            @if ($errors->any())
                                                @foreach ($errors->all() as $error)
                                                    <div class="alert alert-danger" role="alert">{{$error}}</div>
                                                @endforeach
                                            @endif
                                                <div class="form-group">
                                                    <input type="number" name="phone_no" id="phone_no" class="form-control" value="{{ session('data')['phone_no'] }}" placeholder="Cellphone Number" required {{ session('data')['phone_no']? 'readonly': '' }} maxlength="20">
                                                </div>
                                            @if(session('data')['code'])
                                                <div class="form-group">
                                                    <input type="number" name="code" id="code" class="form-control" placeholder="Code  {{ session('data')['code'] }}" required  maxlength="5">
                                                </div>
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-success">Verify code</button>
                                                </div>
                                            @else
                                                <div class="form-group">
                                                    <button type="submit" class="btn btn-primary">Send verification code</button>
                                                </div>
                                            @endif
                                            </form>
                                            @if(session('data')['code'])
                                            <a href="">Resend verification code.</a><br>
                                            <a href="">Enter different phone number.</a>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col">
                                <h3 class="text-dark col mx-auto">Info About JiveMo</h3>
                                <div class="div">
                                In order to use JiveMo to text your favorite Influencer you must use a valid US or Canadian phone number that can receive text messages. Phone numbers are NEVER sold or shared and are only used to setup text messaging. Your number is ALWAYS kept PRIVATE.
                                </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<div class="before-footer"></div>
@endsection

@section('scripts')
<script>
    $(document).load(function(){
        document.title = "Sign Up - JiveMo";
    })
</script>

@endsection