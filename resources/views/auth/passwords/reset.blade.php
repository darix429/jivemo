@extends('layouts.app')

@section('content')

<div class=" hero-image-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
		<div class="row">
			<div class=" package-page phone-page pb-4 col">
				<div class="jivemo-profile-bottom-outer-wrapper phone-page signup mx-auto col-8 pb-lg-5 pb-md-4 pb-sm-4 pb-3 ">
					<div class="profile-bottom-container">
						<div class="jivemo-profile-bottom-inner-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
							<div class="row">
								<div class="col-lg-9 col-md-9 col-sm-12 col-12 mx-auto">
                                    @if (session('status'))
                                        <div class="alert alert-success" role="alert">
                                            {{ session('status') }}
                                        </div>
                                    @endif
                                    <div class="col-lg-12">
                                        <form method="POST" action="{{ route('password.update') }}">
                                            @csrf

                                            <input type="hidden" name="token" value="{{ $token }}">

                                            <div class="form-group row">
                                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                                <div class="col-md-6">
                                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ $email ?? old('email') }}" required autofocus>

                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>

                                                    @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>
                                            </div>

                                            <div class="form-group row">
                                                <label for="password-confirm" class="col-md-4 col-form-label text-md-right">{{ __('Confirm Password') }}</label>

                                                <div class="col-md-6">
                                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
                                                </div>
                                            </div>

                                            <div class="form-group row mb-0">
                                                <div class="col-md-6 offset-md-4">
                                                    <button type="submit" class="btn btn-primary">
                                                        {{ __('Reset Password') }}
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
								</div>


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="footer-margin" style="
            height: 100px;
            background: #fff;
            margin-top: -100px;
        "></div>

    

@endsection

