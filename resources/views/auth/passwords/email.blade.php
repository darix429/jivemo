@extends('layouts.app')

@section('content')

<div class=" hero-image-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
		<div class="row">
			<div class=" package-page phone-page pb-4 col">
				<div class="jivemo-profile-bottom-outer-wrapper phone-page signup mx-auto col-8 pb-lg-5 pb-md-4 pb-sm-4 pb-3 ">
					<div class="profile-bottom-container">
						<div class="jivemo-profile-bottom-inner-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
							<div class="row">
								<div class="col-lg-6 col-md-6 col-sm-12 col-12 mx-auto">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif
                                <div class="col-lg-12">
                                    <form method="POST" action="{{ route('password.email') }}">
                                        @csrf

                                        <div class="form-group row ">
                                            <div class="col-md-12">
                                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="{{ __('E-Mail Address') }}" required>

                                                @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>

                                        <div class="form-group row mb-0">
                                            <div class="col-md-12 ">
                                                <button type="submit" class="btn btn-primary">
                                                    {{ __('Send Password Reset Link') }}
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
								</div>


							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <div class="footer-margin" style="
            height: 100px;
            background: #fff;
            margin-top: -100px;
        "></div>

    

@endsection
