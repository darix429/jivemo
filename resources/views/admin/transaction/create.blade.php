@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="modal-header">
                    <h3><a href="{{route('admin-userdetails', $user->username)}}">Back</a></h3>
                    <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h3 class="modal-title">Create Transaction</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="modal-body">
                        <form method="post" class="form-horizontal" action="{{ route('admin-transaction-create', $user->username)}}">
                            @csrf
                            <input type="hidden" name="user_id" value="{{ $user->id }}">
                            <div class="row">
                                
                                <div class="col form-group">
                                    <label for="seller_id" class="col-sm-12 control-label">Seller</label>
                                    <div class="col-sm-12">
                                        <select name="seller_id" class="form-control">
                                            <option value="">--</option>
                                            @foreach($users as $user)
                                                <option value="{{ $user->id }}">{{ $user->username }}</option>
                                            @endforeach
                                            
                                        </select>
                                        @if ($errors->has('seller_id'))
                                            <p class="text-danger"><strong>{{ $errors->first('seller_id') }}</strong></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="row">
                                <div class="col form-group">
                                    <label for="transaction_type" class="col-sm-12 control-label">Transaction Type</label>
                                    <div class="col-sm-12">
                                        <select name="transaction_type" class="form-control">
                                            <option value="">--</option>
                                            @foreach($transaction_types as $type)
                                                <option value="{{ $type->id }}">{{ $type->name }}</option>
                                            @endforeach
                                            
                                        </select>
                                        @if ($errors->has('transaction_type'))
                                            <p class="text-danger"><strong>{{ $errors->first('transaction_type') }}</strong></p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col form-group">
                                    <label for="amount" class="col-sm-12 control-label">Amount</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" value="{{ old('amount') }}" id="amount" name="amount" >
                                        @if ($errors->has('amount'))
                                            <p class="text-danger"><strong>{{ $errors->first('amount') }}</strong></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col form-group">
                                    <label for="description" class="col-sm-12 control-label">Description</label>
                                    <div class="col-sm-12">
                                        <textarea class="form-control" id="description" name="description" >{{ old('description') }}</textarea>
                                        @if ($errors->has('description'))
                                            <p class="text-danger"><strong>{{ $errors->first('description') }}</strong></p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col form-group">
                                    <label for="admin_note" class="col-sm-12 control-label">Admin note</label>
                                    <div class="col-sm-12">
                                        <textarea class="form-control" id="admin_note" name="admin_note" >{{ old('admin_note') }}</textarea>
                                        @if ($errors->has('admin_note'))
                                            <p class="text-danger"><strong>{{ $errors->first('admin_note') }}</strong></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col form-group">
                                    <label for="payout" class="col-sm-12 control-label">Payout</label>
                                    <div class="col-sm-12">
                                        
                                        <select name="payout" class="form-control">
                                            <option value="0">No</option>
                                            <option value="1">Yes</option>
                                            
                                        </select>
                                        @if ($errors->has('payout'))
                                            <p class="text-danger"><strong>{{ $errors->first('payout') }}</strong></p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col form-group">
                                    <label for="invoice_id" class="col-sm-12 control-label">Invoice ID</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" value="{{ old('invoice_id') }}" id="invoice_id" name="invoice_id" >
                                        @if ($errors->has('invoice_id'))
                                            <p class="text-danger"><strong>{{ $errors->first('invoice_id') }}</strong></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col form-group">
                                    <label for="payout_id" class="col-sm-12 control-label">Payout ID</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" value="{{ old('payout_id') }}" id="payout_id" name="payout_id" >
                                        @if ($errors->has('payout_id'))
                                            <p class="text-danger"><strong>{{ $errors->first('payout_id') }}</strong></p>
                                        @endif
                                    </div>
                                </div>

                                <div class="col form-group">
                                    <label for="connection_id" class="col-sm-12 control-label">Connection ID</label>
                                    <div class="col-sm-12">
                                        <input type="text" class="form-control" value="{{ old('connection_id') }}" id="connection_id" name="connection_id" >
                                        @if ($errors->has('connection_id'))
                                            <p class="text-danger"><strong>{{ $errors->first('connection_id') }}</strong></p>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-12">
                                    <button type="submit" class="btn btn-success">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection