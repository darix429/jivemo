@extends('layouts.admin')
@section('content')
<section class="content">
    <div class="container-fluid">
        <div class="row">
            @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true">×</button>
                {{ session()->get('message') }}
            </div>
            @endif
            @if(session()->has('error'))
            <div class="alert alert-danger alert-dismissible col-12">
                <button type="button" class="close" data-dismiss="alert"
                    aria-hidden="true">×</button>
                {{ session()->get('error') }}
            </div>
            @endif

            <div class="col-md-12">
                <!-- Widget: user widget style 1 -->
                <div class="card card-widget widget-user">
                    <!-- Add the bg color to the header using any of the bg-* classes -->
                    <div class="widget-user-header bg-info-active">
                        <h3 class="widget-user-username">{{ $user->name }} </h3>                        
                        @if ( $user->tagline )
                            <h5 class="widget-user-desc">{{ $user->tagline }}</h5>
                        @endif
                    </div>
                    <div class="widget-user-image">
                        @if ( $user->profile_image )
                        <img class="img-circle elevation-2" src="{{asset("images/uploads/".$user->profile_image)}}"
                        style="width:100px;height:100px;border-radius:100%;"
                        alt="{{ $user->name }}">
                        @endif
                    </div>
                    <div class="card-footer">
                        <div class="row">
                            @if ( count( $connections ) )
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header">{{ count(
                                        $connections ) }}</h5>
                                    <span class="description-text">Connections</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            @endif
                            <!-- /.col -->
                            <div class="col-sm-4 border-right">
                                <div class="description-block">
                                    <h5 class="description-header"><span class="badge badge-warning">${{ $user->getPayoutInfo('amount') }}</span></h5>
                                    <span class="description-text">Earn</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            <!-- /.col -->

                            @if ( count( $user->packages ) )
                            <div class="col-sm-4">
                                <div class="description-block">
                                    <h5 class="description-header">{{ count( $user->packages ) }}</h5>
                                    <span class="description-text">Packages</span>
                                </div>
                                <!-- /.description-block -->
                            </div>
                            @endif
                            <!-- /.col -->
                        </div>
                        <!-- /.row -->
                    </div>
                </div>
                <!-- /.widget-user -->
            </div>

            <div class="col-md-12">
                <div class="card">
                    <div class="card-header p-2">
                        <ul class="nav nav-pills pull-left">
                            @if( $user->is_influencer )
                            <li class="nav-item"><a class="nav-link active show" href="#activity" data-toggle="tab">Packages</a> </li>
                            @endif

                            <li class="nav-item"><a class="nav-link @if(!$user->is_influencer) active show @endif" href="#connections" data-toggle="tab">Connections</a></li>
                            <li class="nav-item"><a class="nav-link" href="#reported" data-toggle="tab">Reported</a></li>
                            <li class="nav-item"><a class="nav-link" href="#transactions" data-toggle="tab">Transactions</a></li>
                            <li class="nav-item"><a class="nav-link" href="#phone" data-toggle="tab">Phone</a></li>
                            @if( $user->InfluencerApplication)
                                <li class="nav-item"><a class="nav-link" href="#applyforinfluencer" data-toggle="tab">Apply For Influencer</a></li>
                            @endif
                            @if( $user->is_influencer )
                            <li class="nav-item"><a class="nav-link" href="#payouts" data-toggle="tab">Payout</a></li>
                            @endif
                        </ul>
                        <a class="btn btn-primary pull-right" href="{{route('admin-user-view', ['id'=> $user->id])}}" role="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit Profile</a>
                    </div><!-- /.card-header -->
                    <div class="card-body">
                        <div class="tab-content">

                            {{-- //package section --}}
                            @if( $user->is_influencer )
                            <div class="tab-pane active show" id="activity">
                                
                                <div class="card">
                                    <div class="card-header">
                                        <h3 class="card-title float-left">Packages</h3>
                                        <a href="{{route('admin-add-package', ['id'=> $user->id])}}" class="btn btn-primary pull-right">Add Package</a>
                                    </div>
                                    <div class="card-body">
                                        <!-- Post -->
                                        @if(!$packages->isEmpty())
                                        @foreach ( $packages as $package)
                                        <div class="post">
                                            <div class="user-block">
                                                <img class="profile-user-img img-fluid
                                                    img-circle" src="{{asset("images/uploads/".$user->profile_image)}}"
                                                height="128" width="128"
                                                style="border-radius:100%;" alt="{{
                                                $user->name }}">


                                                <span class="username">
                                                    <a href="#">{{ $package->name }}</a>
                                                </span>
                                                <span class="description">{{ $package->created_at
                                                    }}</span>
                                            </div>
                                            <!-- /.user-block -->
                                            <p>{{ $package->description }}</p>
                                                <span><strong>${{$package->fee }} per month for {{$package->no_of_message }} messages exchanged</strong></span>
                                                    
                                            <div class="package-manage">
                                                <div class="package-edit">
                                                    <a class="btn btn-primary btn-sm"
                                                        href="{{route('admin-package-update',
                                                        ['id'=> $package->id])}}">Edit</a>
                                                    <button type="button" class="btn
                                                        btn-danger user_delete_products btn-sm"
                                                        user_id="{{ $package->id }}" >
                                                        Delete</button>
                                                </div>
                                            </div>
                                        </div>
                                        @endforeach
                                        @else
                                        <div class="mx-5 text-center">
                                            <h4 class="px-5">No Package</h4>
                                        </div><hr />
                                        @endif
                                    </div>
                                </div>
                                

                                @if(!$packages->isEmpty())
                                <div class="pagination-sm m-0 float-right">
                                    {{ $packages->links() }}
                                </div>
                                @endif
                            </div>
                            @endif

                            {{-- //end package <i class="fas fa-search-location"></i>
                            --}}

                            {{-- Connections list --}}

                            <div class="tab-pane @if(!$user->is_influencer) active show @endif" id="connections">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Connections</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table id="example1" class="table
                                                table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>Seller</th>
                                                        <th>Buyer</th>
                                                        <th>Texting Package</th>
                                                        <th>Status</th>
                                                        <th>Date Subscribed</th>
                                                        <th>Next Charge Date</th>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(!$connections->isEmpty())
                                                    @foreach ($connections as $connection)
                                                    <tr>
                                                        <td>
                                                            <a
                                                                href="{{route('admin-userdetails',
                                                                ['username'=>
                                                                $connection->Seller->username])}}">{{ $connection->Seller->username }}</a>
                                                        </td>
                                                        <td><a
                                                                href="{{route('admin-userdetails', ['username'=> $connection->user->username])}}">{{ $connection->user->username }}</a></td>
                                                        <td>
                                                            @if($connection->package)
                                                            {{ $connection->package->name }}
                                                            @endif
                                                        </td>
                                                        <td>
                                                            {{ ucfirst($connection->status) }}
                                                        </td>
                                                        <td>{{ $connection->created_at->toFormattedDateString() }}</td>
                                                        <td>{{  \Carbon\Carbon::parse($connection->next_payment_date)->format('M d, Y') }}</td>
                                                        <td>

                                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">
                                                                Action
                                                            </button>
                                                            <div class="dropdown-menu">
                                                                
                                                                <form class="dropdown-item" name="user-phone-update d-inline" method="post" action="{{route('admin-connection-status',
                                                                    ['id'=> $connection->id])}}" onsubmit="return confirm('Are you sure?')">
                                                                    @csrf
                                                                    @method('PUT')
                                                                    <input type="hidden" value="active" name="status">
                                                                    <input type="submit" value="Active" class="btn btn-link">
                                                                </form>

                                                                 <form class="dropdown-item" name="user-phone-update d-inline" method="post" action="{{route('admin-connection-status',
                                                                    ['id'=> $connection->id])}}" onsubmit="return confirm('Are you sure?')">
                                                                    @csrf
                                                                    @method('PUT')
                                                                    <input type="hidden" value="ending" name="status">
                                                                    <input type="submit" value="Ending" class="btn btn-link">
                                                                </form>

                                                                 <form class="dropdown-item" name="user-phone-update d-inline" method="post" action="{{route('admin-connection-status',
                                                                    ['id'=> $connection->id])}}" onsubmit="return confirm('Are you sure?')">
                                                                    @csrf
                                                                    @method('PUT')
                                                                    <input type="hidden" value="inactive" name="status">
                                                                    <input type="submit" value="Inactive" class="btn btn-link">
                                                                </form>                                                            
                                                            </div>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <th colspan="7">
                                                            <div class="mx-5
                                                                text-center">
                                                                <h4 class="px-">No Connection</h4>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        @if(!$connections->isEmpty())
                                        <div class="pagination-sm ml-4
                                            float-right">
                                            {{ $connections->links() }}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{-- end Connections list --}}

                            {{-- Reported User --}}
                            <div class="tab-pane" id="reported">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Reported List</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table id="example" class="table
                                                table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <td>Username</td>
                                                        <td>Date</td>
                                                        <td>Message</td>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(!$reported_users->isEmpty())
                                                    @foreach ($reported_users as
                                                    $reported)
                                                    <tr>
                                                        <td><a
                                                                class="users-list-name"
                                                                href="{{route('admin-users')}}/{{
                                                                $reported->user->username
                                                                }}">{{
                                                                $reported->user->username
                                                                }}</a></td>
                                                        <td>{{ $reported->created_at->toFormattedDateString()
                                                            }}</td>
                                                        <td>{{ $reported->message
                                                            }}</td>
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <th colspan="5">
                                                            <div class="mx-5 text-center">
                                                                <h4 class="px-5">No Reported User</h4>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        @if(!$reported_users->isEmpty())
                                        <div class="pagination-sm ml-4
                                            float-right">
                                            {{ $reported_users->links() }}
                                        </div>
                                        @endif
                                    </div>
                                </div>
                            </div>
                            {{-- End Reported User --}}


                            {{-- Transactions User --}}
                            <div class="tab-pane" id="transactions">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title float-left">Transactions List</h3>
                                            <a href="{{ route('admin-transaction-create', ['username'=> $user->username]) }}" class="btn btn-primary float-right">Add</a>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table id="example" class="table
                                                table-bordered table-striped">
                                                <thead>
                                                    <tr>
                                                        <th>User</th>
                                                        <th>Transaction Type</th>
                                                        <th>Date</th>
                                                        <th style="width: 40px">Amount</th>
                                                        <th>Invoice ID(stripe)</th>
                                                        <th>Payout Status</th>
                                                        <th>Payout ID</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @if(!$transactions->isEmpty())
                                                    @foreach ($transactions as $transaction)
                                                    <tr>
                                                        <td>
                                                            <a href="{{route('admin-userdetails', ['username'=> $transaction->User->username])}}">{{  $transaction->User->username }}</a>
                                                        </td>
                                                        <td>{{ $transaction->TransactionType->name }}</td>
                                                        <td>{{ $transaction->created_at->toFormattedDateString() }}</td>
                                                        @if($transaction->User->is_influencer )
                                                        <td>
                                                            <span class="badge bg-success">+{{ $transaction->amount }}</span>
                                                        </td>
                                                        @else
                                                        <td>
                                                            <span class="badge bg-danger">+{{ $transaction->amount }}</span>
                                                        </td>
                                                        @endif

                                                        <td>@if($transaction->invoice_id) <a target="_blank" href="https://dashboard.stripe.com/payments/{{$transaction->invoice_id}}">Invoice</a> @else N/A @endif</td>
                                                        <td> @if($transaction->payout == 1) Paid @else Unpaid @endif</td>
                                                        <td> @if($transaction->payout) {{ $transaction->payout_id }} @else N/A @endif</td>
                                                        
                                                    </tr>
                                                    @endforeach
                                                    @else
                                                    <tr>
                                                        <th colspan="5">
                                                            <div class="mx-5
                                                                text-center">
                                                                <h4 class="px-5">No Transaction</h4>
                                                            </div>
                                                        </th>
                                                    </tr>
                                                    @endif
                                                </tbody>
                                            </table>
                                        </div>
                                        @if(!$transactions->isEmpty())
                                        <div class="pagination-sm ml-4
                                            float-right">
                                            {{ $transactions->links() }}
                                        </div>
                                        @endif
                                    </div><!-- /.card -->
                                </div>
                            </div>
                            {{-- Transactions End --}}

                            {{-- phone --}}
                            <div class="tab-pane" id="phone">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <h3 class="card-title">Phone</h3>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <form name="user-phone-update" method="post" action="{{route('admin-phone-update', $user->username)}}">
                                                @csrf
                                                <input class="form-control-sm" type="text" name="phone_no" value="@if($user->phone){{$user->phone->phone_no}}@else {{old('phone_no')}} @endif"/>
                                                
                                                <input type="submit" value="Update" class="btn btn-primary mr-4"> 
                                                @if($user->phone and $user->phone->is_verified) Verified @endif
                                                @if ($errors->has('phone_no'))
                                                    <div class="error">{{ $errors->first('phone_no') }}</div>
                                                @endif
                                            </form>
                                            @if($user->phone and $user->phone->phone_no)
                                            <form name="user-phone-update d-inline" method="post" action="{{route('admin-phone-delete', $user->username)}}" onsubmit="return confirm('Are you sure?')">
                                                @csrf
                                                @method('DELETE')
                                                <input type="submit" value="Delete" class="btn btn-danger">
                                            </form>
                                            @endif
                                        </div>
                                    </div><!-- /.card -->
                                </div>
                            </div>
                            {{-- phone End --}}

                            {{-- payouts User --}}
                            @if( $user->is_influencer )
                            <div class="tab-pane" id="payouts">
                                <div class="col-md-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <div class="float-left">
                                                <h3 class="card-title">Payouts List</h3>
                                            </div>
                                            <div class="float-left ml-5">
                                                <span class="ml-2">
                                                    Amount <span class="badge badge-success">${{ $user->getPayoutInfo('amount') }}
                                                    </span>
                                                </span>
                                                <span class="ml-2">
                                                    Fee <span class="badge badge-warning">${{ $user->getPayoutInfo('fee') }} </span>
                                                </span>
                                                <span class="ml-2">
                                                    Income <span class="badge badge-success">${{ $user->getPayoutInfo('income') }}</span></a>
                                                </span>

                                                <span class="ml-4"><a href="{{ route('admin-payout-create', $user->id)}}" class="btn btn-primary">Calculate Payouts</a></span>
                                            </div>
                                            <a href="{{ route('admin-payout-create', ['id'=> $user->id]) }}" class="btn btn-primary float-right">Add</a>
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body">
                                            <table id="example" class="table table-bordered
                                                table-striped">
                                                <thead>
                                                    <tr>
                                                        <td>Date</td>
                                                        <td>Invoice No</td>
                                                        <td>Remarks</td>
                                                        <td>Amount</td>
                                                        <td>Fee</td>
                                                        <td>Income</td>
                                                        <td>Status</td>
                                                        <th>Action</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @foreach ($payouts as $payout)
                                                    <tr>
                                                        <td>{{ $payout->created_at->toFormattedDateString() }}</td>
                                                        <td>{{ $payout->invoice_no }}</td>
                                                        <td>{{ $payout->remarks }}</td>
                                                       
                                                        <td><span class="badge badge-success">${{ $payout->amount }}</span></td>
                                                        <td>${{ $payout->fee }}</td>
                                                        <td>${{ $payout->income }}</td>
                                                         <td>
                                                            @if( $payout->status )
                                                            <span class="badge
                                                                badge-success">Paid</span>
                                                            @else
                                                            <span class="badge
                                                                badge-warning">Unpaid</span>
                                                            @endif
                                                        </td>
                                                        <td>
                                                            <a class="btn btn-primary" href="{{ route('admin-payout-edit', ['id'=> $payout->id]) }}">
                                                                <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="pagination-sm ml-4 float-right">
                                            {{ $payouts->links() }}
                                        </div>
                                    </div><!-- /.card -->
                                </div>
                            </div>
                            @endif
                            {{-- Transactions End --}}
                            

                            {{-- applyforinfluencer --}}

                            @if( $user->InfluencerApplication)
                            <div class="tab-pane" id="applyforinfluencer">
                                <div class="col-md-12">
                                    <div class="card">
                                        <!-- /.card-header -->
                                        <div class="card-body row">
                                            <div class="col-md-4">
                                                <!-- Widget: user widget style 2 -->
                                                <div class="card card-widget widget-user-2">
                                                    <!-- Add the bg color to the header using any of the bg-* classes -->
                                                    <div class="widget-user-header
                                                        bg-warning">
                                                        @if( $user->InfluencerApplication->verification_image
                                                        )
                                                        <div class="widget-user-image">
                                                            <img class="img-circle elevation-2" src="{{ asset("images/uploads/" .$user->InfluencerApplication->verification_image) }}"
                                                            style="height:80px;width:80px;border-radius:
                                                            100%;" alt="{{ $user->name }}">
                                                        </div>
                                                        @endif
                                                        <!-- /.widget-user-image -->
                                                        <h3 class="widget-user-username">{{ $user->first_name }} {{ $user->last_name }}</h3>
                                                        <h5 class="widget-user-desc">{{ $user->tagline }}</h5>
                                                    </div>
                                                    <div class="card-footer p-0">
                                                        <ul class="nav flex-column">

                                                            @if( $user->InfluencerApplication->facebook_url )
                                                            <li class="nav-item">
                                                                <a href="{{ $user->InfluencerApplication->facebook_url }}" target="_blank"
                                                                    class="nav-link">
                                                                    Facebook 
                                                                    <span class="float-right badge bg-primary"><i class="fa fa-facebook" aria-hidden="true"></i></span>
                                                                </a>
                                                            </li>
                                                            @endif

                                                            @if( $user->InfluencerApplication->twitter_url )
                                                            <li class="nav-item">
                                                                <a href="{{ $user->InfluencerApplication->twitter_url }}" target="_blank"
                                                                    class="nav-link">
                                                                    Twitter <span
                                                                        class="float-right
                                                                        badge bg-primary"><i
                                                                            class="fa
                                                                            fa-twitter"
                                                                            aria-hidden="true"></i></span>
                                                                </a>
                                                            </li>
                                                            @endif

                                                            @if( $user->InfluencerApplication->instagram_url )
                                                            <li class="nav-item">
                                                                <a href="{{ $user->InfluencerApplication->instagram_url }}" target="_blank" class="nav-link"> Instagram 
                                                                    <span class="float-right badge bg-primary">
                                                                        <i class="fa fa-instagram" aria-hidden="true"></i>
                                                                    </span>
                                                                </a>
                                                            </li>
                                                            @endif
                                                            <li>
                                                                <form class="row" method="post" action="{{route('admin-influencer-review-update', ['id'=> $user->id ])}}">
                                                                    @csrf
                                                                    <div class="input-group input-group-lg col-md-9 mt-3 mb-3 mx-auto">
                                                                        <div
                                                                            class="input-group-prepend">
                                                                            <select
                                                                                name="is_varified_influencer"
                                                                                class="form-control
                                                                                btn
                                                                                btn-warning
                                                                                dropdown-toggle">
                                                                                <option>Panding</option>
                                                                                <option
                                                                                    value="1"
                                                                                    @if(
                                                                                    $user->is_varified_influencer
                                                                                    == 1 )
                                                                                    selected="selected"
                                                                                    @endif
                                                                                    >Approve</option>
                                                                                <option
                                                                                    value="0"
                                                                                    @if(
                                                                                    $user->is_varified_influencer
                                                                                    == 0 )
                                                                                    selected="selected"
                                                                                    @endif
                                                                                    >Upapprove</option>
                                                                            </select>
                                                                        </div>
                                                                        <!-- /btn-group -->
                                                                        <input type="submit"
                                                                            class="btn
                                                                            btn-info
                                                                            btn-flat"
                                                                            value="update">
                                                                    </div>
                                                                </form>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <!-- /.widget-user -->
                                            </div>

                                            <div class="col-md-8">
                                                <div class="card card-primary">
                                                    <div class="card-header">
                                                        <h3 class="card-title">About Me</h3>
                                                    </div>
                                                    <!-- /.card-header -->
                                                    <div class="card-body">
                                                        <strong><i class="fa fa-file-text"
                                                                aria-hidden="true"></i>
                                                            Description</strong>
                                                        <p class="text-muted">
                                                            {{ $user->InfluencerApplication->description }}
                                                        </p>
                                                        <img class="elevation-2" src="{{ asset(" images/uploads/" .$user->InfluencerApplication->verification_image) }}" alt="{{ $user->name }}"><br />
                                                        <a class="btn btn-success mt-4
                                                            text-center" href="{{ asset("images/uploads/" .$user->InfluencerApplication->verification_image) }}" download>
                                                            Download
                                                        </a>
                                                    </div>
                                                    <!-- /.card-body -->
                                                </div>
                                            </div>



                                        </div>
                                    </div><!-- /.card -->
                                </div>
                            </div>
                            @endif
                            {{-- Transactions End --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endsection



    @section('scripts')
    <script>
    $(function() {

        var activetab = localStorage.getItem('userdetail_active_tab');
        if( activetab ){
            $('.nav-item a[href$="' + activetab + '"]').trigger('click');
        }

        $(document).on('click', '.nav-item a', function(){
            var href = $(this).attr('href');
            localStorage.setItem("userdetail_active_tab", href);
            console.log(href);
        })
        
        {{-- delete user  --}}
        $(document).on("click", '.user_delete_products', function(event) { 
            var userid = $(this).attr("user_id");
            
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    Swal.fire(
                        'Deleted!',
                        'Your file has been deleted.',
                        'success'
                    );

                    //ajax call
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "/admin/packages/"+userid,
                        type: 'DELETE', // replaced from put
                        dataType: "JSON",
                        data: {
                            "id": userid // method and token not needed in data
                        },
                        success: function (response){
                            console.log(response); // see the reponse sent
                            

                        },error: function(xhr) {
                            console.log(xhr.responseText); // this line will save you tons of hours while debugging
                            // do something here because of error
                        }
                    });
                }
                })
            });




    });
    </script>
    @endsection