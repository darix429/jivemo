@extends('layouts.admin')
@section('content')
    <div class="row">
         @if(session()->has('message'))
        <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert"
                aria-hidden="true">×</button>
            {{ session()->get('message') }}
        </div>
        @endif
        @if(session()->has('error'))
        <div class="alert alert-danger alert-dismissible col-12">
            <button type="button" class="close" data-dismiss="alert"
                aria-hidden="true">×</button>
            {{ session()->get('error') }}
        </div>
        @endif
        
         @if($data)
        <div class="col-md-6 d-none">
            <!-- DIRECT CHAT PRIMARY -->
            <div class="card card-primary card-outline direct-chat-primary">
              <div class="card-header">
                <h3 class="card-title">Basic</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              <form>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Email address</label>
                        <input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email">
                    </div>
                </div>

                <!-- /.card-body -->
                <div class="card-footer">
                <button type="submit" class="btn btn-primary">Save</button>
                </div>
              </form>
              <!-- /.card-footer-->
            </div>
            <!--/.direct-chat -->
        </div>

         <div class="col-md-3">
            <!-- DIRECT CHAT PRIMARY -->
            <div class="card card-success card-outline direct-chat-primary">
              <div class="card-header">
                <h3 class="card-title">Page Setting</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              
               
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{route('setting')}}" method="POST" enctype="multipart/form-data">
                       @csrf
                    <div class="form-group">
                        <label for="title">Page Title</label>
                        <input type="text" class="form-control" id="title" placeholder="Page title" name="page_title" value="{{ old('page_title', $data->page_title)}}">
                    </div>
                    <div class="form-group">
                        <label for="title">Description</label>
                        <input type="text" class="form-control" id="title" name='page_description' value="{{ old('page_description', $data->page_description)}}">
                    </div>
                    <div class="form-group">
                        <label for="tag">Page Meta Tag</label>
                        <input type="text" class="form-control" id="tag" name="page_metatag" value="{{ old('page_metatag', $data->page_metatag)}}">
                    </div>

                    <div class="form-group">
                        <label for="copyright">Copyright Text</label>
                        <input type="text" class="form-control" id="copyright" placeholder="Copyright" name="copyright" value="{{ old('copyright', $data->copyright)}}">
                    </div>


                    <div class="form-group">
                        <label for="logo">Front Logo</label>
                        @if( $data->logo)
                        <img src="{{ $data->logo }}" class="img img-small-thumbnail">
                        @endif
                        <input type="file" class="form-control" id="logo" placeholder="Logo" name="logo" value="{{ old('logo', $data->logo)}}">
                    </div>

                    <div class="form-group">
                        <label for="admin_logo">Admin Logo</label>
                        @if( $data->admin_logo)
                        <img src="{{ $data->admin_logo }}" class="img img-small-thumbnail">
                        @endif
                        <input type="file" class="form-control" id="admin_logo" placeholder="Logo" name="admin_logo" value="{{ old('admin_logo', $data->admin_logo)}}">
                    </div>
                    
                    <button type="submit" class="btn btn-primary">Save</button>
                    </form>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

        <div class="col-md-3">
            <!-- DIRECT CHAT PRIMARY -->
            <div class="card card-success card-outline direct-chat-primary">
              <div class="card-header">
                <h3 class="card-title">Fee</h3>

                <div class="card-tools">
                  <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                  </button>
                  <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                  </button>
                </div>
              </div>
              
                <!-- /.card-header -->
                <div class="card-body">
                    <form action="{{route('setting')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="msg_fee">Message Fee</label>
                            <input type="decimal" class="form-control" id="msg_fee" placeholder="Message Fee" name="msg_fee" value="{{ old('msg_fee', $data->msg_fee)}}">
                        </div>
                        <div class="form-group">
                            <label for="connection_fee">Connection Fee</label>
                            <input type="decimal" class="form-control" id="connection_fee" placeholder="Connection Fee" name="connection_fee" value="{{ old('connection_fee', $data->connection_fee)}}">
                        </div>
                        <div class="form-group">
                            <label for="connection_fee">Processing & Payout Fee(%)</label>
                            <input type="decimal" class="form-control" id="processing_fee_percent" placeholder="Connection Fee" name="processing_fee_percent" value="{{ old('processing_fee_percent', $data->processing_fee_percent)}}">
                        </div>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </form>                        
                </div>
                <!-- /.card-body -->
            </div>
            <!--/.direct-chat -->
        </div>
    
        @else
            <p>Database configuration missing. Please contact your technical person</p>
        @endif


       
    </div>
@endsection