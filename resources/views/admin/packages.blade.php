@extends('layouts.admin')
@section('title')
  Packages
@endsection
@section('content')
    <div class="row">

        @if(session()->has('message'))
            <div class="alert alert-success alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            {{ session()->get('message') }}
            </div>
        @endif


        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title pull-left">Packages</h3>
                    <a  href="{{route('admin-add-package', ['id' => $user->id])}}" class="btn btn-primary pull-right">Add Package</a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="packages" class="table table-bordered table-striped">
                        <thead> 
                            <tr>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Price</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

    </div>
@endsection
@section('scripts')
<script>
    $(function() {
        $('#packages').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('get.packages') !!}',
            columns: [
                { data: 'name', name: 'name' },
                { data: 'created_at', name: 'created_at' },
                { 
                    "data": "fee",
                    "render": function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<span class="badge badge-success">$'+ data + ' / month</span>';
                        }
                        return data;
                    }
                },
                { 
                    "data": "id",
                    "render": function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<a class="mr-3 btn btn-primary" href="/admin/packages/' + data + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><button type="button" class="btn btn-danger user_delete_products" data-toggle="modal" user_id="' + data + '" data-target="#delete_product' + data + '"> <i class="fa fa-trash" aria-hidden="true"></i> </button>';
                        }
                        return data;
                    }
                },
            ]
        });


        {{-- delete user  --}}
        $(document).on("click", '.user_delete_products', function(event) { 
            var ele = $(this);
            var userid = ele.attr("user_id");
            
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    // debugger;
                    //ajax call
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: "/admin/delete-package/"+ userid,
                        type: 'DELETE', // replaced from put
                        // dataType: "JSON",
                        data: {
                            '_token' : $('meta[name="csrf-token"]').attr('content'),
                            "id"     : userid,
                            "ajax"   :1
                        },
                        success: function (response){
                            ele.parents('tr').hide();
                            console.log(response); // see the reponse sent
                             Swal.fire(
                                'Deleted!',
                                'The package has been deleted.',
                                'success'
                            );
                        },error: function(xhr) {
                            console.log(xhr.responseText); 
                             Swal.fire(
                                'Failed!',
                                'Failed to delete package.',
                                'error'
                            );
                        }
                    });
                }
                })
            });

    });
    </script>
@endsection