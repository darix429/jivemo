@extends('layouts.admin')
@section('content')
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Users List</h3>
                    </div>

                    <!-- /.card-header -->
                    <div class="card-body">
                        <table class="table table-bordered" id="phonelogs">
                            <thead>
                                <tr>
                                    <th>From</th>
                                    <th>To</th>
                                    <th style="width:100px;" >Date</th>
                                    <th>Message</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
            </div>
            <!-- /.card-body -->
        </div>
    <!-- /.card -->
<!-- /.card -->
</div>
</div>
@endsection
                
@section('scripts')
<script>
    $(function() {
        $('#phonelogs').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('get.phonelogs') !!}',
            columns: [
                { 
                    "data": "from",
                    "render": function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<a href="/admin/users/' + data + '">' + data + '</a>';
                        }
                        return data;
                    }
                }, 
                { 
                    "data": "to",
                    "render": function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<a href="/admin/users/' + data + '">' + data + '</a>';
                        }
                        return data;
                    }
                }, 
                { data: 'created_at', name: 'created_at' },
                { data: 'message', name: 'message' },
            ]
        });
    });
    </script>
@endsection