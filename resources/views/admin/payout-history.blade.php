@extends('layouts.admin')
@section('title')
  Payouts History
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex flex-row justify-content-between">
                        <h3><a href="{{route('admin-payouts')}}">Back</a></h3>
                        <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                        <h3 class="modal-title">Payouts (<small>History</small>)</h3>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ session()->get('message') }}
                        </div>
                    @endif

                    <table id="payout" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Username</th>
                                <th>Period</th>
                                <th>Amount</th>
                                <th>Status</th>
                                <th>Date</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($collection as $data)
                                <tr>
                                    <td><a href='/admin/users/{{ $data->user->username }}'>({{$data->user_id}}) {{ $data->user->username }}</a></td>
                                    <td><a href='/admin/payout/history/info/{{ $data->id }}'>{{ date("M-Y", strtotime($data->period )) }}</a></td>
                                    <td>${{ number_format($data->income, 2) }}</td>
                                    <td>{{ $data->status ? "Paid": "Unpaid" }}</td>
                                    <td>{{ date("M d, Y", strtotime($data->created_at)) }}</td>
                                </tr>
                            @endforeach 
                        </tbody>
                        
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection