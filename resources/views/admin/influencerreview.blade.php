@extends('layouts.admin')
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <h3 class="card-title">Influencer Review List</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <table id="influencer" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Email</th>
                                <th>Status</th>
                                <th>Privacy</th>
                                <th>Details</th>
                            </tr>
                        </thead>
                    </tfoot>
                </table>
            </div>
            <!-- /.card-body -->
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#influencer').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('get.influencer') !!}',
            columns: [
                { 
                    "data": "user_id",
                    "render": function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<a href="/admin/users/' + data + '">' + data + '</a>';
                        }
                        return data;
                    }
                }, 
                { data: 'description', name: 'description' },
                { data: 'verification_image', name: 'verification_image' },
                { data: 'created_at', name: 'created_at' },
                { data: 'action', name: 'action' },
            ]
        });
    });
    </script>
@endsection