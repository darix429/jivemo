@extends('layouts.admin')
@section('title')
  Transactions
@endsection
@section('content')
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Transactions</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="transactions" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Date</th>
                                    <th>Buyer</th>
                                    <th>Package Name</th>
                                    <th>Influencer</th>
                                    <th>Amount</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>

        <!-- /.card -->
        </div>
    </div>
@endsection

@section('scripts')
<script>
    $(function() {
        $('#transactions').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('get.transactions') !!}',
            columns: [
                { data: 'created_at', 
                    "render": function(data, type, row, meta) {
                        var date = new Date(data);
                        var monthNames = [
                            "Jan", "Feb", "Mar",
                            "Apr", "May", "Jun", "Jul",
                            "Aug", "Sep", "Oct",
                            "Nov", "Dec"
                        ];
                        return monthNames[date.getMonth()] + " " + date.getDate() + ", " + date.getFullYear();
                    }
                },
                { 
                    data: "buyer",
                    "render": function(data, type, row, meta){
                        return data;
                    }
                }, 
                { data: 'package', name: 'package' },
                { data: 'influencer', name: 'influencer' },
                { data: 'amount', name: 'amount' }
            ],
            order: [
                [0, "DESC"]
            ]
        });
    });
    </script>
@endsection