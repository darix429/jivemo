@extends('layouts.admin')
@section('content')
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                
                    {{-- Phones list --}}
                    <!-- /.card-header -->
                        <div class="card-header">
                            <h3 class="card-title pull-left">Phones</h3>
                        </div>
                        <div class="card-body">
                            <table id="phones-table" class="table table-bordered table-striped" style="text-transform: capitalize;">
                                <thead>
                                    <tr>
                                        <th>User Name</th>
                                        <th>Phone No</th>
                                        <th>Statues</th>
                                        <th>Date</th>
                                        <th>Action </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->

                </div>
            <!-- /.card -->
        <!-- /.card -->
        </div>
    </div>
@endsection

@section('scripts')
<script>
        jQuery(function($) {
            $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('get.phones') !!}',
                columns: [
                    { 
                        "data": "username",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = '<a href="/admin/users/' + data + '">' + data + '</a>';
                            }
                            return data;
                        }
                    }, 
                    { data: 'phone', name: 'phone' },
                    { data: 'is_verified', name: 'is_verified' },
                    { 
                        "data": "id",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = '<a class="mr-3 badge bg-info" href="/admin/usersdetails/' + data + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><button type="button" class="user_delete_products badge bg-danger" data-toggle="modal" user_id="' + data + '" data-target="#delete_product' + data + '"> <i class="fa fa-trash" aria-hidden="true"></i> </button>';
                            }
                            return data;
                        }
                    },
                ]
            });

            {{-- delete user  --}}
            $(document).on("click", '.user_delete_products', function(event) { 
                var userid = $(this).attr("user_id");
                
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        Swal.fire(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        );

                        //ajax call
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "/admin/users/"+userid,
                            type: 'DELETE', // replaced from put
                            dataType: "JSON",
                            data: {
                                "id": userid // method and token not needed in data
                            },
                            success: function (response){
                                console.log(response); // see the reponse sent
                                

                            },error: function(xhr) {
                                console.log(xhr.responseText); // this line will save you tons of hours while debugging
                                // do something here because of error
                            }
                        });
                    }

                    
                    })
                });

                
            });
    </script>
@endsection