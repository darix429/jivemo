@extends('layouts.admin')
@section('title')
  Edit Template
@endsection
@section('content')


<div class="card">
<div class="card-header">
    <h3>Edit {{ $template->type=='sms'? "SMS" : "Email" }} Template</h3>
    <button type="button" class="btn btn-default"><a href="{{route('templates')}}">Back to list</a></button>
    <button type="submit" form="form" class="btn btn-primary pull-right">Save</button>
</div>
<div class="card-body">


<form id="form" action="{{ route('save-template') }}" method="post">
    @csrf
    <div>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" role="alert">{{$error}}</div>
            @endforeach
        @endif
        @if (session('success'))
            <div class="alert alert-success" role="alert">{{ session('success') }}</div>
        @endif
    </div>
    <input type="hidden" name="action" value="edit">
    <div class="form-group">
        <label for="type">ID</label>
        <input type="text" name="id" id="id" value="{{$template->id}}" readonly class="form-control">
    </div>
    <div class="form-group">
        <label for="type">Type</label>
        <input type="text" name="type" id="type" value="{{$template->type}}" readonly class="form-control">
    </div>
    <div class="form-group">
        <label for="type">Name</label>
        <input type="text" name="name" id="name" value="{{$template->name}}" class="form-control">
    </div>
    @if($template->type == 'email')
    <div class="form-group">
        <label for="type">Subject</label>
        <input type="text" name="subject" id="subject" value="{{$template->subject}}" class="form-control">
    </div>
    @endif
    <div class="form-group">
        <label for="type">Auto send on event</label>
        <select name="event" id="event" class="form-control">
            <option value="0" default>Manual</option> 
            @foreach($notification_events as $event)
                @if($template->type === $event->type)
                <option value="{{$event->id}}"  {{ $template->id == $event->template_id ? "selected":"" }}>{{$event->name}} - {{$event->description}}</option>  
                @endif
            @endforeach   
        </select>
    </div>
    <div class="form-group">
        <label for="type">Status</label>
        <select name="status" id="status" class="form-control">
            <option value="1" {{ $template->status? "selected": ""}}>Enabled</option>
            <option value="0"{{ !$template->status? "selected": ""}}>Disabled</option>
        </select>
    </div>
    <div class="form-group">
        <label for="template">Template Body</label>
        <textarea name="template" class="form-control" id="template" cols="30" rows="10" required placeholder="use [variable_name] for variable names">{{$template->template}}</textarea>
    </div>
</form>
</div>
</div>

@endsection