@extends('layouts.admin')
@section('title')
  New Sms Template
@endsection
@section('content')


<div class="card">
<div class="card-header">
    <h3>Add SMS Template</h3>
    <button type="button" class="btn btn-default"><a href="{{route('templates')}}">Back to list</a></button>
    <button type="submit" form="form" class="btn btn-primary pull-right">Save</button>
</div>
<div class="card-body">


<form id="form" action="{{ route('save-template') }}" method="post">
    @csrf
    <div>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" role="alert">{{$error}}</div>
            @endforeach
        @endif
        @if (session('success'))
            <div class="alert alert-success" role="alert">{{ session('success') }}</div>
        @endif
    </div>
    <input type="hidden" name="action" value="save">
    <div class="form-group">
        <label for="type">Type</label>
        <input type="text" name="type" id="type" value="sms" readonly class="form-control">
    </div>
    <div class="form-group">
        <label for="type">Name</label>
        <input type="text" name="name" id="name" value="{{old('name')}}" class="form-control">
    </div>
    <div class="form-group">
        <label for="type">Auto send on event</label>
        <select name="event" id="event" class="form-control">
            <option value="0" default>Manual</option> 
            @foreach($notification_events as $event)
                @if($event->type==='sms')
                <option value="{{$event->id}}">{{$event->name}}-{{$event->description}}</option>  
                @endif
            @endforeach   
        </select>
    </div>
    <div class="form-group">
        <label for="type">Status</label>
        <select name="status" id="status" class="form-control">
            <option value="1">Enabled</option>
            <option value="0">Disabled</option>
        </select>
    </div>
    <div class="form-group">
        <label for="template">Template Body</label>
        <textarea name="template" class="form-control" id="template" cols="30" rows="10" required placeholder="use [variable_name] for variable names">{{old('template')}}</textarea>
    </div>
</form>
</div>
</div>

@endsection