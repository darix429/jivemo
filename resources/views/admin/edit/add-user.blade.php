@extends('layouts.admin')
@section('title')
  New User
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <!-- /.card-header -->
                <div class="card-header">
                    <h3 class="card-title pull-left">Users add</h3>
                    <div class="user-list-primary  pull-right ">
                        <a  href="{{route('admin-add-user')}}" class="btn btn-primary active">Add User</a>
                        <a class="btn btn-primary" href="{{ route('admin-users') }}">All</a>
                        <a class="btn btn-primary  " href="{{ route('admin-users-influencer') }}">Influencer</a>
                        <a class="btn btn-primary" href="{{ route('admin-users-buyer') }}">Buyer</a>
                    </div>
                </div>
                <div class="card-body">
                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ session()->get('message') }}
                        </div>
                    @endif
                    
                    <div class="row">
                        <div class="col">
                            <div class="message-packages signup">

                                <div class="middle signup p-4">

                                    <form id="JimevoRegister" method="POST" action="{{ route('add-admin-user') }}" role="form" data-toggle="validator">
                                        @csrf

                                        <!-- username -->
                                        <div class="form-group">
                                            <input name ='username' type="text" placeholder="Username" class="form-control" value="{{ old('username') }}"  required autofocus data-minlength="6" data-maxlength="30" minlength="6" maxlength="30">
                                            @if ($errors->has('username'))
                                                <p class="text-danger"><strong>{{ $errors->first('username') }}</strong></p>
                                            @endif
                                        </div>


                                        <!-- email address -->
                                        <div class="form-group">
                                            <input type="email" name="email" class="form-control" id="email" placeholder="Email" value="{{ old('email') }}" data-error="Bruh, that email address is invalid" data-maxlength="70" maxlength="70" required>
                                            <div class="help-block with-errors"></div>
                                            @if ($errors->has('email'))
                                                <p class="text-danger"><strong>{{ $errors->first('email') }}</strong></p>
                                            @endif
                                        </div>


                                        <!-- phone number -->
                                        <div class="form-group">
                                            <input type="number" name="phone_no" class="form-control" id="phone_no" placeholder="Phone number" value="{{ old('phone_no') }}" data-error="Bruh, that phone number is invalid" data-maxlength="13" maxlength="13" required>
                                            <div class="help-block with-errors"></div>
                                            @if ($errors->has('phone_no'))
                                                <p class="text-danger"><strong>{{ $errors->first('phone_no') }}</strong></p>
                                            @endif
                                        </div>
                                        

                                        <!-- password -->
                                        <!-- <div class="form-group">
                                            <div class="form-group">
                                                <input type="password" name="password" data-minlength="6" data-maxlength="30" minlength="6" maxlength="30" class="form-control" id="inputPassword"  data-error="Minimum of 6 characters" placeholder="Password" required>
                                                @if ($errors->has('password'))
                                                    <p class="text-danger"><strong>{{ $errors->first('password') }}</strong></p>
                                                @endif
                                            </div>
                                            <div class="form-group">
                                                <input type="password" name="password_confirmation" data-minlength="6" data-maxlength="30"  minlength="6" maxlength="30" class="form-control" id="inputPasswordConfirm" data-match="#inputPassword" data-match-error="Whoops, these don't match" placeholder="Confirm Password" required>
                                                @if ($errors->has('password_confirmation'))
                                                    <p class="text-danger"><strong>{{ $errors->first('password_confirmation') }}</strong></p>
                                                @endif
                                            </div>
                                        </div> -->
                                        

                                        
                                        <!-- is_influencer -->
                                            <div class="form-group" style="clear:both">
                                            <div class="row">
                                                <input type="checkbox" name="is_influencer" class="float-left mr-2 mt-1" id="is_influencer" value="1">
                                                <label class="float-left">{{ __('Influencer?') }}</label>
                                            </div>
                                        </div> 

                                        <div class="form-group" style="clear:both">
                                            <div class="row">
                                                <input type="checkbox" name="is_active" class="float-left mr-2 mt-1" id="is_active" value="1" checked>
                                                <label class="float-left">{{ __('Active ?') }}</label>
                                            </div>
                                        </div> 

                                        <input type="submit" value="Add" class="btn btn-primary">
                                    </form>	
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection