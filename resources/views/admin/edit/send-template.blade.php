@extends('layouts.admin')
@section('title')
  Send Template
@endsection
@section('content')


<div class="card">
<div class="card-header">
    <h3>Send Template</h3>
    <button type="button" class="btn btn-default"><a href="{{route('templates')}}">Back to list</a></button>
    <button type="submit" form="form" class="btn btn-primary pull-right">Send</button>
</div>
<div class="card-body">


<form id="form" action="{{ route('send-template') }}" method="post">
    @csrf
    <div>
        @if ($errors->any())
            @foreach ($errors->all() as $error)
                <div class="alert alert-danger" role="alert">{{$error}}</div>
            @endforeach
        @endif
        @if (session('success'))
            <div class="alert alert-success" role="alert">{{ session('success') }}</div>
        @endif
    </div>
    <div class="form-group">
        <label for="to">To</label>
        <input type="to" name="to" id="to" placeholder="email address / phone number" required class="form-control">
    </div>
    <div class="form-group">
        <label for="type" required>Subject</label>
        <input type="text" name="subject" id="subject" value="" class="form-control">
    </div>
    <div class="form-group">
        <label for="template">Select Template</label>
        <select name="template" id="template" required class="form-control">
            @foreach($templates as $template)
                <option value="{{ $template->id }}">{{ $template->name }}</option>
            @endforeach
        </select>
    </div>
</form>
</div>
</div>

@endsection