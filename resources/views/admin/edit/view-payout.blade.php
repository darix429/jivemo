@extends('layouts.admin')
@section('title')
  Payout Details
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="modal-header">
                    <h3><a href="{{route('admin-payouts-history')}}">Back</a></h3>
                    <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="modal-body">
                            <div class="">
                                <strong>{{ $details["user"]->username }} - Payout Period {{date_format(date_create($payout->period ), "M-Y")}}</strong><br>
                                <p>
                                    Status: Paid<br>

                                    Invoice No: {{ $payout->invoice_no }} <br>
                                    Payout Amount: <span style="color: #2879FE; font-weight: 800;">${{ number_format($payout->income, 2) }}</span>
                                    
                                </p>

                                <p>
                                    Total Revenue: ${{ $details['revenue']}} <br>
                                    Phone Connection Fee: ${{ $details['connection_fee' ]}} <br>
                                    Messages Fee: ${{ $details['message_fee' ]}} <br>
                                    Processing Fee: ${{ $details['process_fee' ]}} <br>
                                    Total Fees: ${{ $details['fee' ]}}<br>

                                </p>
                            </div>

                            <hr>

                            <div class="">
                                <!-- Connection Details  -->
                                <table class="table">
                                    <thead>
                                        <tr>
                                            <th>Subscriber</th>
                                            <th>Package</th>
                                            <th>Charge date</th>
                                            <th>Amount</th>
                                            <th>Fees</th>
                                            <th>Payout</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    @foreach($details['payout_details'] as $detail)
                                        <tr>
                                            <td>{{ $detail['subscriber'] }}</td>
                                            <td>{{ $detail['package'] }}</td>
                                            <td>{{ $detail['charge_date'] }}</td>
                                            <td>{{ $detail['revenue'] }}</td>
                                            <td>{{ $detail['fee'] }}</td>
                                            <td>{{ $detail['income'] }}</td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>

                    </div>
                    
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection