@extends('layouts.admin')
@section('title')
  Edit Package
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="modal-header">
                    <h3><a href="{{route('admin-users')}}/{{ $package->user->username }}">Back</a></h3>
                    <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h3 class="modal-title">Edit Packages</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="modal-body">
                        <form method="post" class="form-horizontal" action="{{route('admin-package-update', ['id' => $package->id])}}">
                            @csrf
                            <input type="hidden" name="id" value="{{ $package->id }}">
                            <div class="form-group">
                                <label for="Name" class="col-sm-12 control-label">Name</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" value="{{ old('name', $package->name) }}" id="Name" name="name"  data-minlength="3" data-maxlength="150" minlength="3" maxlength="150"  >
                                </div>
                                @if ($errors->has('name'))
                                    <p class="text-danger"><strong>{{ $errors->first('name') }}</strong></p>
                                @endif
                            </div>
                            <div class="form-group">
                                <label for="description" class="col-sm-12 control-label">Description</label>
                                <div class="col-sm-12">
                                    <textarea class="form-control" id="description" rows="6" data-maxlength="250" maxlength="250" name="description">{{ old('description', $package->description) }}</textarea>
                                </div>
                                @if ($errors->has('description'))
                                    <p class="text-danger"><strong>{{ $errors->first('description') }}</strong></p>
                                @endif
                            </div>

                            <div class="form-group">
                                <label for="no_of_message" class="col-sm-12 control-label">Number of Messages Included Per Month</label>
                                <div class="col-sm-12">
                                    <div class="input-group mb-3">
                                        <input type="number" class="form-control" id="no_of_message" name="no_of_message" min="10" max="500" value="{{ old('no_of_message', $package->no_of_message) }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-commenting" aria-hidden="true"></i></i>  Message / Month </span>
                                        </div>
                                    </div>
                                    @if ($errors->has('no_of_message'))
                                        <p class="text-danger"><strong>{{ $errors->first('no_of_message') }}</strong></p>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label for="fee" class="col-sm-12 control-label">Monthly Subscription Fee Per User</label>
                                <div class="col-sm-12">
                                    <div class="input-group mb-3">
                                        <input type="number" name="fee"  step=".01" min="1" max="500" value="{{ old('fee', $package->fee) }}" name="fee" class="form-control">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-usd" aria-hidden="true"></i>  Month / User</span>
                                        </div>
                                    </div>
                                    @if ($errors->has('fee'))
                                        <p class="text-danger"><strong>{{ $errors->first('fee') }}</strong></p>
                                    @endif
                                </div>
                            </div>
                        
                            <div class="form-group">
                                <label for="max_person" class="col-sm-12 control-label">Maximum Number of People Who Can Subscribe</label>
                                <div class="col-sm-12">
                                    <div class="input-group mb-3">
                                        <input type="number" class="form-control" id="max_person" name="max_person"  min="1" max="500" value="{{ old('max_person', $package->max_person) }}">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><i class="fa fa-user" aria-hidden="true"></i>  User</span>
                                        </div>
                                    </div>
                                    @if ($errors->has('max_person'))
                                        <p class="text-danger"><strong>{{ $errors->first('max_person') }}</strong></p>
                                    @endif
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-12">
                                    <div class="checkbox">
                                        <label>
                                            <input name="term_and_conditions" value="1" type="checkbox"> I agree to the <a href="#">terms and conditions</a>
                                        </label><br />
                                        <label>
                                            <input name="is_active" value="1" type="checkbox" checked > is Active 
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-12">
                                    <button type="submit" class="btn btn-success">Update</button>
                                </div>
                            </div>
                        </form>                                
                    </div>
                    
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection