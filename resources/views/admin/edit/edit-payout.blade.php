@extends('layouts.admin')
@section('title')
  Edit Payout
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
                <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                {{ session()->get('message') }}
                </div>
            @endif
            
            <div class="card">
                <div class="modal-header">
                    <h3><a href="{{route('admin-payouts')}}">Back</a></h3>
                    <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h3 class="modal-title">Edit Packages</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="modal-body">
                        <form method="post" class="form-horizontal" action="{{ route('admin-payout-update', ['id' => $payout->id])}}">
                            @csrf
                            <div class="form-group">
                                <label for="amount" class="col-sm-12 control-label">Invoice No</label>
                                <div class="col-sm-12">
                                    <input type="text" class="form-control" value="{{  old('invoice_no', $payout->invoice_no) }}" id="invoice_no" name="invoice_no" readonly >
                                    @if ($errors->has('invoice_no'))
                                        <p class="text-danger"><strong>{{ $errors->first('invoice_no') }}</strong></p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="amount" class="col-sm-12 control-label">Amount</label>
                                <div class="col-sm-12">
                                    <input type="number" step="0.01" class="form-control" value="{{ old('amount', $payout->amount) }}" id="amount" name="amount"  min="0" max="1000" >                                   
                                    @if ($errors->has('amount'))
                                        <p class="text-danger"><strong>{{ $errors->first('amount') }}</strong></p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="fee" class="col-sm-12 control-label">Fee</label>
                                <div class="col-sm-12">
                                    <input type="number" step="0.01" class="form-control" value="{{ old('fee', $payout->fee) }}" id="fee" name="fee"  min="0" max="1000" >                                   
                                    @if ($errors->has('fee'))
                                        <p class="text-danger"><strong>{{ $errors->first('fee') }}</strong></p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="income" class="col-sm-12 control-label">Income</label>
                                <div class="col-sm-12">
                                    <input type="number" step="0.01" class="form-control" value="{{ old('income', $payout->income) }}" id="income" name="income"  min="0" max="1000" >                                   
                                    @if ($errors->has('income'))
                                        <p class="text-danger"><strong>{{ $errors->first('income') }}</strong></p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="remarks" class="col-sm-12 control-label">Remarks</label>
                                <div class="col-sm-12">
                                    <textarea type="text" class="form-control" id="remarks" name="remarks" data-maxlength="250" maxlength="250" >{{ old('remarks', $payout->remarks) }}</textarea>                                @if ($errors->has('remarks'))
                                    <p class="text-danger"><strong>{{ $errors->first('remarks') }}</strong></p>
                                    @endif
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Payment Status</label>
                                <select class="form-control" name="status" required>
                                    <option value="1" {{ ($payout->status == 1)? 'selected':'' }} >Paid</option>
                                    <option value="0" {{ ($payout->status == 0)? 'selected':'' }} >Unpaid</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-12">
                                    <button type="submit" class="btn btn-success">update</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection