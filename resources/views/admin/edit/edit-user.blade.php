@extends('layouts.admin')
@section('title')
  Edit User
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            @if(session()->has('message'))
                <div class="alert alert-success alert-dismissible">
                  <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                  {{ session()->get('message') }}
                </div>
            @endif
            <div class="card">
                <div class="modal-header">
                    <h3><a href="{{route('admin-users')}}/{{ $user->username }}">Back</a></h3>
                    <a href="#" data-dismiss="modal" class="class pull-right"><span class="glyphicon glyphicon-remove"></span></a>
                    <h3 class="modal-title">Edit User</h3>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                    <div class="modal-body">
                        <form method="post" id="editUserForm" class="form-horizontal" action="{{route('admin-user-update', ['id' => $user->id])}}"  enctype="multipart/form-data" >
                            @csrf
                            <input type="hidden" name="id" value="{{ $user->id }}">
                            <div class="form-row">
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <label for="Name" class="col-sm-12 control-label">First Name</label>
                                        <input type="text" name="first_name" value="{{ $user->first_name }}" class="form-control" data-maxlength="50" maxlength="50">
                                        @if ($errors->has('first_name'))
                                            <p class="text-danger"><strong>{{ $errors->first('first_name') }}</strong></p>
                                        @endif
                                    </div>
                                    <!-- /input-group -->
                                </div>
                                <!-- /.col-lg-6 -->
                                <div class="col-lg-6">
                                    <div class="input-group">
                                        <label for="Name" class="col-sm-12 control-label">Last Name</label>
                                        <input type="text" name="last_name" value="{{ $user->last_name }}" class="form-control" data-maxlength="50" maxlength="50">
                                        @if ($errors->has('last_name'))
                                            <p class="text-danger"><strong>{{ $errors->first('last_name') }}</strong></p>
                                        @endif
                                    </div>
                                    <!-- /input-group -->
                                </div>
                                <!-- /.col-lg-6 -->
                            </div>
                            <!-- /.row -->

                            <div class="row form-group mt-2">
                                <label for="Name" class="col-sm-12 control-label">About Me</label>
                                <div class="col-sm-12">
                                    <textarea class="form-control" name="about_me" rows="3" placeholder="Enter ..." data-maxlength="500" maxlength="500">{{ $user->about_me }}</textarea>
                                    @if ($errors->has('about_me'))
                                        <p class="text-danger"><strong>{{ $errors->first('about_me') }}</strong></p>
                                    @endif
                                </div>
                            </div>
                            
                            <div class="form-group row">
                                <label for="email" class="col-lg-12">Email Address</label>
                                <input type="text" name="email" value="{{ $user->email }}" class="form-control" data-maxlength="64" maxlength="64">
                            </div>
                            
                            @if( $user->is_influencer )
                            <div class="form-group">
                                <label for="Name" class="col-sm-12 control-label">Let's Text About(What people can message you about)</label>
                                <div class="col-sm-12">
                                    <textarea class="form-control" name="talk_about" rows="3" placeholder="Enter ..." data-maxlength="500" maxlength="500">{{ $user->talk_about }}</textarea>
                                    @if ($errors->has('talk_about'))
                                        <p class="text-danger"><strong>{{ $errors->first('talk_about') }}</strong></p>
                                    @endif
                                </div>
                            </div>
                            @endif


                            <div class="form-group">
                                <label for="Name" class="col-sm-12 control-label">Profile Privacy</label>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="profile_privacy" value="1" @if( $user->profile_privacy ) checked="checked" @endif   >
                                    <label class="form-check-label">Public</label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="profile_privacy"  value="0" @if( !$user->profile_privacy ) checked="checked" @endif >
                                    <label class="form-check-label">Private</label>
                                </div>
                            </div>

                            {{--  Profile Image  --}}
                            <div class="form-group row">
                                <label for="profile_image">Profile Image </label>
                                <div class="custom-file">
                                    <input type="file" class="custom-file-input" id="profile_image" name="profile_image">
                                    <label class="custom-file-label col-lg-4" for="profile_image">Choose Profile Image</label>
                                    </div>
                                    <div class="input-group-append">
                                </div>
                            </div>

                            <div class="row" style="width: 500px;">
                                <img alt="preview" id="preview" style="display: none;">
                            </div>
                            {{-- End  Profile Image  --}}

                            {{--  User Status  --}}
                            <div class="form-group row">
                                <label  for="User Status" class="col-lg-12">User Status</label>
                                <select name="is_active" class="form-control  col-lg-4">
                                    <option value="1" @if( $user->is_active ) selected="selected" @endif  >Activate</option>
                                    <option value="0" @if( !$user->is_active ) selected="selected" @endif  >Deactivate</option>
                                </select>
                                </div>
                            {{-- End  User Status  --}}


                            {{--  User Profile Type  --}}
                            <div class="form-group row">
                                <label  for="User Profile Type" class="col-lg-12">Profile Type</label>
                                <select name="is_influencer" class="form-control  col-lg-4">
                                    <option value="0" @if( !$user->is_influencer ) selected="selected" @endif  >General</option>
                                    <option value="1" @if( $user->is_influencer ) selected="selected" @endif  >Influencer</option>
                                </select>
                                </div>
                            {{-- End  User Profile Type  --}}


                            {{--  User Status  --}}
                            <div class="form-group row">
                                <button type="submit" class="btn btn-success">Update Profile</button>
                                </div>
                            {{-- End  User Status  --}}

                        </form>
                    </div>
                    
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection
@section('scripts')
<!-- cropper -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.css" />
<script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.js"></script>

<script type='text/javascript'>
    var cropper = null;
    var posX, posY;

    // image selection
    $("#profile_image").on("change", function(e) {
        var preview = document.getElementById("preview");
        preview.src = URL.createObjectURL(event.target.files[0]);
        if (cropper != null) {
            cropper.destroy();
        }
        cropper = new Cropper(preview, {
            viewMode: 2,
            aspectRatio: 2/2,
            movable: false,
            zoomable: false,
            minCropBoxHeight: 30,
            minCropBoxWidth: 30,
            autoCropArea: 1,
            crop(event) {
                posX = event.detail.x;
                posY = event.detail.y;
            }
        });
        $(preview).show();
    });

    $("#editUserForm").submit(function(e){
        e.preventDefault();

        if(cropper != null) {
            try {
                console.log("cropped available > use ajax");

                const form = $("#editUserForm");
                const formData = form.serializeArray();
                const postUrl = form.attr("action");

                var postDataString = "{";
                formData.forEach(data => {
                    postDataString += '"'+data.name+'":"'+data.value+'",';
                });
                postDataString = postDataString.slice(0,-1);
                postDataString += "}";

                // scale down large image
                const cropWidth = cropper.getData().width;
                cropper.moveTo(posX,posY);
                if (cropWidth > 400) {
                    var scale = 150/cropWidth;
                    cropper.scaleX(scale);
                    cropper.scaleY(scale);
                }
                cropper.getCroppedCanvas().toBlob(function(blob){
                    var postData = new FormData(document.getElementById("editUserForm"));
                    postData.delete("profile_image");
                    postData.append("profile_image", blob, "image.png");
                    
                    // submit form
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    $.ajax({
                        url: postUrl,
                        type: "POST",
                        data: postData,
                        processData: false,
                        contentType: false
                    }).done(function(res,status,xhr) {
                        console.log(res); // see the reponse sent
                        Swal.fire(
                            'User Update',
                            'The user has been updated!',
                            'success'
                        );
                    }).fail(function(xhr, status, er) {
                        console.log(xhr.responseText); 
                    });
                });
            } catch (er) {
                console.log("ERROR", er);
            }
        } else {
            console.log("nothing cropped");
            $(this).submit();
        }
    });

</script>
@endsection