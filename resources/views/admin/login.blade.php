@extends('layouts.app')
@section('title')
  Login or Register
@endsection
@section('content')

    <div class="hero-image-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
        <div class="row">
            <div class="jivemo-profile-bottom package-page phone-page pb-4 col">
                <div class="jivemo-profile-bottom-outer-wrapper phone-page signup mx-auto col-11 pb-lg-5 pb-md-4 pb-sm-4 pb-3 ">
                    <div class="profile-bottom-container">
                        <div class="jivemo-profile-bottom-inner-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
                            <div class="row">
                                
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 mx-auto">
                                    <h3 class="text-dark col mx-auto">{{ __('Login') }}</h3>
                                    <div class="message-packages signup">
                                        <div class="middle signup right p-4 mt-lg-0 mt-md-0 mt-sm-4 mt-4">
                                            <form id="JimevoRegister" method="POST" action="{{ route('login') }}"  role="form" data-toggle="validator" >
                                                @csrf

                                                <!-- email -->
                                                <div class="form-group">
                                                    <input name="email" type="text" class="form-control" id="name" placeholder="E-Mail / Username"  value="{{ old('email') }}"  data-error="Enter username or email address"  required autofocus data-maxlength="70" maxlength="70">
                                                    <div class="help-block with-errors"></div>
                                                </div>


                                                 <!-- password -->
                                                <div class="form-group">
                                                    <input  id="password"  type="password" placeholder="Password"    name="password" required data-maxlength="30" maxlength="30">
                                                    @if ($errors->has('email'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('email') }}</strong>
                                                        </span>
                                                    @endif
                                                </div>


                                                @if (Route::has('password.request'))
                                                    <div class="forgot-password">
                                                        <a href="{{ route('password.request') }}">
                                                            {{ __('Forgot Password?') }}
                                                        </a>
                                                    </div>
                                                @endif


                                                <input type="submit" value="Log In">
                                            </form>	
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection