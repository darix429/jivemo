@extends('layouts.admin')
@section('title')
  Users
@endsection
@section('content')
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                
                    {{-- user list --}}
                    <!-- /.card-header -->
                        <div class="card-header">
                            <h3 class="card-title pull-left">Users List </h3>
                            
                            <div class="user-list-primary  pull-right ">
                                <a  href="{{route('admin-add-user')}}" class="btn btn-primary">Add User</a>
                                <a class="btn btn-primary active" href="{{ route('admin-users') }}">All</a>
                                <a class="btn btn-primary" href="{{ route('admin-users-influencer') }}">Influencer</a>
                                <a class="btn btn-primary" href="{{ route('admin-users-buyer') }}">Buyer</a>
                            </div>
                        </div>
                        <div class="card-body">
                            <table id="users-table" class="table table-bordered table-striped" style="text-transform: capitalize;">
                                <thead>
                                    <tr>
                                        <th>Id</th>
                                        <th>username</th>
                                        <th>Email</th>
                                        <th>Phone</th>
                                        <th>Date</th>
                                        <th>Action </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <!-- /.card-body -->

                </div>
            <!-- /.card -->
        <!-- /.card -->
        </div>
    </div>
@endsection

@section('scripts')
<script>
    var dataTable;
        jQuery(function($) {
            
            dataTable = $('#users-table').DataTable({
                processing: true,
                serverSide: true,
                ajax: '{!! route('get.userlist') !!}',
                columns: [
                    { data: 'id', name: 'id' },
                    { 
                        "data": "username",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = '<a href="/admin/users/' + data + '">' + data + '</a>';
                            }
                            return data;
                        }
                    }, 
                    { data: 'email', name: 'email' },
                    { data: 'phone_no', name: 'phone_no', defaultContent: '' },
                    { data: 'created_at', name: 'created_at' },
                    { 
                        "data": "id",
                        "render": function(data, type, row, meta){
                            if(type === 'display'){
                                data = '<a class="mr-3 badge bg-info" href="/admin/usersdetails/' + data + '"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><button type="button" class="user_delete_products badge bg-danger" data-toggle="modal" user_id="' + data + '" data-target="#delete_product' + data + '"> <i class="fa fa-trash" aria-hidden="true"></i> </button>';
                            }
                            return data;
                        }
                    },
                ]
            });

            {{-- delete user  --}}
            $(document).on("click", '.user_delete_products', function(event) { 
                var userid = $(this).attr("user_id");
                
                Swal.fire({
                    title: 'Are you sure?',
                    text: "You won't be able to revert this!",
                    type: 'warning',
                    showCancelButton: true,
                    confirmButtonColor: '#3085d6',
                    cancelButtonColor: '#d33',
                    confirmButtonText: 'Yes, delete it!'
                }).then((result) => {
                    if (result.value) {
                        //ajax call
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                            }
                        });
                        $.ajax({
                            url: "/admin/delete-user/"+userid,
                            type: 'DELETE', // replaced from put
                            dataType: "JSON",
                            // data: {
                            //     "id": userid // method and token not needed in data,
                            // },
                            success: function (response){
                                console.log(response); // see the reponse sent
                                Swal.fire(
                                    'Deleted!',
                                    'The user has been deleted',
                                    'success'
                                );
                                dataTable.ajax.reload();
                            },error: function(xhr) {
                                console.log(xhr.responseText); // this line will save you tons of hours while debugging
                                // do something here because of error
                            }
                        });
                    }

                    
                    })
                });

                
            });
    </script>
@endsection