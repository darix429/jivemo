@extends('layouts.admin')
@section('title')
  Connections
@endsection
@section('content')
    <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h3 class="card-title">Connections</h3>
                    </div>
                    <!-- /.card-header -->
                    <div class="card-body">
                        <table id="connections" class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Seller</th>
                                    <th>Buyer</th>
                                    <th>Texting Package</th>
                                    <th>Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.card-body -->
                </div>
    </div>
@endsection
@section('scripts')
<script>
    $(function() {
        $('#connections').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('get.connections') !!}',
            columns: [
                { 
                    "data": "seller_id",
                    "render": function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<a href="/admin/users/' + data + '">' + data + '</a>';
                        }
                        return data;
                    }
                }, 
                { 
                    "data": "user_id",
                    "render": function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<a href="/admin/users/' + data + '">' + data + '</a>';
                        }
                        return data;
                    }
                }, 
                { data: 'package_id', name: 'package_id' },
                { data: 'created_at', name: 'created_at' },
                { 
                    "data": "id",
                    "render": function(data, type, row, meta){
                        if(type === 'display'){
                            data = '<button type="button" class="btn btn-danger user_delete_products" data-toggle="modal" user_id="' + data + '" data-target="#delete_product' + data + '"> <i class="fa fa-trash" aria-hidden="true"></i> </button>';
                        }
                        return data;
                    }
                },
            ]
        });

        {{-- delete user  --}}
        $(document).on("click", '.user_delete_products', function(event) { 
            var ele = $(this);
            var userid = ele.attr("user_id");
             
            Swal.fire({
                title: 'Are you sure?',
                text: "You won't be able to revert this!",
                type: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#3085d6',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Yes, delete it!'
            }).then((result) => {
                if (result.value) {
                    //ajax call
                    $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                        }
                    });
                    
                    var url = "/admin/connections/"+ userid+"/";

                    $.ajax({
                        url: url,
                        type: 'DELETE', // replaced from put
                        // dataType: "JSON",
                        data: {
                            '_token' : $('meta[name="csrf-token"]').attr('content'),
                            "id"     : userid,
                            "ajax"   :1
                        },
                        success: function (response){
                            ele.parents('tr').hide();
                             Swal.fire(
                                'Connection Delete',
                                'Connection has been successfully deleted.',
                                'success'
                            );
                        },error: function(xhr) {
                             Swal.fire(
                                'Connection Delete',
                                'A problem has been encountered while trying to delete the connection.',
                                'error'
                            );
                        }
                    });
                }
                })
            });




    });
    </script>
@endsection