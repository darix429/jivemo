@extends('layouts.admin')
@section('title')
  Templates
@endsection
@section('content')
    <div class="row">

        @if(session()->has('message'))
        @endif


        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex flex-row justify-content-between">
                        <div class="flex-grow-1"><h3 class="card-title pull-left">Templates</h3></div>
                        <div class="ml3">&nbsp;<a  href="{{route('send-template')}}" class="btn btn-success pull-right">Send Template</a>&nbsp;</div>
                        <div class="ml3">&nbsp;<a  href="{{route('add-email-template', ['id' => $user->id])}}" class="btn btn-primary pull-right">Add Email Template</a>&nbsp;</div>
                        <div class="ml3">&nbsp;<a  href="{{route('add-sms-template', ['id' => $user->id])}}" class="btn btn-primary pull-right">Add SMS Template</a>&nbsp;</div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">
                
                    @if (session('success'))
                        <div class="alert alert-success" role="alert">{{ session('success') }}</div>
                    @endif

                    <table id="packages" class="table table-bordered table-striped">
                        <thead> 
                            <tr>
                                <th>Id</th>
                                <th>Type</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($templates as $template)
                                <tr>
                                    <td>{{ $template->id }}</td>
                                    <td>{{ $template->type }}</td>
                                    <td>{{ $template->name }}</td>
                                    <td>{{ $template->status ? "Enabled" : "Disabled" }}</td>
                                    <td>
                                    @if($template->type == "sms")
                                        <button><a href="{{route('edit-template', [$template->id])}}">Edit</a></button>
                                    @else
                                        <button><a href="{{route('edit-email-template', [$template->id])}}">Edit</a></button>
                                    @endif
                                        <button><a href="{{route('delete-template', [$template->id])}}">Delete</a></button>
                                    </td>
                                </tr>
                            @endforeach                        
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>

    </div>
@endsection