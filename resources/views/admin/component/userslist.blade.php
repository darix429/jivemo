<div class="col-md-6">
    <!-- USERS LIST -->
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Latest Members</h3>
            <div class="card-tools">
                <span class="badge badge-danger">{{ $register_user_count }} Members</span>
                <button type="button" class="btn btn-tool" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-tool" data-widget="remove"><i class="fa fa-times"></i>
                </button>
            </div>
        </div>
        <!-- /.card-header -->
        <div class="card-body p-0">
        <ul class="users-list clearfix">
            @foreach ($user_list as $user)
                <li>
                        <a class="users-list-name" href="{{route('admin-users')}}/{{ $user->username }}">
                    @if( $user->profile_image )
                        <img src="{{asset("images/uploads/".$user->profile_image)}}" style="height:70px;width:70px;border-radius: 100%;"  alt="{{ $user->name }}">
                    @else
                        <img src="/images/user-icons.png" style="border-radius: 100%;"  alt="{{ $user->name }}">
                    @endif
                        </a>
                    <a class="users-list-name" href="{{route('admin-users')}}/{{ $user->username }}">{{ $user->username }}</a>
                    <span class="users-list-date">{{ $user->created_at->diffForHumans() }}</span>
                </li>
            @endforeach
        </ul>
        <!-- /.users-list -->
        </div>
        <!-- /.card-body -->
        <div class="card-footer text-center">
        <a href="{{ route('admin-users') }}">View All Users</a>
        </div>
        <!-- /.card-footer -->
    </div>
    <!--/.card -->
    </div>