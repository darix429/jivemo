<!-- /.card-header -->
<div class="card-header">
    <h3 class="card-title pull-left">Users List</h3>
    <div class="user-list-primary  pull-right ">
        <a  href="{{route('admin-add-user')}}" class="btn btn-primary">Add User</a>
        <a class="btn btn-primary" href="{{ route('admin-users') }}">All</a>
        <a class="btn btn-primary {{ Request::path() == 'admin/users/influencer' ? 'active' : '' }} " href="{{ route('admin-users-influencer') }}">Influencer</a>
        <a class="btn btn-primary {{ Request::path() == 'admin/users/buyer' ? 'active' : '' }}" href="{{ route('admin-users-buyer') }}">Buyer</a>
    </div>
</div>
<div class="card-body">
    <table id="users-table" class="table table-bordered table-striped">
        <thead>
            <tr>
                <th>Id</th>
                <th>username</th>
                <th>Email</th>
                <th>Date</th>
                <th>Action </th>
            </tr>
        </thead>
    </table>
</div>