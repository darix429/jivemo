<div class="col-lg-6">
    <div class="card">
        <div class="card-header">
            <h3 class="card-title">Recently Added Packages</h3>

            <div class="card-tools">
            <button type="button" class="btn btn-tool" data-widget="collapse">
                <i class="fa fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-widget="remove">
                <i class="fa fa-times"></i>
            </button>
            </div>
        </div>

        <!-- /.card-header -->
        <div class="card-body p-0">
            <ul class="products-list product-list-in-card pl-2 pr-2">
            
                {{-- item --}}
                @foreach ($packages as $package)
                    <li class="item">
                        <div class="product-img">
                                <img src="{{asset("images/uploads/".$package->user->profile_image)}}" style="height:43px;width:43px;border-radius: 100%;"  alt="{{ $package->name }}">
                        </div>
                        <div class="product-info">
                        <a href="{{route('admin-users')}}/{{ $package->user->username }}" class="product-title">{{ $package->name }}
                            @if( $package->fee )
                                <span class="badge badge-warning float-right">${{ $package->fee }}/month</span></a>
                            @endif
                            <span class="users-list-date">{{ $package->created_at->diffForHumans() }} / <strong>Limited to {{ $package->max_person }} People {{$package->max_person - $package->subscribers_count}} remaining ({{ $package->subscribers_count }}/{{ $package->max_person}})</strong></span>
                            
                        </div>
                    </li>

                @endforeach
                <!-- /.item -->
            </ul>
        </div>
        <!-- /.card-body -->
        <div class="card-footer text-center">
            <a href="{{route('admin-packages')}}" class="uppercase">View All Products</a>
        </div>
        <!-- /.card-footer -->
        </div>

    </div>