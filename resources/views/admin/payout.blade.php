@extends('layouts.admin')
@section('title')
  Payouts
@endsection
@section('content')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex flex-row justify-content-between">
                        <div class="flex-grow-1"><h3 class="card-title">Payout</h3></div>
                        <div class="ml3"><a  href="{{route('admin-payouts-history')}}" class="btn btn-primary pull-right">History</a></div>
                    </div>
                </div>
                <!-- /.card-header -->
                <div class="card-body">

                    @if(session()->has('message'))
                        <div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        {{ session()->get('message') }}
                        </div>
                    @endif


                    <table id="payout" class="table table-bordered table-striped">
                        <thead>
                            <tr>
                                <td>Username</td>
                                <td>Period</td>
                                <td>Income</td>
                                <td>Status</td>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($collection as $payout)
                                <tr>
                                    <td><a href='/admin/users/{{ $payout["user"]->username }}'>({{$payout["user"]->id}}) {{ $payout["user"]->username }}</a></td>
                                    <td>
                                        <a href="{!! route('admin-payout-info', ['date'=>$payout['date_to'], 'id'=>$payout['user']->id]) !!}">
                                        {{date_format(date_create($payout['date_from']), "M-Y")}}
                                        </a>
                                    </td>
                                    <td>${{ $payout["income"] }}</td>
                                    <td>unpaid</td>
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
                <!-- /.card-body -->
            </div>
        </div>
    </div>
@endsection


@section('scripts')

@endsection