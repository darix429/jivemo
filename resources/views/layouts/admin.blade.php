<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>JiveMo Admin - @yield('title')</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <meta name="csrf-token" content="{{ csrf_token() }}">
   <link rel="stylesheet" href="/css/adminlte.min.css">
   
   <link rel="stylesheet" href="{{ asset('plugins/font-awesome/css/font-awesome.min.css') }}">
   <link rel="stylesheet" href="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.css') }}">
   <link rel="stylesheet" href="{{ asset('plugins/datepicker/datepicker3.css') }}">
   <link rel="stylesheet" href="{{ asset('plugins/datatables/dataTables.bootstrap4.css') }}">
   <!-- <link rel="stylesheet" href="{{ asset('plugins/morris/morris.css') }}"> -->
   <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/dist/sweetalert2.css') }}">
   <link href="https://cdn.quilljs.com/1.3.6/quill.snow.css" rel="stylesheet">
   <link rel="stylesheet" href="/css/admin-jivemo.css">

   

  <script src="{{ asset('plugins/jquery/jquery.min.js') }}"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{ asset('plugins/jQueryUI/jquery-ui.min.js') }}"></script>
   
  <script src="https://cdn.tiny.cloud/1/b6eckolp3kwdfxfkc50g5upp4cf3vxw1zfcbsgxt1u4zkc1p/tinymce/5/tinymce.min.js"></script>

  <script>
  $(document).ready(function(){
    tinymce.init({
    selector: '#template-editor',
    height: 500,
    menubar: false,
    toolbar: 'undo redo | formatselect | bold italic backcolor | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | removeformat | help'
    });
  })
  </script>
   
</head>
<body class="hold-transition sidebar-mini">
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand bg-white navbar-light border-bottom">
    <!-- Left navbar links -->
    <!-- <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#"><i class="fa fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/admin" class="nav-link">Dashboard</a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="/" target="_blank" class="nav-link">Home</a>
      </li>
    </ul> -->

    <!-- SEARCH FORM -->
    <form class="form-inline ml-3" method="post" action="{{route('admin-users')}}">
        <div class="input-group input-group-sm">
              @csrf
              <input type="hidden" name="id" value="">
              <input class="form-control form-control-navbar" name="search" type="search" placeholder="Search" aria-label="Search">
              <div class="input-group-append">
                <button class="btn btn-navbar" type="submit">
                  <i class="fa fa-search"></i>
                </button>
              </div>
        </div>
    </form>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
      <li class="nav-item">
          <a class="nav-link" href="/logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
      </li>
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{route('admin')}}" class="brand-link">
      <img src="{{ config('site_setting.admin_logo')}}" alt="JiveMo Logo" class="brand-image" style="opacity: .8">
      <span class="brand-text font-weight-light">&nbsp;</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
       @if(Auth::user()->profile_image)
        <div class="image">
          <img src="{{asset("images/uploads/".Auth::user()->profile_image)}}" style="height:30px;width:30px;border-radious:100%;" class="circle p-0 img-circle elevation-2"/>
        </div>
        @endif
       
        <div class="info">
          <a href="{{route('admin')}}" class="d-block">{{ Auth::user()->name }}</a>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          

          <li class="nav-item has-treeview">
              <a href="{{route('admin-users')}}" class="nav-link  {{ Request::path() == 'admin/users' ? 'active' : '' }} ">
                <i class="fa fa-users" aria-hidden="true"></i>
                  <p> Users</p>
            </a>
          </li>
          
          
          <li class="nav-item has-treeview">
            <a href="{{route('admin-transactions')}}" class="nav-link   {{ Request::path() == 'admin/transactions' ? 'active' : '' }} ">
                <i class="fa fa-usd" aria-hidden="true"></i>
              <p>
                Transactions List
              </p>
            </a>
            <li class="nav-item has-treeview">
                <a href="{{route('admin-connections')}}" class="nav-link    {{ Request::path() == 'admin/connections' ? 'active' : '' }} ">
                    <i class="fa fa-commenting-o" aria-hidden="true"></i>
                    <p>
                      Connections List
                  </p>
              </a>
            </li>
            <li class="nav-item has-treeview">
                <a href="{{route('admin-phonelogs')}}" class="nav-link  {{ Request::path() == 'admin/phonelogs' ? 'active' : '' }}">
                  <i class="fa fa-phone" aria-hidden="true"></i>
                    <p>
                      Phone Logs
                  </p>
              </a>
            </li>

            <li class="nav-item has-treeview">
                <a href="{{route('admin-influencer-review')}}" class="nav-link   {{ Request::path() == 'admin/influencer-review' ? 'active' : '' }}">
                  <i class="fa fa-user-plus" aria-hidden="true"></i>
                    <p> Influencer Review</p>
              </a>
            </li>

            <li class="nav-item has-treeview">
                <a href="{{route('admin-packages')}}" class="nav-link  {{ Request::path() == 'admin/packages' ? 'active' : '' }}">
                  <i class="fa fa-folder" aria-hidden="true"></i>
                    <p>Packages</p>
              </a>
            </li>

            <li class="nav-item has-treeview">
                <a href="{{route('admin-payouts')}}" class="nav-link  {{ Request::path() == 'admin/payout' ? 'active' : '' }}">
                  <i class="fa fa-money" aria-hidden="true"></i>
                    <p>Payout</p>
              </a>
            </li>

            <li class="nav-item">
                <a href="{{route('setting')}}" class="nav-link  {{ Request::path() == 'admin/setting' ? 'active' : '' }}">
                  <i class="fa fa-cog" aria-hidden="true"></i>
                    <p>Settings</p>
              </a>
            </li>

            <li class="nav-item has-treeview">
              <a href="{{route('templates')}}" class="nav-link  {{ Request::path() == 'admin/templates' ? 'active' : '' }}">
                <!-- <i class="fa fa-mobile" aria-hidden="true"></i> -->
                <p>Templates</p>
              </a>
            </li>
         
          
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper" id="app">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <!-- <h1 class="m-0 text-dark">Dashboard</h1> -->
          </div><!-- /.col -->
          <div class="col-sm-6">
            <!-- <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v2</li>
            </ol> -->
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content mb-2">
      <div class="container-fluid">
       @yield('content')
       
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>{{ config('site_setting.copyright') }} <a href="http://jivemo.com">JiveMo</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<script>
    window.auth_user = {!! json_encode($auth_user); !!};
</script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->

<script src="{{ asset('plugins/knob/jquery.knob.js') }}"></script>
<script src="{{ asset('plugins/sparkline/jquery.sparkline.min.js') }}"></script>
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-1.2.2.min.js') }}"></script>
<script src="{{ asset('plugins/jvectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('plugins/slimScroll/jquery.slimscroll.min.js') }}"></script>
<script src="{{ asset('plugins/datepicker/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('plugins/datatables/jquery.dataTables.js') }}"></script>
<script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.js') }}"></script>



<!-- Morris.js charts -->
<script src="https://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.0.4/popper.js"></script>
<script src="{{ asset('plugins/morris/morris.min.js') }}"></script>

<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script src="{{ asset('js/adminlte.min.js') }}"></script>
<script src="{{ asset('js/dashboard.js') }}"></script>



  @yield('scripts')
</body>
</html>
