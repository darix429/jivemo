<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>JiveMo | @yield('title')</title>
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style1.css') }}" rel="stylesheet">
    <style>
    input::-webkit-outer-spin-button,
    input::-webkit-inner-spin-button {
        -webkit-appearance: none;
        margin: 0;
    }
    input[type=number] {
        -moz-appearance:textfield;
    }
    </style>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
</head>
<body>
<body class="container-fluid p-0">
	<!-- nav-menu -->
    <!-- @include("frontend.header.header") -->
    <div class="jivemo-nav">
        <div class="nav-container">
            <div class="row">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <div class="logo">
                            <a href="{{ url('/') }}">
                            <img src="{{ config('site_setting.logo', '/images/logo.png') }}" alt="logo">
                            </a>
                        </div>
                        
                        <button class="navbar-toggler d-none" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        

                        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                                <li>
                                    <a class="nav-link" href="/">Home</a>
                                </li>
                                <li>
                                    <a class="nav-link" href="/faqs" class="nav-link float-left ml-4" exact-active-class="is-active">FAQs</a>
                                </li>
                            </ul>
                            <div class="btn btn-success large-size">
                                <div class="top-right links">
                                    <a href="{{ route('login') }}">{{ __('Login') }} / {{ __('Sign Up') }}</a>
                                </div>
                            </div>
                        </div>
                        
                    </nav>
                </div>
            </div>
        </div>
    </div>
    
    @if (session('message'))
        <div class="hero-image-wrapper pt-lg-5 pt-md-4 pt-sm-4 pt-3">
            <div class="alert alert-{{ session('type', 'info') }} col-10 mx-auto m-0" id="successMessage">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {!! session('message') !!}
            </div>
        </div>
    @endif
    <div id="app"></div>
    @yield('content')

    @include("frontend.footer.footer")
    <!-- Scripts -->
    @yield('footer')
    <script>
        window.auth_user = {!! json_encode($auth_user); !!};
    </script>

    <script src="{{ asset('js/app.js') }}"></script>

    
    @yield('scripts')
</body>
</html>
