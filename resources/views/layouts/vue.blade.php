<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>JiveMo | @yield('title')</title>
    
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link href="{{ asset('css/style1.css') }}" rel="stylesheet">
    
    <!-- cropper -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/cropperjs/1.5.1/cropper.min.js"></script>

    @yield('header')
</head>
<body>
<body class="container-fluid p-0">
    <div id="app">
        <div class="jivemo-nav">
            <div class="nav-container">
                <div class="row">
                    <div class="col-lg-12">
                        <nav class="navbar navbar-expand-lg navbar-light">
                            <div class="logo">
                            <router-link to="/">
                                <img src="{{ config('site_setting.logo', '/images/logo.png') }}" alt="logo">
                            </router-link>
                            </div>

                            <div class="mobile-user-icon">
                            @if(Auth::user())
                            

                            <div class="right jevemo-menu-dropdown jivemo-mobie-dev">
                                    <a href="javascript:void(0)">
                                        @if(Auth::user()->profile_image)
                                            <img src="{{asset("images/uploads/".Auth::user()->profile_image)}}" class="circle p-0"/>
                                        @else
                                            <span class="circle">{{Auth::user()->first_name ? Auth::user()->first_name[0] : "U"}}</span>
                                        @endif
                                    </a>
    
                                </div>
    
                                <div class="user-profile-dropdown">
                                    <div class="user-profile-images circle">
                                        <div class="user-content">
                                            <div class="userprofile-content-right">
                                                <router-link :to="{name:'connection'}" title="{{Auth::user()->name}}" >
                                                    <h2>@if(trim(Auth::user()->name)) {{ Auth::user()->name }} @else {{ Auth::user()->username}} @endif</h2>
                                                </router-link>
                                                <user-menu></user-menu>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else

                            <div class="btn btn-success large-size">
                                
                                <div class="top-right links">
                                    @auth
                                        <router-link :to="{name:'connection'}" class="text-white">{{ __('Dashboard') }}</router-link>
                                    @else
                                        <a href="{{ route('login') }}">{{ __('Login') }} / {{ __('Register') }}</a> 
                                    @endauth
                                </div>
                            </div>
                            @endif
                            </div>
                            
                            <button class="navbar-toggler d-none" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                            </button>
                            

                            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                            <ul class="navbar-nav mr-auto mt-2 mt-lg-0">

                                <li>
                                    <router-link to="/" class="nav-link float-left ml-4" exact-active-class="is-active">Home</router-link>
                                </li>
                                <li>
                                    <router-link to="/faqs" class="nav-link float-left ml-4" exact-active-class="is-active">FAQs</router-link>
                                </li>

                                
                                <!-- <li>
                                    <router-link to="/get-paid-to-text" class="nav-link float-left ml-4" exact-active-class="is-active">Get Paid to Text</router-link>
                                </li> -->
                                {{--  <li>
                                    <router-link to="/influencer" class="nav-link float-left ml-4">Explore</router-link>
                                </li>  --}}
                                <li class="user-avator">
                                    @if(Auth::user())
                                    <div class="right jevemo-menu-dropdown">
                                        {{-- @if( Auth::user()->is_influencer   )
                                        <span class="rate">$<span class="userBlanace">{{Auth::user()->balance}}</span></span>
                                        @endif --}}
                                        <a href="javascript:void(0)">
                                            @if(Auth::user()->profile_image)
                                                <img src="{{asset('images/uploads/'.Auth::user()->profile_image)}}" class="circle p-0"/>
                                            @else
                                                <span class="circle">{{Auth::user()->first_name ? Auth::user()->first_name[0] : "U"}}</span>
                                            @endif
                                        </a>
                                    </div>

                                    <div class="user-profile-dropdown">
                                        <div class="user-profile-images circle">
                                            <div class="user-content">
                                                <div class="userprofile-content-right">
                                                    <router-link :to="{name:'connection'}" title="{{Auth::user()->username}}" >
                                                        <h2>{{Auth::user()->username}}</h2>
                                                    </router-link>
                                                    <user-menu></user-menu>
                                                </div>
                                            </div>
                                            
                                        </div>
                                    </div>
                                </li>
                            </ul>
                            
                            @else

                            <div class="btn btn-success large-size">
                                
                                <div class="top-right links">
                                    @auth
                                        <router-link :to="{name:'connection'}" class="text-white">{{ __('Dashboard') }}</router-link>
                                    @else
                                        <a href="{{ route('login') }}">{{ __('Login') }} / {{ __('Sign Up') }}</a>
                                    @endauth
                                </div>
                                
                            </div>
                            @endif
                            </div>
                            
                        </nav>
                    </div>
                </div>
            </div>
        </div>
        @if (session('message'))
            <div class="hero-image-wrapper p-1" style="min-height:0px">
                <div class="alert alert-{{ session('type', 'info') }} col-10 mx-auto mt-2 mb-2" id="successMessage">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                    {!! session('message') !!}
                </div>
            </div>
        @endif
        <div class="router-view">
            <router-view></router-view>
            <vue-progress-bar></vue-progress-bar>
        </div>
    
        @include("frontend.footer.footer")
    </div>
    @yield('footer')
    <script>
        window.auth_user = {!! json_encode($auth_user); !!};
        window.STRIPE_API_KEY = "{{ getStripeApiKey() }}"
    </script>
    <script src="{{ asset('js/app.js') }}"></script>
    <script>
        $( window ).on( "load", function() {
            
                jQuery('.right.jevemo-menu-dropdown.jivemo-mobie-dev').on('click', 'a', function(e) {
                    jQuery(this).parent().parent().find('.user-profile-dropdown').toggleClass('active');
                });
            });
    </script>
    
</body>
</html>
