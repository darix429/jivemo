import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

const Store = new Vuex.Store({
  state: {
    homePackage: [],
    connections: [],
    transactions: [],
    validCards: [],
    bconnections: [],
    packages: []
  },
  mutations: {
    /** asign bulk data from api */
    assignHomePackage(state, data) {
      state.homePackage = data;
    },
    setConnections(state, data) {
      state.connections = data;
    },
    setTransactions(state, data) {
      state.transactions = data;
    },
    setValidCards(state, data) {
      state.validCards = data;
    },
    setbConnections(state, data) {
      state.bconnections = data;
    },
    setPackages(state, data) {
      state.packages = data;
    },
    /** add single row */
    addConnection(state, data) {
      state.connections.push(data);
    },
    addPackage(state, data) {
      state.packages.push(data);
    }
  },
  getters: {
    homePackage: state => state.homePackage,
    connections: state => state.connections,
    transactions: state => state.transactions,
    validCards: state => state.validCards,
    bconnections: state => state.bconnections,
    packages: state => state.packages
  }
});
export default Store;
