/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require("./bootstrap");
/** moment */
// var moment = require("moment");
var moment = require("moment-timezone");
moment.tz.setDefault("America/Los_Angeles");
window.moment = moment;

require("../../public/js/jivemo");

window.Vue = require("vue");
/**
 * Vue Router
 */
import VueRouter from "vue-router";
Vue.use(VueRouter);

/**
 * Form
 */
import { Form, HasError, AlertError } from "vform";
window.VuForm = Form;
Vue.component(HasError.name, HasError);
Vue.component(AlertError.name, AlertError);
/**
 * loading overlay
 */
import loading from "vue-full-loading";
window.loading = loading;

/**
 * form client side validation
 **/
import VeeValidate from "vee-validate";
Vue.use(VeeValidate);

/**
 * pgogress bar
 */
import VueProgressBar from "vue-progressbar";
Vue.use(VueProgressBar, {
  color: "rgb(143, 255, 199)",
  failedColor: "red",
  height: "10px"
});

/**
 * sweet alert
 * */
import Swal from "sweetalert2";
window.Swal = Swal;
const toast = Swal.mixin({
  toast: true,
  position: "top-end",
  showConfirmButton: false,
  timer: 3000
});
window.toast = toast;

window.Fire = new Vue();
/** custom guard Gate */
import Gate from "./Gate";
Vue.prototype.$gate = new Gate(window.auth_user);

/**
 * all routes
 */
const routes = [
  {
    path: "/",
    name: "home",
    component: require("./components/Home.vue").default,
    meta: {
      title: "JiveMo - Bond With Fans Using Text Messaging"
    }
  },
  {
    path: "/privacy-policy",
    name: "privacy-policy",
    component: require("./components/pages/PrivacyPolicy.vue").default,
    meta: {
      title: "Privacy Policy Page - JiveMo"
    }
  },
  {
    path: "/faqs",
    name: "faqs",
    component: require("./components/pages/FAQs.vue").default,
    meta: {
      title: "FAQs - JiveMo"
    }
  },
  {
    path: "/terms-and-condition",
    name: "terms-and-condition",
    component: require("./components/pages/TermsAndCondition.vue").default,
    meta: {
      title: "Terms and Conditions - JiveMo"
    }
  },
  {
    path: "/get-paid-to-text",
    name: "get-paid-to-text",
    component: require("./components/pages/GetPaidToText.vue").default,
    meta: {
      title: "Get Paid to Text with Your Fans - JiveMo"
    }
  },
  // {
  //   path: "/profile1",
  //   name: "edit",
  //   component: require("./components/Profile.vue").default,
  //   meta: { requiresAuth: true }
  // },
  {
    path: "/profile",
    name: "editprofile",
    component: require("./components/EditProfile.vue").default,
    meta: { requiresAuth: true, title: "Edit Your Profile - JiveMo" }
  },
  {
    path: "/payout",
    name: "payout",
    component: require("./components/Payout.vue").default,
    meta: { requiresAuth: true, title: "View Payout Info- JiveMo" }
  },
  {
    path: "/billing",
    name: "billing",
    component: require("./components/Billing.vue").default,
    meta: { requiresAuth: true, title: "View Billing Info - JiveMo" }
  },
  {
    path: "/profile/:username",
    name: "profile",
    component: require("./components/ProfileDetail.vue").default,
    meta: { requiresAuth: false }
  },
  {
    path: "/influencer",
    name: "influencer",
    component: require("./components/Influencer.vue").default,
    meta: { requiresAuth: true }
  },
  {
    path: "/influencer-application",
    name: "application",
    component: require("./components/InfluencerApplication.vue").default,
    meta: { requiresAuth: true }
  },
  {
    path: "/connection",
    name: "connection",
    component: require("./components/connections/Connection.vue").default,
    meta: { requiresAuth: true, title: "View Connections - JiveMo" }
  },
  // {
  //   path: "/transactions",
  //   name: "transactions",
  //   component: require("./components/Transactions.vue").default,
  //   meta: { requiresAuth: true }
  // },
  // {
  //   path: "/connection/:connection/message",
  //   name: "view-message",
  //   component: require("./components/connections/Message.vue").default,
  //   meta: { requiresAuth: true }
  // },
  {
    path: "/connection",
    name: "dashboard",
    component: require("./components/connections/Connection.vue").default,
    meta: { requiresAuth: true }
  },
  {
    path: "/verify/phone",
    name: "verify_phone",
    component: require("./components/VerifyPhone.vue").default,
    meta: { requiresAuth: true }
  },
  {
    path: "/package",
    name: "package",
    component: require("./components/package/PackageList.vue").default,
    meta: { requiresAuth: true, title: "View Your Texting Packages - JiveMo" }
  },
  {
    path: "/package/:id",
    component: require("./components/package/Package.vue").default,
    meta: { requiresAuth: true },
    children: [
      {
        path: "",
        name: "join_package",
        component: require("./components/package/Confirm.vue").default,
        meta: { requiresAuth: true }
      },
      {
        path: "checkout",
        name: "checkout",
        component: require("./components/Checkout.vue").default,
        meta: { requiresAuth: true, title: "Complete Texting Package Purchase - JiveMo" }
      }
    ]
  },
  {
    path: "/email/verify",
    name: "email_verify",
    component: require("./components/EmailVerify.vue").default,
    meta: { requiresAuth: true }
  },

  {
    path: "/blocked-user",
    component: require("./components/BlockedUsers.vue").default,
    meta: { requiresAuth: true }
  },
  {
    path: "/reported-user",
    component: require("./components/ReportedUsers.vue").default,
    meta: { requiresAuth: true }
  },

  // {
  //   path: "/balance",
  //   component: require("./components/Balance.vue").default,
  //   meta: { requiresAuth: true }
  // },
  {
    path: "*",
    component: require("./components/404.vue").default
  },

  {
    path: "/login",
    meta: {
      title: "JiveMo - Login or Register"
    }
  }
  //   { path: "/register", component: require("./components/Register.vue").default },
];

const router = new VueRouter({
  mode: "history",
  routes // short for `routes: routes`
});
/**
 * run for each routes
 */
router.beforeEach((to, from, next) => {
  console.log("to login",to.path);
  document.title = to.meta.title || "JiveMo";
  next();
});

/**
 * The following block of code may be used to automatically register your
 * Vue components. It will recursively scan this directory for the Vue
 * components and automatically register them with their "basename".
 *
 * Eg. ./components/ExampleComponent.vue -> <example-component></example-component>
 */

// const files = require.context('./', true, /\.vue$/i)
// files.keys().map(key => Vue.component(key.split('/').pop().split('.')[0], files(key).default))
/**
 * pagination
 **/
Vue.component("pagination", require("laravel-vue-pagination"));
Vue.component("page-not-found", require("./components/404.vue").default);

Vue.component("faq", require("./components/extra/faq.vue").default);
Vue.component(
  "how-it-works",
  require("./components/extra/how-it-works.vue").default
);
Vue.component(
  "tab-list",
  require("./components/extra/_tab_section.vue").default
);
Vue.component(
  "user-menu",
  require("./components/extra/_user_menu.vue").default
);

Vue.component("profileSmall", {
  props: ["pkg"],
  template: `
    <div class="jivemo-profile-inner-wrapper col-11 mx-auto">
        <div class="jivemo-profile-top">
            <div class="row ryt">
                <div class="col-lg-2 col-md-3 col-sm-12 col-12">
                    <div class="image small mx-auto">
                        <img v-bind:src="'/images/uploads/'+ pkg.user.profile_image" class="" alt="man">
                    </div>
                </div>
                <div class="col-lg-10 col-md-9 col-sm-12 col-12">
                    <p class="inner-title pt-4">Confirm Texting  Package  to  Start  Texting  <strong>{{ pkg.user.first_name}} {{ pkg.user.last_name }}</strong></p>
                </div>
            </div>
        </div>
    </div>
  `
});

Vue.component(
  "passport-clients",
  require("./components/passport/Clients.vue").default
);

Vue.component(
  "passport-authorized-clients",
  require("./components/passport/AuthorizedClients.vue").default
);

Vue.component(
  "passport-personal-access-tokens",
  require("./components/passport/PersonalAccessTokens.vue").default
);

/**
 * Filters
 */
Vue.filter("capitalize", function(value) {
  if (!value) return "";
  value = value.toString();
  return value.charAt(0).toUpperCase() + value.slice(1);
});
Vue.filter("uppercase", function(value) {
  if (!value) return "";
  return value.toString().toUpperCase();
});
Vue.filter("MaskPhoneNumbers", function(value) {
  if (!value) return "";
  return "XXX-XXX-" + value.substring(value.length - 4, value.length);
});

Vue.filter("formatPhone", function($phone_string) {
  if (!$phone_string) return "";
  return (
    $phone_string.substring(0, 3) +
    "-" +
    $phone_string.substring(2, 3) +
    "-" +
    $phone_string.substring(6, 10)
  );
});

Vue.filter("humanize", function(value) {
  if (!value) return "";
  return (
    moment(value)
      // .tz("America/Los_Angeles")
      .fromNow()
  );
});
Vue.filter("dateformat", function(value) {
  if (!value) return "";
  return moment(value).format("YYYY-MM-DD");
});
Vue.filter("addmonth", function(value, month) {
  if (!value) return "";
  return moment(value)
    .add(1, "M")
    .format("YYYY-MM-DD");
});

Vue.filter("truncate", function(text, length, suffix) {
  if (text) return text.substring(0, length) + suffix;
});

/** Vuex stores */
import store from "./store/JivemoStore";
/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

const app = new Vue({
  el: "#app",
  store,
  router
});
