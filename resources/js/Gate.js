export default class Gate {
  constructor(user) {
    this.user = user;
  }
  isLoggedIn() {
    return this.user !== null;
  }
  getUserId() {
    if (this.user) return this.user.id;
    return false;
  }
  isActive() {
    if (this.user) return this.user.is_active !== 0;
    return false;
  }

  isEmailConfirmed() {
    if (this.user) return this.user.is_email_confirmed !== 0;
    return false;
  }

  isInfluencer() {
    if (this.user) return this.user.is_influencer !== 0;
    return false;
  }
  isVarifiedInfluencer() {
    if (this.user) return this.user.is_varified_influencer !== 0;
    return false;
  }
  isPhoneVerified() {
    if (this.user) return this.user.is_phone_verified !== 0;
    return false;
  }
  updatePhoneVerify(value) {
    this.user.is_phone_verified = value;
  }
}
