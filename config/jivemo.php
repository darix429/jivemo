<?php

return [

    /*
    |--------------------------------------------------------------------------
    | JiveMo Constants
    |--------------------------------------------------------------------------
    |
    | This option defines the default log channel that gets used when writing
    | messages to the logs. The name specified in this option should match
    | one of the channels defined in the "channels" configuration array.
    |
    */
    "MessageFee" => 0.035,
    "PhoneConnectonFee" => 2.50,
    'ProcessingFee' => 4.00,
    'PhoneSmsCode' =>[
        'ConfirmPriceChange'   => "88conf",
        'ConfirmNotify'        => "571yes",
        'ElmTip'               => "elmtip",
        'AdminPhone'        => "16412434312", //"4244007672", 
        'AdminNotifier'     => "16412434312",
        'AdminNumber'       => "16412434312", //B",
        'AdminNumberEric'   => "2132611897", //E",
        'SmsProcessUrlProd' => 'https://jivemo.com/api/jivemo_phones/process_call/',
        'SmsProcessUrlTest' => 'http://jivemo.com/api/jivemo_phones/process_call/',
    ],
    'PhoneSmsMessageType' =>[
        'ConnectionEstablish' => 1,
        'MessageSend' => 2,
        'NewUserConnectSeller' => 3,
        'NewUserConnectBuyer' => 4,
        'PriceChangeBuyer' => 5,
        'UserLeftElm' => 6,
        'RenewalWarning' => 7,
        'SellerMaxLimit' => 8,
        'AlreadyActive' => 9,     // SMS Onl,
        'NotificationRequestAccepted' => 10,
        'ConfirmedNewPrice' => 11,
        'AlreadyConfirmed' => 12,
        'UserActive' => 13,
        'InsufficientFunds' => 14,
        'ConfirmedNewPriceToSeller' => 15,
        'ConfirmedTipBuyer' => 16,
        'ConfirmedTipSeller' => 17,
        'PriceChangeSeller' => 18,
        'BlockedUser' => 19,
        'YouAreInactive' => 20,
        'UserRemovedPhone' => 21,
    ],

    
    'PhoneEmailMessageType' => [
        'PhoneInactive'   => 1,
        'AdminSuspended'  => 2,
        'NewUserConnectSeller'=> 3,
        'NewUserConnectBuyer' => 4,
        'PriceChangeBuyer'=> 5,
        'UserLeftElm' => 6,
        'RenewalWarning'  => 7,
        'SellerMaxLimit'  => 8,
        'AdminUnSuspended'=> 9,  //Email Onl,
        'NotificationRequestAccepted' => 10,
        'ConfirmedNewPrice'   => 11,
        'AlreadyConfirmed'=> 12,
        'UserActive'  => 13,
        'InsufficientFunds'   => 14,
        'ConfirmedNewPriceToSeller'   => 15,
        'ConfirmedTipBuyer'   => 16,
        'ConfirmedTipSeller'  => 17,
        'PriceChangeSeller'   => 18,
        'BlockedUser' => 19,
        'YouAreInactive'  => 20,
        'UserRemovedPhone'=> 21,
    ],

];