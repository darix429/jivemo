<?php
/**
 * Our Influencers Section
 */
function getJivemoOurinfluenser(){
    $posts = DB::table('users')->get();
        
    return $posts;
}

function writeUserLog ($user, $message) {
	$path			= storage_path ().'/logs/'.$user.'.log';
	$timestamp 		= \Carbon\Carbon::now();
	$dateTime		= $timestamp->toDateTimeString();
	$userAgent		= \Request::header('User-Agent');
	$clientIp		= \Request::getClientIp();
	$message		= $dateTime.' => '.$clientIp.' => '.$userAgent.' => '.$message."\n";		
	$bytesWritten 	= \File::append($path, $message);
}

function writeCustomLog ($module, $message) {
    //$customer		= str_replace('@', '_at_', $customer);
    $path			= storage_path ().'/logs/'.$module.'-'.date('Y-m-d').'.log';
    $timestamp 		= \Carbon\Carbon::now();
    $dateTime		= $timestamp->toDateTimeString();
    $userAgent		= \Request::header('User-Agent');
    $clientIp		= \Request::getClientIp();
    $message		= $dateTime.' => '.$clientIp.' => '.$userAgent.' => '.$message."\n";		
    $bytesWritten 	= \File::append($path, $message);
}

function FormatPhone($phone_string){
    $formatted_phone_string = sprintf("%s-%s-%s", substr($phone_string, 0, 3), substr($phone_string, 3, 3), substr($phone_string, 6, 10));
    return $formatted_phone_string;
}
function MaskPhoneNumbers($phone_number){
    $show_num = substr($phone_number, -4);
    return 'XXX-XXX-' . $show_num;
}

function cleanNumber($num){
    $pos = strpos(strtolower($num), '+1');
    if ($pos !== false) { //Only process if text is what we said it would be
        $num = substr($num, 2, strlen($num));
    }
    return $num;
}

/**
 * 
 */
function getPhoneMessageContentForSms($message_type_code, $username = "User", $charges = "0.10"){

    /*  class --> PhoneSmsMessageType
        const PhoneInactive = 1;
        const AdminSuspended = 2;
        const NewUserConnectSeller = 3;
        const NewUserConnectBuyer = 4;
        const PriceChangeBuyer = 5;
        const UserLeftElm = 6;
        const RenewalWarning = 7;
        const SellerMaxLimit = 8;
        const AlreadyActive = 9;     // SMS Only
        const NotificationRequestAccepted = 10;
        const ConfirmedNewPrice = 11;
        const AlreadyConfirmed = 12;
        const UserActive = 13;
        const InsufficientFunds = 14;
        const ConfirmedNewPriceToSeller = 15;
        const ConfirmedTipBuyer = 16;
        const ConfirmedTipSeller = 17;
        const PriceChangeSeller = 18;
        const BlockedUser = 19;
        const YouAreInactive = 20;
        const UserRemovedPhone = 21;
    */
    $credit_text = $charges == 1 ? 'credit' : 'credits';
    $message_type = array();
    $message_type[1]  = "** JiveMo Admin Msg ** $username's not active. Text '".config('constants.ElmPhoneSmsCode.ConfirmNotify')."' to be notified when available. Charges Apply - 0.20 $credit_text a notification";
    $message_type[2]  = "** JiveMo Admin Msg ** Message connection has been suspended by the Admin until further notice";
    $message_type[3]  = "** JiveMo Admin Msg ** $username has connected with you. Use this number to text them. Reply to begin conversation. You earn $charges $credit_text per text";
    $message_type[4]  = "** JiveMo Admin Msg ** You connected with $username. Use this number to text them. Reply to begin conversation - Cost Per Text: $charges $credit_text";
    $message_type[5]  = "** JiveMo Admin Msg ** $username changed price to $charges $credit_text, confirm online or text '".config('constants.ElmPhoneSmsCode.ConfirmPriceChange')."'. You can't text user until you confirm";
    $message_type[6]  = "** JiveMo Admin Msg ** The phone account you are trying to reach is NOT active. The user account is gone - This will be the last warning you get via SMS";
    $message_type[7]  = "** JiveMo Admin Msg ** You have 5 days until your account will be renewed";
    $message_type[8]  = "** JiveMo Admin Msg ** You can no longer send messages UNTIL the user has responded";
    $message_type[9]  = "** JiveMo Admin Msg ** $username is already active";
    $message_type[10] = "** JiveMo Admin Msg ** Request accepted, you will be notified when $username is active again and charged 0.20 $credit_text";
    $message_type[11] = "** JiveMo Admin Msg ** You have confirmed the new price of $charges $credit_text - Reply to begin conversation";
    $message_type[12] = "** JiveMo Admin Msg ** You already confirmed the new price change to $charges $credit_text - Reply to begin conversation";
    $message_type[13] = "** JiveMo Admin Msg ** $username is now active - Reply to begin conversation. Cost Per Text: $charges $credit_text";
    $message_type[14] = "** JiveMo Admin Msg ** You don't have enough Credits to text. Go here to purchase credits: https://extralunchmoney.com/purchasecredit";
    $message_type[15] = "** JiveMo Admin Msg ** $username has confirmed the new price of $charges $credit_text - Reply to begin conversation";
    $message_type[16] = "** JiveMo Admin Msg ** You tipped $username $charges $credit_text.";
    $message_type[17] = "** JiveMo Admin Msg ** $username just tipped you $charges $credit_text";
    $message_type[18] = "** JiveMo Admin Msg ** $username has NOT approved your new price, message not sent, you will be notified when user approves";
    $message_type[19] = "** JiveMo Admin Msg ** Message NOT sent. You blocked $username, please unblock $username to begin sending messages again";
    $message_type[20] = "** JiveMo Admin Msg ** Message NOT sent. You are currently inactive and disabled, please activate your phone to start sending again";
    $message_type[21] = "** JiveMo Admin Msg ** $username's phone has been deleted and can no longer be reached via JiveMo Phone / SMS";

    return $message_type[$message_type_code];
}

function getPhoneMessageContentForEmail($message_type_code, $buyer_name="User", $seller_name ="User", $cost="", $notify_buyer=1){

    /* PhoneEmailMessageType
        const PhoneInactive = 1;
        const AdminSuspended = 2;
        const NewUserConnectSeller = 3;
        const NewUserConnectBuyer = 4;
        const PriceChangeBuyer = 5;
        const UserLeftElm = 6;
        const RenewalWarning = 7;
        const SellerMaxLimit = 8;
        const AdminUnSuspended = 9;  //Email Only
        const NotificationRequestAccepted = 10;
        const ConfirmedNewPrice = 11;
        const AlreadyConfirmed = 12;
        const UserActive = 13;
        const InsufficientFunds = 14;
        const ConfirmedNewPriceToSeller = 15;
        const ConfirmedTipBuyer = 16;
        const ConfirmedTipSeller = 17;
        const PriceChangeSeller = 18;
        const BlockedUser = 19;
        const YouAreInactive = 20;
        const UserRemovedPhone = 21;
    */
    $credit_text = $cost == 1 ? 'credit' : 'credits';

    if($notify_buyer){
        $other_username = $seller_name;
    }else{
        $other_username = $buyer_name;
    }
    $elm_phone_url = "/connections";

    $message_type = $email_subject = array();

    $message_type[1]  = "$other_username's phone is currently not active. You will receive an email when the phone is active again";
    $email_subject[1] = "JiveMo SMS User Not Active: $other_username";

    $message_type[2]  = "Message connection with $other_username has been suspended by the Admin until further notice";
    $email_subject[2] = "JiveMo SMS Connection Suspended by Admin: $other_username";

    $message_type[3]  = "$other_username has connected with you using the JiveMo number listed in the subject line, you can reply ONLINE or via SMS to begin conversation";
    $email_subject[3] = "JiveMo SMS New User Connection: $other_username";

    $message_type[4]  = "You connected with $other_username via this JiveMo number, you can reply ONLINE or via SMS to begin conversation - Charges Apply: $cost";
    $email_subject[4] = "JiveMo SMS New User Connection: $other_username";

    $message_type[5]  = "$other_username has changed price to $cost, confirm online or send a text/SMS (Not Email) with '".config('constants.ElmPhoneSmsCode.ConfirmPriceChange')."' before you can resume sending SMS to $other_username";
    $email_subject[5] = "JiveMo SMS Price Update: $other_username";

    $message_type[6]  = "The phone account you are trying to reach is NOT active. The user account is no longer available on JiveMo";
    $email_subject[6] = "JiveMo SMS Phone Deleted: $other_username";

    $message_type[7]  = "You have 5 days until your SMS account and phone connection will be renewed";
    $email_subject[7] = "JiveMo SMS 5 Days Until Renewal: $other_username";

    $message_type[8]  = "You can no longer send SMS messages UNTIL the user has responded";
    $email_subject[8] = "JiveMo SMS Can Not Send Messages Until User Responds: $other_username";

    $message_type[9]  = "Message connection with $other_username has been Unsuspended by the Admin";
    $email_subject[9] = "JiveMo SMS Unsuspended By Admin: $other_username";

    $message_type[10] = "Request accepted, you will be notified when $other_username is active again and charged 0.20 Credits";
    $email_subject[10] = "JiveMo SMS Request for Notification Accepted: $other_username";

    $message_type[11] = "You confirmed the new price of $cost, you can now begin sending message via SMS to $seller_name.";
    $email_subject[11] = "JiveMo SMS Confirmation of New Price Received: $other_username";

    $message_type[12] = "You already confirmed, you can now begin sending message via SMS to $seller_name."; //May Not Use
    $email_subject[12] = "JiveMo SMS Confirmation of New Price Received: $other_username";

    $message_type[13] = "$other_username is now active you can reply via SMS to the user anytime - Current Charges Per SMS: $cost";
    $email_subject[13] = "JiveMo SMS User Active: $other_username";

    $message_type[14] = "You have insufficient funds for using JiveMo SMS, please go here to recharge before using: https://extralunchmoney.com/purchasecredit";
    $email_subject[14] = "JiveMo SMS Insufficient Funds: $other_username";

    $message_type[15] = "$other_username just confirmed the new price of $cost - text/SMS user to being conversation";
    $email_subject[15] = "JiveMo SMS Confirmation of New Price: $other_username";

    $message_type[16] = "You just tipped $other_username the amount of $cost";
    $email_subject[16] = "JiveMo SMS Tip of $cost Sent: $other_username";

    $message_type[17] = "You just received a tip from $other_username in the amount of $cost $credit_text";
    $email_subject[17] = "JiveMo SMS Tip of $cost Received: $other_username";

    $message_type[18] = "$other_username has not approved your new price, message not sent, you will be notified when $other_username approves";
    $email_subject[18] = "JiveMo SMS Message Not Sent: $other_username";

    $message_type[19] = "You tried sending a message to $other_username who you blocked. Message NOT sent. Please unblock $other_username to begin sending messages again";
    $email_subject[19] = "JiveMo SMS Message Not Sent to Blocked User: $other_username";

    $message_type[20] = "Your last message to $other_username was not sent, your phone is currently inactive, please enable to to be active again";
    $email_subject[20] = "JiveMo SMS Message Not Sent: $other_username";

    $message_type[21] = "$other_username's phone has been deleted and can no longer be reached via JiveMo Phone / SMS";
    $email_subject[21] = "JiveMo SMS User Connection Deleted: $other_username";

    $return_message = "This is an JiveMo Phone notification message: " . PHP_EOL . PHP_EOL . $message_type[$message_type_code];
    $return_message .= PHP_EOL . PHP_EOL . 'To manage your connections, click here: ' . PHP_EOL . PHP_EOL . $elm_phone_url;
    $return_message .= PHP_EOL . PHP_EOL . '-JiveMo SMS Team';

    $return_message_arr['content'] = $return_message;
    $return_message_arr['subject'] = $email_subject[$message_type_code];

    return $return_message_arr;
}

function getStripeApiKey(){
    return env('STRIPE_API_KEY', '');
}

/**
 * send sms to given number
 */
function twilio_send_sms($from, $to, $body, $media_url="", $special_cache_error = "", $no_die=false){
    
    $twilio = new App\TwilioClient\TwilioElmClient();
    $twilio->SendSms($from, $to, $body, $media_url);
    
    if(empty($no_die)){
        die($special_cache_error);
    }
    
    return true;
}