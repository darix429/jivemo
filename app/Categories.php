<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $table = "content_categories";
    protected $fillable = [ 'name', 'active'];
}
