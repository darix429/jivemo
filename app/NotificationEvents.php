<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class NotificationEvents extends Model
{
    //
    use Notifiable;
    
    protected $fillable = ['id', 'template_id', 'name', 'description'];
}
