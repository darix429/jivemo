<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InfluencerApplication extends Model
{
    //
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id', 'instagram_url', 'facebook_url','twitter_url','verification_image', 'description'
    ];

    public function User(){
        return $this->belongsTo('App\User', 'user_id');
    }
}
