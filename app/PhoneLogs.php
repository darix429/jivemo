<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PhoneLogs extends Model
{
    protected $fillable = ['from', 'to', 'message'];

    public function From(){
        return $this->belongsTo('App\User', 'from');
    }

    public function To(){
        return $this->belongsTo('App\User', 'to');
    }

}
