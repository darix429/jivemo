<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $fillable = ['user_id', 'phone_no', 'is_verified', 'code'];

    public function User(){
        return $this->belongsTo('App\User');
    }
}