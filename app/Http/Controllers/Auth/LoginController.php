<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;
use Validator;
use App\User;
use App\Phone;
use App\Countries;
use App\TwilioClient\TwilioElmClient;
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/connection';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function showLoginForm()
    {
        $countries = new Countries();
        return view('auth.login', ["countries"=>$countries->get()]);
    }

    protected function generateVerificationCode($phone) {
        $code = mt_rand(10000,99999);

        // send code to phone number        
        $twilio = new TwilioElmClient();
        $smsBody = "** JiVeMo Admin ** Login Verification Code: $code";
        $twilio->SendSms(config('jivemo.PhoneSmsCode.AdminPhone'), $phone, $smsBody);

        return $code;
    }

    protected function validatePhoneCountry($phone, $country) {
        $sid    = env('TWILLO_SID', '');
        $token  = env("TWILLO_TOKEN", "");
        $twilio = new Client($sid, $token);        
        try {
            $res = $twilio->lookups->v1->phoneNumbers($phone)->fetch(array("countryCode" => $country));
            return true;
        } catch (TwilioException $e) {
            return false;
        }
    }

    protected function loginuser(Request $request) {
        $req = request(["phone_no", "code", "redirect"]);
        
        // recaptcha
        if (empty($req["code"])) {
            $url = "https://www.google.com/recaptcha/api/siteverify";
            $data = [
                "secret"=>"6Lef3NUUAAAAAAXBXtYOtW_0h4RD_CozzLPuzBvF",
                "response"=>request('g-recaptcha-response')
            ];
            $opts = [
                "http"=>[
                    "header"=>"Content-type: application/x-www-form-urlencoded\r\n",
                    "method"=>"POST",
                    "content"=>http_build_query($data)
                ]
            ];
            $context = stream_context_create($opts);
            $res = file_get_contents($url, false, $context);
            $resJson = json_decode($res);
            if ($resJson->success != true) return back()->withErrors(["Captcha Error"]);
        }

        // recaptcha passed
        $test_phones = ["1112221234", "1234567890", "3216549870", "1113331234","1114441234"];
        $phone = $req["phone_no"];
        $validator = Validator::make($req, [
            "phone_no" => "required|regex:/^[0-9]{10}+$/"
        ])->validate();

        if (!in_array($phone, $test_phones)) {
            if (!$this->validatePhoneCountry($phone, $request->country)) {
                return back()->withErrors(['You have entered an invalid phone number.']);
            }
        }
        
        if (!empty($req["redirect"])) {
            if (explode("-",$req["redirect"])[0] == "checkout"){
                $path = "/package/". explode("-",$req["redirect"])[1] . "/checkout";
                $request->session()->put("redirect", $path);
            }
        }

        if (!empty($req["code"])) {
            // verification_code
            if ($request->session()->get("verification_code") == $req["code"]) {
                //check if new user
                $registered_phone = Phone::where('phone_no', $phone)->first();
                if ($registered_phone === null) {
                    // new user
                    $request->session()->put("phone_no", $phone);
                    return view("auth/register2");
                } else {
                    // login
                    $user_id = $registered_phone->user_id;
                    $user = User::find($user_id);
                    $this->guard()->login($user);

                    if (!empty($request->session()->get("redirect"))){
                        $redirect = $request->session()->get("redirect");
                        $request->session()->forget("redirect");
                        return $this->authenticated($request, $this->guard()->user())
                                ?: redirect($redirect);
                    } else{
                        return $this->authenticated($request, $this->guard()->user())
                                ?: redirect()->intended($this->redirectPath());
                    }
                }
            } else {
                //invalid code
                $code = $request->session()->get("verification_code");
                return back()
                        ->with("data",array("phone_no"=>$phone, "code"=>$code))
                        ->withErrors(['You have entered an invalid code']);
            }

        } else {
            $code = $this->generateVerificationCode($phone);
            $request->session()->put("verification_code", $code);
            return back()->with("data",array("phone_no"=>$phone, "code"=>$code, "country"=>$request->country));
        }
    }

    public function logout(Request $request) {
        Auth::logout();
        return redirect('/login');
    }
    
    /**
     * Get the needed authorization credentials from the request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    protected function credentials(Request $request)
    {
        $field = filter_var($request->get($this->username()), FILTER_VALIDATE_EMAIL)
            ? $this->username()
            : 'username';

        return [
            $field      => $request->get($this->username()),
            'password'  => $request->password,
            'is_active' => 1
        ];
    }

    /**
     * Send the response after the user was authenticated.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    protected function sendLoginResponse(Request $request) {
        
        $request->session()->regenerate();
        $this->clearLoginAttempts($request);

        $authUser = Auth::user();
        $redirect = $this->redirectPath();
        if( $authUser->id == 1){
            $redirect = "/admin";
            return redirect()->route('admin');
        }

        //for api login
        // return ['message' => 'Login Success!!' , 'redirect' => $redirect, 'auth_user' => $authUser];
        //for direcet login
        return $this->authenticated($request, $this->guard()->user())
                ?: redirect()->intended($this->redirectPath());
    }
}
