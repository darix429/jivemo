<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Phone;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Request as Req;
use Mail;
use App\Notifications\sendEmailVerificationNotification;
use App\TwilioClient\TwilioElmClient;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/email/verify';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        return Validator::make($data, [
            'username' => array(
                'required',
                'min:6',
                'max:15',
                'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
                'unique:users',
            ),
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password'          => array(
                'required',
                'min:6',
                //'regex:/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*(_|[^\w])).+$/',
                'confirmed'
            ),
            'is_influencer' => 'nullable'
        ]);

    }

    // phone number verification
    protected function verifyuser(Request $request) {
        $req = request(["phone_no", "code"]);
        $phone = $req["phone_no"];
        $validator = Validator::make($req, [
            "phone_no" => "required|unique:phones|regex:/^[0-9]{10}+$/"
        ])->validate();

        if (!empty($req["code"])) {
            if ($request->session()->get("verification_code") == $req["code"]) {
                $request->session()->put("phone_no", $phone);
                return view("auth/register2");
            } else {
                return back()->withErrors(['You have entered an invalid code']);
            }
        } else {
            $code = mt_rand(10000,99999);
            $request->session()->put("verification_code", $code);

            // send code to phone number        
            // $twilio = new TwilioElmClient();
            // $smsBody = "** JiVeMo Admin ** Registration Verification Code: $code";
            // $twilio->SendSms(config('jivemo.PhoneSmsCode.AdminPhone'), $phone, $smsBody);

            return back()->with("data",array("phone_no"=>$phone, "code"=>$code));
        }

    }

    protected function registeruser(Request $request) {
        $req = $request->all();
        $req["phone_no"] = $request->session()->get("phone_no");
        $req["is_email_confirmed"] = true;
        $req["email_verified_at"] = date("Y-m-d H:i:s");

        $validator = Validator::make($req, [
            "phone_no" => "required|regex:/^[0-9]{10}+$/",
            "username" => "required|string|unique:users|min:6|max:30|regex:/(^([a-zA-Z]+)(\d+)?$)/u",
            "email" => "required|unique:users|min:6|max:70",
            "firstname" => "string",
            "lastname" => "string"
        ]);

        if ($validator->fails()) {
            return view("auth/register2")->withErrors($validator);
        } else {
            event(new Registered($user = $this->createuser($req)));
            $p = [
                "user_id" => $user->id,
                "phone_no" => $req["phone_no"],
                "is_verified" => 1
            ];
            $phone = Phone::create($p);

            \Request::session()->flash("message", "Registration complete!");
            \Request::session()->flash("type", "success");

            $this->guard()->login($user);

            if (!empty($request->session()->get("redirect"))){
                $redirect = $request->session()->get("redirect");
                $request->session()->forget("redirect");
                return $this->registered($request, $user)
                                ?: redirect($redirect);
            } else{
                return $this->registered($request, $user)
                                ?: redirect($this->redirectPath());
            }

        }
    }

    protected function createuser(array $data) {
        $d = [
            'username'  => $data['username'],
            'email'     => $data['email'],
            'first_name' => $data["firstname"],
            'last_name' => $data["lastname"],
            'email_verified_at' => $data["email_verified_at"],
            'is_email_confirmed' => $data["is_email_confirmed"],
            'signup_ip'     => Req::ip(),
            'is_influencer' => Req::input('is_influencer', 0),
            'is_active' => Req::input('is_active', 1)
        ];
        $user =  User::create($d);
        return $user;
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        
        $d = [
            'username'  => $data['username'],
            'email'     => $data['email'],
            'password'  => Hash::make($data['password']),
            'signup_ip'     => Req::ip(),
            'is_influencer' => Req::input('is_influencer', 0)
        ];
        
        $user =  User::create($d);

        $user->username = $user->username;
        $user->notify(new sendEmailVerificationNotification());

        return $user;

    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        // dd($request->all());
        $this->validator($request->all())->validate();

        event(new Registered($user = $this->create($request->all())));

        $this->guard()->login($user);

       \Request::session()->flash("message", "User Created! and Email Send plese verify your email");
       \Request::session()->flash("type", "success");
       
        return $this->registered($request, $user)
                        ?: redirect($this->redirectPath());
    }

    
}
