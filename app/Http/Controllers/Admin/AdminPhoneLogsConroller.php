<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\PhoneLogs;
use Carbon\Carbon;

class AdminPhoneLogsConroller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.phonelogs');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        //phone logs
        $phonelogs = PhoneLogs::
                WhereHas('From')->
                WhereHas('To')->
                with(
                    [
                        'From' => function($q){
                            $q->select('id', 'username');
                        },
                        'To' => function($q){
                            $q->select('id', 'username');
                        },
                    ]
                )
                ->select('id','from','to','message','created_at');

        //transaction
        return  Datatables::of($phonelogs)
                ->editColumn('created_at', function($phonelogs){
                    return ($phonelogs->created_at->toFormattedDateString());
                })
                ->editColumn('from', function($phonelogs){
                    return ($phonelogs->From->username);
                })
                ->editColumn('to', function($phonelogs){
                    return ($phonelogs->To->username);
                })
                ->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
