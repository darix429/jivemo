<?php

namespace App\Http\Controllers\Admin;

use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Phone;
use App\Connection;
use App\Package;
use App\NotificationEvents;
use App\Template;
use App\Message;
use App\Payouts;
use App\Invoices;
use App\Transaction;
use Carbon\Carbon;
use Cartalyst\Stripe\Stripe;
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;
use App\Http\Controllers\API\PhoneController;


class AdminTestController extends Controller {

    function index() {
        $currentDate = Carbon::today();
        $stripe = Stripe::make("sk_test_gHU5tYsihqG0zm2Z2XylveAr");

        echo "<br>currentdate: " . $currentDate;
        echo "<br>system time: " . date("Y-m-d h:m:s", time());

        // cancelled
        $connections =  Connection::where('next_payment_date', $currentDate)
            ->where('is_active', 0)
            ->get();
        foreach($connections as $connection){
            $connection->delete();
        }
        echo "<br>cancelled connections: " . $connections->count();


        // renew
        $connections =  Connection::where('next_payment_date', "<=", $currentDate)
            ->where('is_active', 1)
            ->get();
        foreach($connections as $connection){
            $user = $connection->user;
            if(!$user->stripe_id){
                $connection->invoice_error = "No payment method set for user.";
                $connection->save();
            } else {
                // pay
                $price = $connection->package->fee;
                $data = array('currency'=>'usd','amount' => $price, 'description' => "JiveMo Package #".$connection->package->name);
                
    
                try{
                    $item = $stripe->InvoiceItems()->create($user->getStripeId(), $data);
                    $invoice = $stripe->Invoices()->create($user->getStripeId(), ['tax_percent' => $user->getTaxPercent()]);
                    $pay = $stripe->Invoices()->pay($invoice['id']);
    
                    
                    if(  !array_key_exists("error",$invoice )   ){
                        $invoice_log = Invoices::create([
                            'user_id'       => $connection->user->id,
                            'invoice_id'    => $invoice['id'],
                            'invoice_dump'  => json_encode( $invoice ),
                        ]);
                        Transaction::create([
                            'user_id'               => $user->id, 
                            'foreign_id'            => 0, 
                            'class'                 => "Connection Renewal", 
                            'transaction_type_id'   => 4, 
                            'amount'                => $price,
                            'description'           => "Connection Renewed!", 
                            'admin_notes'           => '' ,
                            'invoice_id'            => $invoice['id'],
                            'connection_id'         => $connection->id
                        ]);
    
                        $connection->message_count = 0;
                        $connection->next_payment_date = date("Y-m-d h:i:s", strtotime("+1 day", strtotime($currentDate)));
                        $connection->invoice_id = $invoice_log->id;
                        $connection->save();
    
    
                    } else {
                        $connection->invoice_error = "Invoice Error: ".$invoice["error"];
                        $connection->save();
                        $connection->delete();
                    }
                    
                }catch(\Exception $e){
                    $connection->invoice_error = $e->getMessage();
                    $connection->save();
                    $connection->delete();
                    echo $e->getMessage();
                }
            }
        }
        echo "<br>rewnewed connections: " . $connections->count();
    }
}