<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Package;
use Carbon\Carbon;
use App\User;
use Auth;

class AdminPackageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        return view('admin.packages', ['user' => Auth::user()]);
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        //packages
        $packages = Package::
                WhereHas('user')->
                with(
                    [
                        'User' => function($q){
                            $q->select('id', 'username');
                        }
                    ]
                )
                ->select('id','user_id','name','created_at','fee','is_active');
        //packages
        return  Datatables::of($packages)
                ->editColumn('name', function($packages){
                    return $packages->name; 
                })
                ->editColumn('created_at', function($packages){
                    return ($packages->created_at->toFormattedDateString());
                })
                ->editColumn('fee', function($packages){
                    return ($packages->fee);
                })
                ->editColumn('id', function($packages){
                    return ($packages->id);
                })
                ->escapeColumns(['id'])
                ->make(true); 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add_package($id)
    {
        //show the package id
        $allUserLists =  '';
        $user = User::findOrFail($id);
        $admin_users = explode(",", env('ADMIN_USERS', 1) );
        $adminUsers = false;
        if( in_array($id, $admin_users ) ){
            $adminUsers = true;
            $allUserLists = User::where('is_influencer', '=', 1)->get();
        }


        return view('admin.edit.add-package', ['admin_users'=>$adminUsers,'user'=>$user,'allUserLists'=>$allUserLists]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add_new_package()
    {
        
        //show the package id
        $user = User::findOrFail($id);
        return view('admin.edit.add-package');
    }

    

    
    


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name'              => 'required|string',
            'description'       => 'required',
            'no_of_message'     => 'required|integer',
            'fee'               => 'required',
            'max_person'        => 'required|integer'
        ]);
        $request->merge(['user_id' => $request->content_category_id]);
        
        Package::create($request->all());

        return redirect(route('admin-packages'))->with(['message' => 'Successfuly Create Package']);;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //show the package id
        $package = Package::with('User')->findOrFail($id);
        return view('admin.edit.edit-package', ['package'=>$package]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //validate section
        $this->validate($request, [
            'name'          => 'required|string',
            'description'   => 'required',
            'no_of_message' => 'required|integer',
            'fee'           => 'required|integer',
            'max_person'    => 'required',
        ]);
        
        $package = Package::findOrFail($request->id);
        
        
        //term_and_conditions
        if( $request->get('term_and_conditions', 0)){
            $request->merge(['is_active' => 1]);
        }
        $package->update($request->all());
        return redirect(route('admin-packages'))->with(['message' => 'Successfully Update']);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id)
    {
        //distroy the package
        $package = Package::find($id);
        $package->delete();
        // $package = Package::destroy($id);
        if($request->get('ajax')) return('done');

        return back()->with(['message' => 'Successfully deleted']);
    }
}
