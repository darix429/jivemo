<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use Redirect;
use App\User;
use App\Phone;
use DB;
use App\Transaction;
use App\Connection; 
use App\Report;
use Carbon\Carbon;
use App\Package;
use App\Payouts;
use App\InfluencerApplication;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Auth\Events\Registered;
use Request as Req;
use Mail;
use App\Notifications\sendEmailVerificationNotification;

class AdminUserController extends Controller
{
    /**
     * Displays front end view
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
    	return view('admin.users');
    }


     /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function userview($id)
    {
        //show the package id
        $user = User::findOrFail($id);
        return view('admin.edit.edit-user', ['user'=>$user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {
        return view('admin.edit.add-user', ['request'=>$request]);
    }

     /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    public function createPost(Request $request)
    {
        $phone = Phone::where("phone_no","=",$request["phone_no"]);
        $update_action = false;
        if ($phone->count() > 0) {
            $phone = $phone->first();
            $update_action = true;
        } else {
            $validator = Validator::make(request(["phone_no"]), [
                "phone_no" => "required|unique:phones|regex:/^[0-9]{10}+$/"
            ])->validate();
        }
        $this->validator($request->all())->validate();


        $user_data = [
            'username'  => $request['username'],
            'email'     => $request['email'],
            'email_verified_at' => date("Y-m-d H:i:s"),
            'is_email_confirmed' => true,
            'signup_ip'     => Req::ip(),
            'is_influencer' => Req::input('is_influencer', 0),
            'is_active'     => Req::input('is_active', 0)
        ];
        $user =  User::create($user_data);

        $phone_data = [
            'user_id' => $user->id,
            'phone_no' => $request['phone_no'],
            "is_verified" => 1
        ];
        if($update_action) {
            $phone->update($phone_data);
        } else {
            $phone = Phone::create($phone_data);
        }

        //
        // $user->notify(new sendEmailVerificationNotification());
        return redirect(route('admin-user-view', $user->id))->with(['message' => 'Successfully created '.$user->username]);

    }

   

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        
        return Validator::make($data, [
            'username' => array(
                'required',
                'max:15',
                'regex:/(^([a-zA-Z]+)(\d+)?$)/u',
                'unique:users',
            ),
            'email'     => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'is_influencer' => 'nullable'
        ]);
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        $user = User::leftjoin("phones","users.id","=","phones.user_id")
            ->select("users.id", "username", "email", "users.created_at", "phones.phone_no")
            ->get();

        // return DataTables::of($model)->toJson();
        return  Datatables::of($user)
                ->editColumn('username', function($user){
                    return   $user->username;
                })
                ->editColumn('created_at', function($user){
                    return ($user->created_at->toFormattedDateString());
                })
                ->make(true); 
    }


     /**
     * Displays front end view
     *
     * @return \Illuminate\View\View
     */
    public function influencer()
    {
    	return view('admin.users-influencer');
    }

    

     /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getInfluencer()
    {
        $user = User::where('is_influencer', '=', 1)
        ->select('id','username','created_at','email');

        // return DataTables::of($model)->toJson();
        return  Datatables::of($user)
                ->editColumn('username', function($user){
                    return   $user->username;
                })
                ->editColumn('created_at', function($user){
                    return ($user->created_at->toFormattedDateString());
                })
                ->make(true); 
    }



     /**
     * Displays front end view
     *
     * @return \Illuminate\View\View
     */
    public function buyer()
    {
    	return view('admin.users-buyer');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getBuyer()
    {
        $user = User::where('is_influencer', '=', 0)
        ->select('id','username','created_at','email');

        // return DataTables::of($model)->toJson();
        return  Datatables::of($user)
                ->editColumn('username', function($user){
                    return   $user->username;
                })
                ->editColumn('created_at', function($user){
                    return ($user->created_at->toFormattedDateString());
                })
                ->make(true); 
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //admin-store-user
    }

    /**
     * Store a newly created resource in storage.
     *
     */
    // public function search(Request $request)
    // {
    //     $search = $request->search;
    //     if( $search ){
    //         $user = User::where('username', 'LIKE', "%$search%")
    //         ->orWhere('first_name', 'LIKE', "%$search%")
    //         ->orWhere('last_name', 'LIKE', "%$search%")
    //         ->orWhere('email', 'LIKE', "%$search%")
    //         ->orWhere('phone_no', 'LIKE', "%$search%")
    //         ->leftJoin('phones', 'users.id','=','phones.user_id')
    //         ->select('phones.phone_no')
    //         ->paginate(env('JIVEMO_PAGINATION', 8));
    //     }

    //     //return the view
    //     return view('admin.userdetails', ['user'=>$user]);
    // }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {
        //user detials
        $userdetails = User::where('username', '=', $username)
                ->with(
                    [
                        'packages' => function($q){
                            return $q->where('is_active', '=', 1)
                                    ->select('id', 'name', 'description','no_of_message', 'fee', 'user_id', 'max_person','created_at')
                                    ->withCount('Subscribers')
                                    ->paginate(env('JIVEMO_PAGINATION', 8));
                        },
                        'phone',
                        'InfluencerApplication',
                        'connection'
                    ]
                )
                ->select('id', 'username', 'first_name', 'last_name', 'talk_about', 'about_me', 'profile_image','is_influencer','is_varified_influencer')
                ->firstOrFail();

                //packages section
                $packages = Package::where('user_id', '=',$userdetails->id )->latest()->paginate(env('JIVEMO_PAGINATION', 8));
        
                //connection list
                $connections = Connection::where('user_id', '=',$userdetails->id )
                    ->orWhere('seller_id', '=', $userdetails->id)
                    ->with('user', 'package')->latest()->paginate(env('JIVEMO_PAGINATION', 8));

                //reported user id
                $reported_users =  Report::where('user_id', '=', $userdetails->id)
                    ->select('user_id','message','reported_user_id','created_at')
                    ->with('user')
                    ->latest()->paginate(env('JIVEMO_PAGINATION', 8));

                //Transactions
                $transactions =   Transaction::where('user_id', '=', $userdetails->id)
                    ->with('TransactionType','User')
                    ->latest()->paginate(env('JIVEMO_PAGINATION', 8));

                //Transactions
                $payouts =   Payouts::where('user_id', '=', $userdetails->id)
                    ->select('id','amount','income', 'fee','invoice_no', 'status','remarks','created_at' )
                    ->latest()->paginate(env('JIVEMO_PAGINATION', 8));

        
        return view('admin.userdetails', ['user'=>$userdetails,'connections'=>$connections,'reported_users'=>$reported_users,'transactions'=>$transactions,'payouts'=>$payouts,'packages'=>$packages]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update( Request $request, $id )
    {   
        //user find
        $user = User::findOrFail( $id );


        $this->validate($request, [
            'first_name'        => 'nullable|max:140',
            'last_name'         => 'nullable|max:140',
            'tagline'           => 'nullable|max:100',
            'talk_about'        => 'nullable',
            'content_category_id'=> 'nullable',
        ]);

        $currentImage = $user->profile_image;
        $image = $request->profile_image;
        
        if( $image ){
            
            
            $name = '';
            if( $image ) {
                $name = time() . '.' . $image->getClientOriginalExtension();
                
                // open and resize an image file
                $img = \Image::make($image)->resize(200, 200);
                // save the same file as jpg with default quality
                $path = public_path('images/uploads/').$name;
                $img->save($path);
                $request->merge(['profile_image' => $name]);

            }
            $request = $request->all();
            unset($request['profile_image']);
            $request['profile_image'] = $name;
        }else{
            $request = $request->all();
            unset($request['profile_image']);
            $request['profile_image'] =  $user->profile_image;
        }
        
        
        $user->update($request);
        return back()->with('message', 'Successfully Update');
    }

    /**
     * phoneUpdate
     *
     * @param  mixed $request
     * @param  mixed $username
     *
     * @return void
     */
    public function phoneUpdate(Request $request, $username){
        $user = User::Where('username', $username)->firstOrFail();
        $phone = $user->phone; 
        
        // update phone number or resend verification code
        if( $phone and trim($phone->phone_no) !== trim($request->phone_no)){
            //check phone number already in our system ? 
            $already = Phone::where('phone_no', '=', $request->phone_no)->first();
            if( $already ){ 
                return back()->with('error', 'Phone Number already in used');
            }
            
            $phone->phone_no = trim($request->phone_no);
            $phone->is_verified = true;
            $phone->save();
        }else{
            $this->validate($request, [
                'phone_no' => 'required|unique:phones|max:10'
            ]);
            Phone::create([
                'user_id' => $user->id,
                'phone_no'  => $request->phone_no,
                'is_verified'   => 1
            ]);
        }

        return back()->with('message', 'Successfully Update');
    }
    
    /**
     * deletePhone
     *
     * @param  mixed $request
     * @param  mixed $username
     *
     * @return void
     */
    public function deletePhone(Request $request, $username){
        $user = User::Where('username', $username)->firstOrFail();
        $phone = $user->phone; 
        if($phone) $phone->delete();
        return back()->with('message', 'Deleted Successfully');
    }

    /**
     * influencer revew update
     */
    public function approveInfluencer( Request $request, $id ){
        //user 
        $user = User::findOrFail( $id );


        //
        $request->merge(['is_influencer' => $request->is_varified_influencer]);
        $user->update($request->all());
        
        //callback
        return back()->with(['message' => 'Successfully Update']);;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        
        if(request('ajax')){
            return('done');
        } else {
            return array("success"=>true, "message"=>"success");
        }

    }
}
