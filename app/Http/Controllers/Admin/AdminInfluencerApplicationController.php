<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\User;
use App\InfluencerApplication;
use Carbon\Carbon;

class AdminInfluencerApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.influencerreview');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        //influencer list
        $influencer = InfluencerApplication::
                WhereHas('User')->
                with(
                    [
                        'User' => function($q){
                            $q->select('id', 'username','email','is_varified_influencer','profile_privacy');
                        },
                    ]
                )
                ->select('id','user_id','created_at','description');
        //influencer
        return  Datatables::of($influencer)
                ->editColumn('user_id', function($influencer){
                    return ($influencer->User->username);
                })
                ->editColumn('description', function($influencer){
                    return ($influencer->User->email);
                })
                ->editColumn('verification_image', function($influencer){
                    return (($influencer->User->is_varified_influencer)?'<span class="badge badge-success">Active</span>':'<span class="badge badge-warning">Panding</span>');
                })
                ->editColumn('created_at', function($influencer){
                    return (($influencer->User->profile_privacy)?'<span class="badge badge-success">Public</span>':'<span class="badge badge-warning">Private</span>');
                })
                ->editColumn('action', function($influencer){
                    return ('<a class="mr-3 btn btn-primary" href="/admin/users/'.$influencer->User->username .'"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>');
                })
                ->escapeColumns(['id'])
                ->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //user 
        $user = User::findOrFail( $id );

        $user->update($request->all());
        
        //callback
        return back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
