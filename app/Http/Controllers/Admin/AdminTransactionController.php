<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Transaction;
use Yajra\Datatables\Datatables;
use Carbon\Carbon;
use App\User;
use App\TransactionType;

class AdminTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.transactions');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {

        $trans = Transaction::orderBy("created_at", "DESC")->get();
        $datatable = Datatables::of($trans)
                    ->editColumn('created_at', function($trans){
                        return Date("Y-m-d", strtotime($trans->created_at));
                    })
                    ->editColumn("buyer", function($trans) {
                        $buyer = $trans->user_id;
                        if ($trans->User)
                        $buyer = $trans->User->username;
                        else if ($trans->Connection) {
                            if ($trans->Connection->user)
                                $buyer = $trans->Connection->user->username;
                            else
                                $buyer = $trans->Connection->user_id;

                        }
                        return $buyer;
                    })
                    ->editColumn("package", function($trans) {
                        $package = "";
                        if ($trans->Connection) {
                            if ($trans->Connection->Package)
                                $package = $trans->Connection->Package->name;
                            else
                                $package = $trans->Connection->package_id;
                        }
                        return $package;
                    })
                    ->editColumn("influencer", function($trans) {
                        $influencer = "";
                        if ($trans->Connection) {
                            if ($trans->Connection->Seller)
                                $influencer = $trans->Connection->Seller->username;
                            else
                                $influencer = $trans->Connection->seller_id;
                        }
                        return $influencer;
                    })
                    ->make(true);
        
        return $datatable;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request, $username)
    {
        $user = User::where('username', $username)->firstOrFail();
        $users = User::all();
        $transaction_types = TransactionType::all();
        
        return view("admin.transaction.create", ['user' => $user, 'users' => $users, 'transaction_types' => $transaction_types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $username)
    {
         $this->validate($request, [
            'user_id'               => 'required|integer',
            'seller_id'             => 'required|integer',
            'transaction_type'      => 'required',
            'description'           => 'required|max:1000',
            'amount'                => 'required',
            'payout'                => 'required',
            'connection_id'                => 'required',
            'admin_note'            => 'required',
            'payout_id'             => 'integer',
            'connection_id'         => 'integer'
        ]);

        $data = $request->except('transaction_type', 'seller_id');
        $data['transaction_type_id']    = $request->transaction_type;
        $data['foreign_id']             = $request->seller_id;
        $data['class']                  = 'Connection';
        
            
        $t = Transaction::create($data);

        return redirect()->route('admin-userdetails', $username);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
