<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Settings;

class AdminSetting extends Controller
{
    function index(){
        
        $data = config('site_setting');
        return view('admin.settings', ['data' => $data]);
    }

    function update(Request $request){
        $data = config('site_setting');
        $inputs = $request->all();
        
        unset($inputs['logo']);
        unset($inputs['admin_logo']);

        $logo = $request->logo;
        if( $logo ){
            $name = time() . '.' . $logo->getClientOriginalExtension();
            // dd($name);
            // open and resize an image file
            $img = \Image::make($logo);
            // save the same file as jpg with default quality
            $db_path = '/images/uploads/'.$name;
            $path = public_path($db_path);
            $img->save($path);

            $inputs['logo'] =  $db_path;
        }
        $admin_logo = $request->admin_logo;
        if( $admin_logo ){
            $name = time() . '.' . $admin_logo->getClientOriginalExtension();
            
            // open and resize an image file
            $img = \Image::make($admin_logo);
            // save the same file as jpg with default quality
            $db_path = '/images/uploads/'.$name;
            $path = public_path($db_path);
            $img->save($path);

            $inputs['admin_logo'] =  $db_path;
        }
        
        $data->update($inputs);
                

        return back()->with('message', 'Successfully updated');

    }
}