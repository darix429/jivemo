<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;
use Auth;
use App\Template;
use App\User;
use App\NotificationEvents;
use App\Notifications\SendEmailNotification;
use App\TwilioClient\TwilioElmClient;

class AdminTemplatesController extends Controller
{
    //
    protected function index() {
        $templates = Template::all();
        $notification_events = NotificationEvents::all();

        return view('admin.templates', ['user' => Auth::user(), 'templates'=> $templates, 'notification_events'=>$notification_events]);
    }

    protected function addSmsTemplate() {
        $notification_events = NotificationEvents::all();
        return view('admin.edit.add-sms-template', ['user' => Auth::user(), 'notification_events'=>$notification_events]);
    }
    protected function addEmailTemplate() {
        $notification_events = NotificationEvents::all();
        return view('admin.edit.add-email-template', ['user' => Auth::user(), 'notification_events'=>$notification_events]);
    }

    protected function editEmailTemplate($id) {
        $template = Template::find($id);
        $notification_events = NotificationEvents::all();
        return view('admin.edit.edit-email-template', ['user' => Auth::user(), 'template'=>$template, 'notification_events'=>$notification_events]);
    }

    protected function editTemplate($id) {
        $template = Template::find($id);
        $notification_events = NotificationEvents::all();
        return view('admin.edit.edit-template', ['user' => Auth::user(), 'template'=>$template, 'notification_events'=>$notification_events]);
    }

    protected function sendTemplateView() {
        $templates = Template::all();
        $notification_events = NotificationEvents::all();
        return view('admin.edit.send-template', ['user' => Auth::user(), 'templates'=>$templates]);
    }

    protected function deleteTemplate($id) {
        $template = Template::find($id);
        $name = $template->name;
        $template->delete();
        return back()->with('success', "Template $name has been successfully deleted.");
    }

    protected function saveTemplate(Request $request) {

        $req = $request->all();
        $rules = array(
            "type"=>"required|string",
            "name"=>"required|string|max:100|min:5",
            "template"=>"required|string|min:10"
        );
        if ($req["type"]=="email") {
            $rules["subject"] = "required|string|max:150|min:5";
        }

        $validator = Validator::make($req, $rules);
        if ($validator->fails()) {
            return back()->withInput()->withErrors($validator);
        }

        $status = $req["status"] == "1"? true : null;
        $data = array(
            "type"=>$req["type"],
            "name"=>$req["name"],
            "template"=>$req["template"],
            "status"=>$status
        );
        if ($req["type"]=="email") {
            $data["subject"] = $req["subject"];
        }
        
        $action = $req["action"];
        if ($action == "save") { // create
            $template = Template::create($data);
            if ($req["event"] > 0) {
                $event = NotificationEvents::find($req["event"]);
                $event->update([
                    "template_id"=>$template->id
                ]);
            }
        } else { //update
            $id = $req["id"];
            $template = Template::find($id);
            $template->update($data);

            $oldEvent = NotificationEvents::where("template_id","=",$template->id)->first();
            if ((int)$req["event"] > 0) {
                $oldEvtId = $oldEvent? $oldEvent->id : 0;
                if ($oldEvtId != $req["event"]) {
                    if ($oldEvent) {
                        $oldEvent->update([
                            "template_id"=>null
                        ]);
                    }
                    $newEvent = NotificationEvents::find($req["event"]);
                    $newEvent->update([
                        "template_id"=>$template->id
                    ]);
                }
            } else {
                if ($oldEvent->id != $req["event"]) {
                    $oldEvent->update([
                        "template_id"=>null
                    ]);
                }
            }

        }

        return redirect()->route("templates", [$template->id])->with("success", "Template saved.");

    }

    protected function sendTemplate(Request $request) {
        try {
            $req = $request->all();
            $template = Template::find($req["template"]);
            $type = $template->type;
            $to = $req["to"];

            if ($type == 'sms') {
                // send code to phone number        
                $twilio = new TwilioElmClient();
                $smsBody = $template->template;
                $twilio->SendSms(config('jivemo.PhoneSmsCode.AdminPhone'), $to, $smsBody);
            } else {
                // $user = User::find($req["user"]);
                $user = (new User)->forceFill([
                    "email" => $to
                ]);

                $subject = $req["subject"];
                $data = array(
                    "subject" => $subject,
                    "message" => $template->template
                );

                $user->notify(new SendEmailNotification($data));
            }
            
            return redirect()->route("send-template-view", [$template->id])->with("success", $template->name . " sent to $to.");

        } catch (\Exception $e) {
            return back()->withErrors(['There was an error encountered while sending the notification email to '. $to]);
        }
    }
}
