<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\User;
use App\Payouts;
use App\Connection;
use App\Transaction;
use App\Package;
use App\Message;
use Carbon\Carbon;

class PayoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.payout', ["collection"=>$this->getData()]);
    }

    

    public function history()
    {
        $collection = Payouts::where("status", "=", "1")
                        ->whereNotNull("remarks")
                        ->get();

        foreach($collection as $data) {
            $remarks = date("Y-m-d", strtotime("-1 month", strtotime($data->remarks)));
            $data["period"] = $remarks;
        }
        return view('admin.payout-history', ['collection' => $collection]);
    }

    public function historyInfo($id) {

        $payout = Payouts::find($id);
        $from = date("Y-m-d", strtotime("-1 month", strtotime($payout->remarks)));
        $to = date("Y-m-d", strtotime($payout->remarks));
        $payout["period"] = $from;

        $user = User::find($payout->user_id);
        
        $connections = Connection::where("seller_id", $user->id)
                                ->whereBetween("created_at", [$from, $to])
                                ->get();
        $payout_details = [];
        $totalRevenue = 0;
        $totalFee = 0; $totalConnectionFee = 0; $totalMessageFee = 0; $totalProcessFee = 0;
        $totalIncome = 0;

        if ($connections->count() > 0) {
            foreach($connections as $connection) {
                $details = $connection->getPayoutDetails();
                $totalConnectionFee += $details["connection_fee"];
                $totalMessageFee += $details["message_fee"];
                $totalProcessFee += $details["process_fee"];
                $totalRevenue += $details["revenue"];
                $totalFee += $details["fee"];
                $totalIncome += $details["income"];
                array_push($payout_details, $details);
            }
        }        

        return view('admin.edit.view-payout')->with(array("payout"=>$payout, "details"=>array(
            "date_from" => $from,
            "date_to" => $to,
            "user" => $user,
            "payout_details" => $payout_details,
            "revenue" => $totalRevenue,
            "fee" => $totalFee,
            "income" => number_format($totalIncome, 2),
            "connection_fee" => $totalConnectionFee,
            "message_fee" => $totalMessageFee,
            "process_fee" => $totalProcessFee,
            "payout_date" => $payout->create_time
        )));


    }

    

    public function info($id, $date)
    {
        $to = strtotime($date);
        $from = date("Y-m-d", strtotime("-1 month", $to));
        $to = date("Y-m-d", $to);

        $user = User::find($id);

        $connections = Connection::where("seller_id", $id)
                                ->whereBetween("created_at", [$from, $to])
                                ->whereNotIn("seller_id", Payouts::select("user_id")->where("remarks", $to))
                                ->get();

        $payout_details = [];
        $totalRevenue = 0;
        $totalFee = 0; $totalConnectionFee = 0; $totalMessageFee = 0; $totalProcessFee = 0;
        $totalIncome = 0;

        if ($connections->count() > 0) {
            foreach($connections as $connection) {
                $details = $connection->getPayoutDetails();
                $totalConnectionFee += $details["connection_fee"];
                $totalMessageFee += $details["message_fee"];
                $totalProcessFee += $details["process_fee"];
                $totalRevenue += $details["revenue"];
                $totalFee += $details["fee"];
                $totalIncome += $details["income"];

                array_push($payout_details, $details);
            }
        }

        $payout_date = strtotime($to);
        $payout_date = implode("-", [date("Y", $payout_date),date("m", $payout_date),"05"]);

        $payout = array(
            "date_from" => $from,
            "date_to" => $to,
            "user" => $user,
            "payout_details" => $payout_details,
            "revenue" => $totalRevenue,
            "fee" => $totalFee,
            "income" => number_format($totalIncome, 2),
            "connection_fee" => $totalConnectionFee,
            "message_fee" => $totalMessageFee,
            "process_fee" => $totalProcessFee,
            "payout_date" => $payout_date
        );
        

        return view('admin.edit.add-payout')->with( 'payout', $payout );
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        
        $users = User::whereNull("deleted_at")
                    ->where("is_influencer", 1)
                    ->get();
        $initial = strtotime($users->min("created_at"));
        $initialMonth = date("m", $initial);
        $initialYear = date("Y", $initial);

        
        $today = strtotime(Carbon::now()->toDateTimeString());
        $todayMonth = date("m", $today);
        $todayYear = date("Y", $today);

        $date = strtotime("$initialYear-$initialMonth-05");
        $target = strtotime("$todayYear-$todayMonth-05");
        $target = strtotime("+1 month", $target);

        $collection = [];

        foreach($users as $user) {
            while($date != $target) {
                $nextDate = date("Y-m-d", strtotime("+1 month", $date));

                $connections = Connection::where("seller_id", $user->id)
                                        ->whereBetween("created_at", [date("Y-m-d", $date), $nextDate])
                                        ->whereNotIn("seller_id", Payouts::select("user_id")->where("remarks", $nextDate))
                                        ->get();
                if ($connections->count() > 0) {

                    $income = 0;
                    foreach($connections as $connection) {
                        $income += $connection->getPayoutDetails()["income"];
                    }
                    $payout_date = strtotime($nextDate);
                    $payout_date = implode("-", [date("Y", $payout_date),date("m", $payout_date),"05"]);
                    array_push($collection, array(
                        "date_from"=>date("Y-m-d", $date),
                        "date_to"=>date("Y-m-d", strtotime($nextDate)),
                        "user"=>$user,
                        "connections"=>$connections,
                        "income"=>number_format($income, 2),
                        "payout_date"=>$payout_date
                    ));
                }


                 // increment date
                $date = strtotime($nextDate);
            }
            // reset date
            $date = strtotime("$initialYear-$initialMonth-05");
        }

        return $collection;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $collection = [];

        $user = User::findOrFail($id);
        $packages = Package::where("user_id", $id)
                    ->where("is_active", "1")
                    ->get();
        foreach ($packages as $package) {
            $connections = Connection::where("seller_id", $id)
            ->where("package_id", $package->id)
            ->whereNull("deleted_at")
            ->where(function($q){
                $q->where("status","active")
                ->orWhere("status", "ending");
            })
            ->get();
            
            $phone_connection_fee = config('site_setting.connection_fee', config('jivemo.PhoneConnectonFee')) * $connections->count();
            $per_message_fee = config('site_setting.msg_fee', config('jivemo.MessageFee')) * $package->no_of_message;
            $processing_fee = (config('site_setting.processing_fee_percent', config('jivemo.ProcessingFee'))/100) * $package->fee;
            $revenue = $package->fee * $connections->count();
            $jivemo_fee = $connections->count() > 0 ? $phone_connection_fee + $per_message_fee + $processing_fee : 0;

            foreach($connections as $connection) {
                $data = array(
                    "subscriber" => $connection->User->username,
                    "package_name" => $package->name,
                    "charge_date" => $connection->next_payment_date,
                    "amount" => ($package->fee),
                    "fee" => round(($jivemo_fee / $connections->count()), 2),
                    "payout" => round((($revenue-$jivemo_fee) / $connections->count()), 2)
                );
                array_push($collection, $data);
            }
        }

        return view('admin.edit.add-payout', ['user'=>$user, 'collection'=>$collection]);
    }

    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate section
        // $user = User::findOrFail($request->user_id);
        $this->validate($request, [
            'user_id'           => 'required|integer',
            'invoice_no'        => 'required',
            'amount'            => 'required|numeric|min:0|not_in:0',
            'income'            => 'required|numeric|min:0|not_in:0',
            'fee'               => 'required|numeric|min:0|not_in:0',
            'status'            => 'required',
            'remarks'           => 'required'
        ]);

        $now = strtotime(Carbon::now()->toDateTimeString());
        $endDate = implode("-",[date("Y",$now),date("m",$now),"05"]);
        $paid = Payouts::where("user_id", $request->user_id)
                        ->where("remarks", $endDate);
        
        if($paid->count() > 0) {
            return back()->withErrors(['This user has been already paid for this period']);
        } else {
            //create
            Payouts::create($request->all());
            Transaction::where('user_id', '=', $request->user_id)
                        ->where('payout', '=', 0)
                        ->update(['payout' => 1]);
            
            return redirect()->route('admin-payouts')->with(['message' => 'Payout create successfully!']);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //show the package id
        $payout = Payouts::WhereHas('User')->with('User')->findOrFail($id);

        return view('admin.edit.edit-payout')->with( 'payout', $payout );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //payout find
        $payout = Payouts::findOrFail( $id );

        //validate section
        $this->validate($request, [
            'invoice_no'        => 'required',
            'amount'            => 'required',
            'income'            => 'required',
            'fee'            => 'required',
            'remarks'           => 'required|string',
            'status'            => 'required',
        ]);

        $payout->update($request->all());

        return back()->with(['message' => 'Successfully Update']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
