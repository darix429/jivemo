<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Yajra\Datatables\Datatables;
use App\Connection;
use Carbon\Carbon;
use App\TwilioClient\TwilioElmClient;

class AdminConnectionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admin.connections');
    }

    /**
     * Process ajax request.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function getData()
    {
        //connections list
        $connections = Connection::
                WhereHas('user')->
                WhereHas('Seller')->
                with(
                    [
                        'user' => function($q){
                            $q->select('id', 'username');
                        },
                        'Seller' => function($q){
                            $q->select('id', 'username');
                        },
                        'package' => function($q){
                            $q->select('id', 'user_id','name');
                        },
                    ]
                )
                ->select('id','user_id','seller_id','package_id','created_at');
        //dd($connections);
        //connections
        return  Datatables::of($connections)
                ->editColumn('seller_id', function($connections){
                    return ($connections->Seller->username);
                })
                ->editColumn('user_id', function($connections){
                    return ($connections->user->username);
                })
                ->editColumn('package_id', function($connections){
                    return (($connections->package)?$connections->package->name:"No Package Avilable Now");
                })
                ->editColumn('created_at', function($connections){
                    return ($connections->created_at->toFormattedDateString());
                })
                ->editColumn('id', function($connections){
                    return ($connections->id);
                })
                ->make(true); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $connection = Connection::findOrFail($id);
        $connection->status = $request->status;
        $connection->save();
        return back()->with(['message' => 'Connection updated!!']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $id=null)
    {
        $connection = Connection::find($request->id);
        $connection_phone = $connection->phone;
        $connection->delete();
        
        $twilio = new TwilioElmClient();
        if(!$twilio->releasePhone($connection_phone)){
            if( $request->get('ajax')) exit('deleted');
            return back()->with(['message' => 'Successfully deleted']);
        } else {
            if( $request->get('ajax')) exit('Connection deleted but twilio number was not released.');
            return back()->withErrors(['Connection deleted but twilio number was not released.']);
        }
        
    }
}
