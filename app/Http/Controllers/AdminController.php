<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Transaction;
use App\Connection; 
use App\PhoneLogs;
use App\InfluencerApplication;
use App\Report;
use Carbon\Carbon;
use App\Package;


class AdminController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    //admin controller
    public function admin(){
        //user list
        $user_list = User::paginate(env('JIVEMO_PAGINATION', 8));
        $packages = Package::WhereHas('user')->with('User')->paginate(env('JIVEMO_PAGINATION', 8));
        $register_user_count    = $this->getRegisterUserCount();
        $get_packages_count     = $this->getRegisterPackageCount();
        $get_connection_count   = $this->getRegisterConnectionCount();

        
        return view('admin.home', ['auth_user' => Auth::user(),'user_list'=>$user_list,'packages'=>$packages,'register_user_count'=>$register_user_count,'get_packages_count'=>$get_packages_count,'get_connection_count'=>$get_connection_count]);
    }

    //get register user
    public function getRegisterUserCount(){
        return User::all()->count();
    }

    //get package count
    public function getRegisterPackageCount(){
        return  Package::all()->count();
    }

     //get package count
     public function getRegisterConnectionCount(){
        return  Connection::all()->count();
    }

    //admin transactions
    public function transactions(){
        //user list
        $transactions =  Transaction::with('TransactionType','User')->paginate(env('JIVEMO_PAGINATION', 8));
        return view('admin.transactions', ['transactions'=>$transactions]);
    }

    //admin connection list
    public function connections(){
        //user list
        $connections =  
            Connection::with(
                    'user', 
                    'package',
                    'Seller'
                )->latest()->paginate(env('JIVEMO_PAGINATION', 8));
        return view('admin.connections', ['connections'=>$connections]);
    }


    //admin phonelogs
    public function phonelogs(){
        //phone logs
        $phonelogs =  PhoneLogs::with('From','To')->paginate(env('JIVEMO_PAGINATION', 8));
        return view('admin.phonelogs', ['phonelogs'=>$phonelogs]);
    }

   

    //admin users
    public function influencerreview(){
        //phone logs
        $influencerreview =  InfluencerApplication::with('User')->paginate(env('JIVEMO_PAGINATION', 8));
        return view('admin.influencerreview', ['influencerreview'=>$influencerreview]);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function connectionDestroy($id)
    {
        $id = Connection::find( $id );
        $id ->delete();
        return back();
    }


}