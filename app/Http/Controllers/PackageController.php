<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PackageController extends Controller
{
    /**
     * Show the package page
     *
     * @since 1.0.0
     */
    public function index( Request $request ){

        return view('frontend.package.index');
    }


    /**
     * Show the newPackage
     *
     * @since 1.0.0
     */
    public function create( Request $request ){

        return view('frontend.package.create');
    }

    /**
     * Show the newPackage
     *
     * @since 1.0.0
     */
    public function monthlySubscription( Request $request ){

        return view('frontend.package.monthlysub');
    }

    
}
