<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;

class InfluencerController extends Controller
{
     /**
     * construct function
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        /**
         * is influencer
         */
        if( !empty( $request->name ) ){
            $ourInfulernser = User::where('first_name', 'like', '%'. $request->name.'%')
                            ->orWhere('last_name', 'like', '%'. $request->name.'%')
                        ->where('is_influencer', '=', 1)
                        ->where('is_active', '=', 1)
                        ->where('profile_privacy', '=', 1)
                        ->latest()->paginate(env('JIVEMO_PAGINATION', 8));
        }else{
            $ourInfulernser = User::where('is_active', '=', 1)
                        ->where('is_influencer', '=', 1)
                        ->where('profile_privacy', '=', 1)
                        ->latest()->paginate(env('JIVEMO_PAGINATION', 8));
        }          


        return ['influencer' => $ourInfulernser];
    }




}
