<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Package;
use App\Connection; 
use App\Message; 
use App\Transaction;
use DB;
use App\User;
use App\Phone;
use App\PhoneLogs;
use App\Template;
use App\NotificationEvents;
use App\Notifications\PackageBuyNotification;
use App\Notifications\SendEmailNotification;
use Carbon\Carbon;
use App\TwilioClient\TwilioElmClient;


class Checkout extends Controller
{
    /**
     * construct method
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $vdata = [
            'package_id'        => 'required|integer',
            'additionalBalance' => 'required|integer',
            // 'cartholder'        => 'required|string',
            // 'zipcode'           => 'required|integer',
            'is_agree'          => 'required|boolean',
            'stripeToken'       => 'required|string',
        ];

        $this->validate($request, $vdata);
        
        $pay_from_old_card = false;
        if(strpos($request->stripeToken, "card_",0) === 0){
            $pay_from_old_card  = true;
        }
        
        $package = Package::find($request->package_id);
        if(!$package) return response()->json(['message' => "Invalid Package", 'redirect' => '/'], 403);
        
        $user = auth('api')->user();

        if(!$user->stripe_id){
           $customer = $user->createStripeCustomer($request->stripeToken, $plan=null, $coupon = null);
           if( isset($customer['error']) ) return response()->json(['message' => $customer['error'] ], 403);
        }elseif(!$pay_from_old_card){
            //update card info
            $changeStripeCard = $user->changeStripeCard($request->stripeToken);
            if( isset($changeStripeCard['error']) ) return response()->json(['message' => $changeStripeCard['error'] ], 403);
        }
        
        //charge amount 
        $chargeAmount       = $package->fee + $request->additionalBalance;
        // $charge          = $user->chargeCustomer($chargeAmount);
        $invoiceDescription = ucfirst($package->User->username). "'s Texting Package ". $package->name;
        $invData            = array('amount' => $chargeAmount, 'description' => $invoiceDescription);
        $invoice            = $user->createInvoiceAndPay($invData);
        
        if(!$invoice) return response()->json(['message' => 'Unable to create invoice please try again.'. $invoice['error'] ], 403);
        if( isset($invoice['error']) ) return response()->json(['message' => $invoice['error'], 'redirect' => '/'], 403);
        
        //update user balance
        $user->balance = $user->balance + $request->additionalBalance;
        $user->save();
        
        /**
         * Generate section
         */
        //transition start 

        $checkout = DB::transaction(function() use($user, $request, $invoice, $package, $chargeAmount ) {
            $connection_users_names = $user->username . ' - ' . $package->user->username;
            $spun_up_number = $this->twilio_spin_up_number("424", $connection_users_names); 
            // $spun_up_number = "4243532513";
            $seller_id      = $request->seller_id;
            $package_id     = $request->package_id;
            $now            = Carbon::now();
            // $next_day     = $now->addDay();
            $next_month     = $now->addMonth();
            $connection = Connection::create(
                [
                    'user_id'               => $user->id,
                    'phone'                 => $spun_up_number,
                    'seller_id'             => $seller_id,
                    'package_id'            => $package_id,
                    'is_active'             => 1,
                    'price'                 => $chargeAmount,
                    'status'                => 'active', 
                    'next_payment_date'     => $next_month,
                    'invoice_id'            => $invoice['id'],
                    'customer_id'           => $user->stripe_id
                ]
            );

            
            /**
             * create first message from admin
             */
            $msg = "Connection created to {$connection->Seller->username} for $ {$connection->package->fee}. Next chage will be on {$next_month}";
            
            $msg = Message::create([
                'sender_id'     => 1, #admin user id
                'receiver_id'   => $user->id,
                'connection_id' => $connection->id,
                'message'       => $msg
            ]);

            

            
            // transction for buyer
            $description = "Bought {$package->name} price {$package->fee} from {$package->user->username} and Account balance added $ {$request->additionalBalance}";
            //transaction recored 
            Transaction::create([
                'user_id'               => $user->id, 
                'foreign_id'            => $msg->id, 
                'class'                 => "Checkout", 
                'transaction_type_id'   => 3, 
                'amount'                => $chargeAmount,
                'description'           => $description, 
                'admin_notes'           => '' ,
                'invoice_id'            => $invoice['id'],
                'connection_id'         => $connection->id
            ]);

            // transction for seller
            // $description = "Sell {$package->name} price {$package->fee} from {$package->user->username} and Account balance added $ {$request->additionalBalance}";
            // //transaction recored 
            // Transaction::create([
            //     'user_id'               => $seller_id, 
            //     'foreign_id'            => $msg->id, 
            //     'class'                 => "Message", 
            //     'transaction_type_id'   => 1, 
            //     'amount'                => $chargeAmount,
            //     'description'           => $description, 
            //     'admin_notes'           => '',
            //     'invoice_id'            => $invoice['id'],
            //     'connection_id'         => $connection->id
            // ]);

            return compact('connection', $connection);

        });

        //Send BUYER TEXT And Email
        $this->send_email_or_sms_to_user($checkout['connection'], true);
        //Send SELLER TEXT AND Email
        // $this->send_email_or_sms_to_user($checkout['connection'], false);

        
        
        return ['message' => 'Connection created successfully!!'];
    }

    function twilio_spin_up_number($area_code, $friendly_name){
        
        // call data
        $data = array(
            "AreaCode"      => $area_code,
            "FriendlyName"  => $friendly_name,
            "SmsUrl"        => $this->get_process_url()
        );

        $twilio = new TwilioElmClient();
        $new_number = $twilio->SpinUpNumber($data);
        
        if($new_number){
            return $twilio->cleanNumber($new_number);
        }else{
            $this->process_admin('Error Spinning Up Number: ' . $friendly_name);
            return 0;
        }
    }
    /**
     * return url 
     * it is used for phone webhook
     * we can get webhook with this url
     */
    function get_process_url(){
        $processUrl = config('jivemo.PhoneSmsCode.SmsProcessUrlProd');

        //for test 
        $processUrl = config('jivemo.PhoneSmsCode.SmsProcessUrlTest');
        
        return $processUrl;
    }

    /**
     * notify admin about connection fail or else
     */
    function process_admin($body){
        twilio_send_sms(config('jivemo.PhoneSmsCode.AdminNotifier'), config('jivemo.PhoneSmsCode.AdminNumber'), $body, "", "", true);
    }

    /**
     * send user sms or email
     */
    function send_email_or_sms_to_user($connection, $notify_buyer=false){
        $buyer_obj = $connection->user;
        $seller_obj = $connection->Seller;
        $variables = array(
            "buyer_username" => $buyer_obj->username,
            "influencer_username" => $seller_obj->username,
            "message_count" => $connection->package->no_of_message
        );

        // notify buyer
        $eventBuyer = NotificationEvents::find(3); // 3 -> notification event id for buyer notif
        if ($eventBuyer->template_id) {
            $template = Template::find($eventBuyer->template_id);
            $message = $template->template;
            foreach($variables as $key=>$value) {
                $message = str_replace("[".$key."]", $value, $message);
            }
            $buyer_phone_number = $connection->user->phone->phone_no;
            twilio_send_sms($connection->phone, $buyer_phone_number, $message, "","",true);
            
            $connection->notified = true;
            $connection->save();
            $connection->phone_logs('buyer', $message);
        }
            
        // notify influencer
        $eventSeller = NotificationEvents::find(4); // 4 -> notification event id for buyer notif
        if ($eventSeller->template_id) {
            $template = Template::find($eventSeller->template_id);
            $message = $template->template;
            foreach($variables as $key=>$value) {
                $message = str_replace("[".$key."]", $value, $message);
            }
            $seller_phone_number = $seller_obj->phone->phone_no;
            twilio_send_sms($connection->phone, $seller_phone_number, $message, "","",true);
            $connection->notified = true;
            $connection->save();
            $connection->phone_logs('seller', $message);
        }

        // email notifications
        $this->package_buy_sell_notification($connection);

    }

    //transaction recored 
    public function package_buy_sell_notification($connection){
        $buyer = $connection->user;
        $package = $connection->package;
        $seller = $connection->Seller;

        $variables = array(
            "buyer_username" => $buyer->username,
            "influencer_username" => $seller->username,
            "package_name" => $package->name,
            "buyer_phone_no" => $connection->phone,
            "influencer_phone_no" => $connection->phone,
            "package_sms_count" => $package->no_of_message,
            "monthly_price"=> $package->fee,
            "next_charge_date"=>$connection->next_payment_date,
            "linkto_connections_page"=>"https://jivemo.com/connection"
        );

        $eventBuyer = NotificationEvents::find(5); //purchase event buyer
        $templateBuyer = Template::find($eventBuyer->template_id);
        if ($templateBuyer) {
            $subject = $templateBuyer->subject;
            $message = $templateBuyer->template;
            foreach($variables as $key=>$value) {
                $subject = str_replace("[".$key."]", $value, $subject);
                $message = str_replace("[".$key."]", $value, $message);
            }

            $data = array(
                "subject" => $subject,
                "message" => $message
            );
            $buyer->notify(new SendEmailNotification($data));
        }
 
        $eventInfluencer = NotificationEvents::find(6); //pruchase event influencer
        $templateInfluencer = Template::find($eventInfluencer->template_id);
        if ($templateBuyer) {
            $subject = $templateInfluencer->subject;
            $message = $templateInfluencer->template;
            foreach($variables as $key=>$value) {
                $subject = str_replace("[".$key."]", $value, $subject);
                $message = str_replace("[".$key."]", $value, $message);
            }

            $data = array(
                "subject" => $subject,
                "message" => $message
            );
            $seller->notify(new SendEmailNotification($data));
        }
    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
