<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Auth;
use App\User;
use App\ContentCategory;

class ProfileController extends Controller
{
    /**
     * construct method
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except('show');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::where('id', '=', auth('api')->user()->id)
            ->with(
                [
                    'phone' => function($q){
                        return $q->where('is_verified', '=', 1);
                    },
                    //'category'
                ]
            )->firstOrFail();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($username)
    {  
        
        $user = User::where('username', $username)                
                ->with(
                    [
                        'packages' => function($q){
                            return $q->where('is_active', '=', 1)
                                    ->select('id', 'name', 'description', 'fee', 'user_id', 'max_person','no_of_message')
                                    ->withCount('Subscribers');
                        },
                        'phone'
                    ]
                )
                ->select('id', 'username', 'first_name', 'last_name', 'talk_about', 'about_me', 'profile_image', 'is_varified_influencer', 'is_influencer')
                ->first();

        if( $user )
            return $user;
        
        return response()->json(['error' => 'Unknown error', 'message' => 'User not found'], 404);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        if(!$user) return response()->json(['error' => 'Unknown error', 'message' => 'User not found'], 404);
        $this->validate($request, [
            'first_name'        => 'nullable|max:140',
            'last_name'         => 'nullable|max:140',
            'tagline'           => 'nullable|max:100',
            'about_me'          => 'nullable|max:500',
            'talk_about'        => 'nullable',
            'profile_privacy'   => 'nullable',
            'content_category_id'=> 'nullable',
            'profile_image'     => 'required'
        ]);
        $currentImage = $user->profile_image;
        if( $request->profile_image != $currentImage) {
            $name = time(). '.'. explode('/', explode(':', substr($request->profile_image, 0, strpos($request->profile_image, ';')))[1])[1];

            // open and resize an image file
            $img = \Image::make($request->profile_image)->resize(200, 200);
            // save the same file as jpg with default quality
            $path = public_path('images/uploads/').$name;
            $img->save($path);

            $request->merge(['profile_image' => $name]);
        }
        $user->update($request->all());

        return ['message' => "User Updated"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
