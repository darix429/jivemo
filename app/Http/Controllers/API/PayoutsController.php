<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\Payouts;
use Carbon\Carbon;
use App\Transaction;
use App\User;

class PayoutsController extends Controller
{
    /**
     * construct function
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = User::find(auth('api')->user()->id);

        //payout details
        $payout = $user->getCurrentPayout();


        //payout section
        // $payouts = Payouts::where('user_id', '=', $user->id)
        //     ->select('user_id','invoice_no','amount', 'income', 'fee','status','remarks','created_at')
        //     ->with('User')
        //     ->latest()
        //     ->paginate(env('JIVEMO_PAGINATION', 8));
        $payouts = Payouts::where("user_id", $user->id)->get();

        //total payout

        // foreach($payouts as $data) {
        //     $data["amount"] = number_format($data["amount"], 2);
        //     $data["fee"] = number_format($data["fee"], 2);
        //     $data["income"] = number_format($data["income"], 2);
        // }
        $history = [];
        foreach($payouts as $d) {
            $remarks = strtotime("-1 month", strtotime($d->remarks));
            array_push($history, array(
                "user_id"=>$d->user_id,
                "created_at"=>date("M d, Y", strtotime($d->created_at)),
                "period"=>date("M-Y", $remarks),
                "amount"=>number_format($d->amount,2),
                "fee"=>number_format($d->fee,2),
                "income"=>number_format($d->income,2),
                "status"=>$d->status
            ));
        }

        $transaction = Transaction::where('user_id', '=', auth('api')->user()->id);
        $total_transcation      = $transaction->select('amount')->sum('amount');
        $complete_payout        = $transaction->where( 'payout', '=',1 )->select('amount')->sum('amount');
        $oncomplete_complete    = ( $total_transcation - $complete_payout );
 
        //payout section
        $collection = collect([
                'user'                  => auth('api')->user(),
                'payouts'               => $history,
                'total_transcation'     => $total_transcation,
                'complete_payout'       => $complete_payout,
                'oncomplete_complete'   => $oncomplete_complete,
                'revenue'               => ($payout['revenue']),
                'fee'                   => ($payout['fee']),
                'income'                => number_format($payout['income'], 2),
                'date_from'             => date("M-Y", strtotime($payout['date_from'])),
                'date_to'               => date("M-Y", strtotime($payout['date_to']))
                
            ]);
        

        //return payout
        return $collection;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

         $this->validate($request, [
            'payout_email'         => 'required',
            'payout_method'        =>  'required'
        ]);

        $user = auth('api')->user();
        
        if($user->id != $id){
            return response()->json(['message' => "You are not authorize to update",], 403);
        }
        $user->payout_email = $request->payout_email;
        $user->payout_method = $request->payout_method;
        $user->save();

        return response()->json(['message' => "Payout method updated successfully"]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
