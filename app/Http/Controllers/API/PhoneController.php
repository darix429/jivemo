<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

use App\Phone;
use App\Connection;
use App\Message;
use App\Transaction;
use App\PhoneLogs;
use App\Template;
use App\NotificationEvents;

use App\TwilioClient\TwilioElmClient;
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;

class PhoneController extends Controller
{   public $twilio;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $sid    = env('TWILLO_SID', '');
        $token  = env("TWILLO_TOKEN", "");
        $this->twilio = new Client($sid, $token);    
        $this->middleware('auth:api')->except('process_call');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = auth('api')->user();
      return $user->phone;
    }

    /**
     * process_call
     *
     * @return void
     */
    function process_call(){
        if ($_SERVER['REQUEST_METHOD'] != "POST"){ echo "Get Method not allowed"; exit; }

        writeCustomLog('jivemoPhoneProcessCall', print_r($_REQUEST, true));
        $body = $_REQUEST['Body'];
        $from = $_REQUEST['From'];
        $to = $_REQUEST['To'];
        $media_url = !empty($_REQUEST['MediaUrl0']) ? $_REQUEST['MediaUrl0'] : '';
        
        $sender_number = cleanNumber($from);
        $connection_number = cleanNumber($to);
        $connection = Connection::GetConnectionByPhone($connection_number);
        if( !$connection) die('phone not found');

        $seller_number = $connection->Seller->phone->phone_no;
        $buyer_number = $connection->user->phone->phone_no;

        $limit = $connection->package->no_of_message;
        $exchange_count = Message::where("connection_id", $connection->id)->count();
        $exchange_count --;
        if($exchange_count == $limit) {
            return response()->json(['message' => "Your message limit reached!!", 'user_balance' => $connection->user->balance], 403);
        } else {
            $receiver_number = null;
            $message_details = array(
                "message"=>$body,
                "connection_id"=>$connection->id
            );
            if ($sender_number == $seller_number) {
                $receiver_number = $buyer_number;
                $message_details["sender_id"] = $connection->Seller->id;
                $message_details["receiver_id"] = $connection->user->id;
            } else {
                $receiver_number = $seller_number;
                $message_details["sender_id"] = $connection->user->id;
                $message_details["receiver_id"] = $connection->Seller->id;
            }
            try {
                $twilio_content = array(
                    "from"=>$connection_number,
                    "body"=>$body
                );
                if (!empty($media_url)) {
                    $twilio_content["mediaUrl"] = $media_url;
                }
                $sendMessage = $this->twilio->messages->create($receiver_number,$twilio_content);
                $message = Message::create($message_details);
            } catch (TwilioException $e) {
                die($e->getMessage());
            }


            // update messasge count
            $connection->message_count += 1;
            $connection->save();

            // if limit reached
            $exchange_count++;
            if ($limit == $exchange_count) {                
                $sellerEvent = NotificationEvents::find(8); //purchase event seller
                $buyerEvent = NotificationEvents::find(7); //purchase event buyer
                $sellerTemplate = Template::find($sellerEvent->template_id);
                $buyerTemplate = Template::find($buyerEvent->template_id);
                $sellerMessage = $sellerTemplate->template;
                $buyerMessage = $buyerTemplate->template;

                $variables = array(
                    "max_sms_count"=>$limit,
                    "reset_date"=>$connection->next_payment_date,
                    "buyer_username"=>$connection->user->username
                );
                foreach($variables as $key=>$value) {
                    $sellerMessage = str_replace("[".$key."]", $value, $sellerMessage);
                    $buyerMessage = str_replace("[".$key."]", $value, $buyerMessage);
                }
                
                try {
                    $sendBuyerMessage = $this->twilio->messages->create($buyer_number,array("body" => $buyerMessage,"from" => $connection_number));
                    $sendSellerMessage = $this->twilio->messages->create($seller_number,array("body" => $sellerMessage,"from" => $connection_number));
                } catch (TwilioException $e) {
                    die($e->getMessage());
                }
            }

            //logs
            $this->phone_logs($message->sender_id,$message->receiver_id,$message->message);

            return response()->json(['message' => 'done']);
        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'phone_no' => 'required|unique:phones'
        ]);
        //first validate phonenumber
        $code = $this->getVerificationCode();
        $request->merge(['code' => $code]);
        $request->merge(['user_id' => auth('api')->user()->id]);
        
        $phone = Phone::create($request->all());
        //send twillo sms for verification code
        $body = "** JiVeMo Admin ** Verification Code: $code";
        twilio_send_sms(config('jivemo.PhoneSmsCode.AdminPhone'), $phone->phone_no, $body);


        return ['message' => "Verification Code Successfully Send", 'phone' => $phone];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if( $request->phone_no){
            $this->validate($request, [
                'phone_no' => 'required|min:10'
            ]);
            $this->validate($request, [
                'phone_no' => 'required|max:10|unique:phones,phone_no,'.$id
            ]);
        
            if( !$this->validatePhoneNumber($request->phone_no)){
                return response()->json(['message' => 'Phone Number Invalid'], 403);
            }
        }
        $phone = Phone::findOrFail($id);
        // validate code
        if( $code = $request->get('code', null)){
            if($code == $phone->code){
                $phone->is_verified = 1;
                $phone->save();

                $this->sendConnectionMobileSms($phone);
                // event('PhoneVerified', $phone);
                return ['message' => 'Phone Verified Successfully', 'phone' => $phone];
            }else{
                return response()->json(['message' => 'Verification Code Invalid'], 403);
            }
        }

        // update phone number or resend verification code
        if( $phone->phone_no !== $request->phone_no){
            //check phone number already in our system ? 
            $already = Phone::where('phone_no', '=', $request->phone_no)->first();
            if( $already ){ 
                return response()->json(['message' => 'Phone Number already in used'], 401);
            }
            $phone->phone_no = $request->phone_no;
            $phone->is_verified = false;
        }
        
        if( $phone->is_verified){
            return response()->json(['message' => 'Phone Number already verified'], 403);
        }
        
        $phone->code = $this->getVerificationCode();
        $phone->save();
        
        //send twillo phone sms
        $body = "** ELM Admin ** Verification Code: $phone->code";
        if( twilio_send_sms(config('jivemo.PhoneSmsCode.AdminPhone'), $phone->phone_no, $body)){
            return ['message' => "Verification Code Successfully send !!"];
        }else{
            return response()->json(['message' => 'Something error please try again!!'], 403);
        }
        
    }

    /**
     * sendConnectionMobileSms
     *
     * @param  mixed $phone
     *
     * @return void
     */
    function sendConnectionMobileSms($phone){
        $connections = $phone->user->unNotifiedConnections();
        
        foreach($connections as $connection){
            if(($connection->User->phone and $connection->User->phone->is_verified) && ($connection->Seller->phone and $connection->Seller->phone->is_verified) ){
                //SEND SMS to buyer
                $body_buyer = getPhoneMessageContentForSms(config('jivemo.PhoneSmsMessageType.NewUserConnectBuyer'), $connection->Seller->username, '');
                $buyer_phone_number = $connection->User->phone->phone_no;
                twilio_send_sms($connection->phone, $buyer_phone_number, $body_buyer, "", "", true);
                $connection->phone_logs('buyer', $body_buyer);
                
                //send sms to seller
                $body_seller = getPhoneMessageContentForSms(config('jivemo.PhoneSmsMessageType.NewUserConnectSeller'), $connection->User->username, '');
                twilio_send_sms($connection->phone, $connection->Seller->phone->phone_no, $body_seller, "", "", true);
                $connection->phone_logs('seller', $body_seller);

                $connection->notified = true;
                $connection->save();
            }
            
        }
    }
    
    /**
     * getVerificationCode
     *
     * @return void
     */
    public function getVerificationCode(){
        return rand(100000, 999999);   
    }
    
    /**
     * validatePhoneNumber
     *
     * @param  mixed $phone
     *
     * @return void
     */
    function validatePhoneNumber($phone){
        $twillo = new TwilioElmClient();
        return $twillo->validatePhoneNumber($phone);

    }
    /**
     * twilio_send_sms
     *
     * @param  mixed $from
     * @param  mixed $to
     * @param  mixed $body
     * @param  mixed $media_url
     *
     * @return void
     */
    function twilio_send_sms($from, $to, $body, $media_url=""){
        // $to = "+9779802075711";
        // $from = "+14702883711";
        // $from = $to;
        // for test perpose 
        // if($from == '8022772715'){
            // $from = "14702883711";
        // }
        //end test perpose
        
        // dd($from ,$to, $body);
        $c = new TwilioElmClient();
        return $c->SendSms($from, $to, $body, $media_url);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * transactionLog
     *
     * @param  mixed $user
     * @param  mixed $message
     *
     * @return void
     */
    // function transactionLog($user, $message){
    //     $this->phone_logs($message->sender_id,$message->receiver_id,$message->message);
    // }

    /**
     * phone_logs
     *
     * @param  mixed $from
     * @param  mixed $to
     * @param  mixed $message
     *
     * @return void
     */
    public function phone_logs($from, $to, $message){
        //phone log 
        PhoneLogs::create([
            'from'      => $from, #admin user id
            'to'        => $to,
            'message'   => $message,
        ]);
    }
}
