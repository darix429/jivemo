<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;

use App\Phone;
use App\Connection;
use App\Message;
use App\Transaction;
use App\PhoneLogs;

use App\TwilioClient\TwilioElmClient;

class PhoneController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api')->except('process_call');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
      $user = auth('api')->user();
      return $user->phone;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'phone_no' => 'required|unique:phones'
        ]);
        //first validate phonenumber
        $code = $this->getVerificationCode();
        $request->merge(['code' => $code]);
        $request->merge(['user_id' => auth('api')->user()->id]);
        
        $phone = Phone::create($request->all());
        //send twillo sms for verification code
        $body = "** JiVeMo Admin ** Verification Code: $code";
        twilio_send_sms(config('jivemo.PhoneSmsCode.AdminPhone'), $phone->phone_no, $body);


        return ['message' => "Verification Code Successfully Send", 'phone' => $phone];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if( $request->phone_no){
            $this->validate($request, [
                'phone_no' => 'required|min:10'
            ]);
            $this->validate($request, [
                'phone_no' => 'required|max:10|unique:phones,phone_no,'.$id
            ]);
        
            if( !$this->validatePhoneNumber($request->phone_no)){
                return response()->json(['message' => 'Phone Number Invalid'], 403);
            }
        }
        $phone = Phone::findOrFail($id);
        // validate code
        if( $code = $request->get('code', null)){
            if($code == $phone->code){
                $phone->is_verified = 1;
                $phone->save();

                $this->sendConnectionMobileSms($phone);
                // event('PhoneVerified', $phone);
                return ['message' => 'Phone Verified Successfully', 'phone' => $phone];
            }else{
                return response()->json(['message' => 'Verification Code Invalid'], 403);
            }
        }

        // update phone number or resend verification code
        if( $phone->phone_no !== $request->phone_no){
            //check phone number already in our system ? 
            $already = Phone::where('phone_no', '=', $request->phone_no)->first();
            if( $already ){ 
                return response()->json(['message' => 'Phone Number already in used'], 401);
            }
            $phone->phone_no = $request->phone_no;
            $phone->is_verified = false;
        }
        
        if( $phone->is_verified){
            return response()->json(['message' => 'Phone Number already verified'], 403);
        }
        
        $phone->code = $this->getVerificationCode();
        $phone->save();
        
        //send twillo phone sms
        $body = "** ELM Admin ** Verification Code: $phone->code";
        if( twilio_send_sms(config('jivemo.PhoneSmsCode.AdminPhone'), $phone->phone_no, $body)){
            return ['message' => "Verification Code Successfully send !!"];
        }else{
            return response()->json(['message' => 'Something error please try again!!'], 403);
        }
        
    }

    /**
     * sendConnectionMobileSms
     *
     * @param  mixed $phone
     *
     * @return void
     */
    function sendConnectionMobileSms($phone){
        $connections = $phone->user->unNotifiedConnections();
        
        foreach($connections as $connection){
            if(($connection->User->phone and $connection->User->phone->is_verified) && ($connection->Seller->phone and $connection->Seller->phone->is_verified) ){
                //SEND SMS to buyer
                $body_buyer = getPhoneMessageContentForSms(config('jivemo.PhoneSmsMessageType.NewUserConnectBuyer'), $connection->Seller->username, '');
                $buyer_phone_number = $connection->User->phone->phone_no;
                twilio_send_sms($connection->phone, $buyer_phone_number, $body_buyer, "", "", true);
                $connection->phone_logs('buyer', $body_buyer);
                
                //send sms to seller
                $body_seller = getPhoneMessageContentForSms(config('jivemo.PhoneSmsMessageType.NewUserConnectSeller'), $connection->User->username, '');
                twilio_send_sms($connection->phone, $connection->Seller->phone->phone_no, $body_seller, "", "", true);
                $connection->phone_logs('seller', $body_seller);

                $connection->notified = true;
                $connection->save();
            }
            
        }
    }
    
    /**
     * getVerificationCode
     *
     * @return void
     */
    public function getVerificationCode(){
        return rand(100000, 999999);   
    }
    
    /**
     * validatePhoneNumber
     *
     * @param  mixed $phone
     *
     * @return void
     */
    function validatePhoneNumber($phone){
        $twillo = new TwilioElmClient();
        return $twillo->validatePhoneNumber($phone);

    }
    /**
     * twilio_send_sms
     *
     * @param  mixed $from
     * @param  mixed $to
     * @param  mixed $body
     * @param  mixed $media_url
     *
     * @return void
     */
    function twilio_send_sms($from, $to, $body, $media_url=""){
        // $to = "+9779802075711";
        // $from = "+14702883711";
        // $from = $to;
        // for test perpose 
        // if($from == '8022772715'){
            // $from = "14702883711";
        // }
        //end test perpose
        
        // dd($from ,$to, $body);
        $c = new TwilioElmClient();
        return $c->SendSms($from, $to, $body, $media_url);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * process_call
     *
     * @return void
     */
    function process_call(){
        if ($_SERVER['REQUEST_METHOD'] != "POST"){
            echo "Get Method not allowed";
            exit;
        }
        writeCustomLog('jivemoPhoneProcessCall', print_r($_REQUEST, true));
        $body           = $_REQUEST['Body'];
        $from_num       = $_REQUEST['From'];
        $to_twilio_num  = $_REQUEST['To'];
        $media_url      = !empty($_REQUEST['MediaUrl0']) ? $_REQUEST['MediaUrl0'] : '';
        
        $from_num = cleanNumber($from_num);
        $to_twilio_num = cleanNumber($to_twilio_num);
        
        $connection = Connection::GetConnectionByPhone($to_twilio_num);
        
        if( !$connection) die('phone not found');

        $ec_id = $connection->id;
        $cost = 1.111; //$this->getCost($connection);
        $seller_id = $connection->Seller->id;
        $buyer_id  = $connection->user_id;

        if($connection->Seller->phone->phone_no == $from_num){ // From Seller TO Buyer
            //message limit validation
            if( $valid = $connection->package->isMessageLimitReached($connection->Seller->id)){
                twilio_send_sms(config('jivemo.PhoneSmsCode.AdminPhone'), $connection->Seller->phone->phone_no, "Your message limit reached!!", '');
                return response()->json(['message' => "Your message limit reached!!", 'user_balance' => $connection->user->balance], 403);
            }
            $msg = array(
                'message'       => $body,
                'sender_id'     => $connection->Seller->id,
                'receiver_id'   => $connection->user->id,
                'connection_id' => $connection->id,
            );
            $message = Message::create($msg);
            $buyer_phone_num = $connection->user->phone->phone_no;
            twilio_send_sms($to_twilio_num, $buyer_phone_num, $body, $media_url);
            $this->phone_logs($message->sender_id,$message->receiver_id,$message->message);
        }else{
            //message limit validation
            if( $valid = $connection->package->isMessageLimitReached($connection->user->id)){
                twilio_send_sms($to_twilio_num, $connection->user->phone->phone_no, "Your message limit reached!!", '');
                return response()->json(['message' => "Your message limit reached!!", 'user_balance' => $connection->user->balance], 403);
            }
            $msg = array(
                'message'       => $body,
                'sender_id'     => $connection->user->id,
                'receiver_id'   => $connection->Seller->id,
                'connection_id' => $connection->id,
            );
            $message = Message::create($msg);
            $this->transactionLog($connection->user, $message);
            $seller_phone_num = $connection->Seller->phone->phone_no;
            twilio_send_sms($to_twilio_num, $seller_phone_num, $body, $media_url);
        }

        return response()->json(['message' => 'done']);
        
    }

    /**
     * transactionLog
     *
     * @param  mixed $user
     * @param  mixed $message
     *
     * @return void
     */
    function transactionLog($user, $message){
        DB::transaction(function () use($user, $message) {
            // cutt user balance
            // $user->balance = $user->balance - config('jivemo.MessageFee');
            // $user->save();
            // trsnaction for sender
            // add transaction table
            Transaction::create([
                'user_id'               => $user->id, 
                'foreign_id'            => $message->id, 
                'class'                 => "Message",
                'transaction_type_id'   => config('jivemo.PhoneSmsMessageType.MessageSend'), 
                'amount'                => config('jivemo.MessageFee'),
                'description'           =>'' 
            ]);

            // trsnaction for sender
            // add transaction table
            Transaction::create([
                'user_id'               => $message->receiver_id, 
                'foreign_id'            => $message->id, 
                'class'                 => "Message",
                'transaction_type_id'   => config('jivemo.PhoneSmsMessageType.MessageSend'), 
                'amount'                => config('jivemo.MessageFee'),
                'description'           =>'' 
            ]);

            //phone log
            $this->phone_logs($message->sender_id,$message->receiver_id,$message->message);
        });
    }

    /**
     * phone_logs
     *
     * @param  mixed $from
     * @param  mixed $to
     * @param  mixed $message
     *
     * @return void
     */
    public function phone_logs($from, $to, $message){
        //phone log 
        PhoneLogs::create([
            'from'      => $from, #admin user id
            'to'        => $to,
            'message'   => $message,
        ]);
    }
}
