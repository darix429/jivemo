<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Connection; 
use App\Report;
use App\BlockUsers;
use App\Message;
use Carbon\Carbon;


class ConnectionController extends Controller
{
    /**
     * construct method
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $authUser = auth('api')->user();
        // dd($authUser->id);
        $connections = Connection::where(function($q) use($authUser) {
                $q->where('user_id', $authUser->id)->orWhere('seller_id', $authUser->id);
            })
            ->whereHas('user', function($q){
                $q->where('is_active', '=', 1);
            })
            ->whereHas('seller', function($q){
                $q->where('is_active', '=', 1);
            })
            ->with([
                    'user',
                    'package',
                    'messages' => function($q){
                            $q->first();
                    }])
            
            ->latest()
            ->paginate(env('JIVEMO_PAGINATION', 8));

        $filterConnection = array();
        
        foreach($connections as $connection){
            $last_message_date = null;
            if($connection->LatestMsgCreated){
                $last_message_date = $connection->LatestMsgCreated->format('Y-m-d');
            }
            $username = $connection->user_id == $authUser->id ? $connection->Seller->username : $connection->User->username;
            $is_phone_verified =  0;
            if($connection->Seller->phone && $connection->Seller->phone->is_verified && $connection->User->phone && $connection->User->phone->is_verified){
                $is_phone_verified = 1;
            }
            $msg_limit = $connection->package ? $connection->package->no_of_message : '';
            // $total_message_count = $connection->package ? $connection->package->getMessageCount($connection->id, $connection->user_id) : '';
            $total_message_count = $connection->message_count;
            
            $filterConnection[] = array(
                'id'                    =>  $connection->id,
                'username'              =>  $username,
                'package_name'          =>  $connection->package ? $connection->package->name : 'N/A',
                'phone'                 =>  $connection->phone,
                'msg_limit'             =>  $msg_limit,
                'total_message_count'   =>  $total_message_count,
                'last_message_date'     =>  $last_message_date,
                'is_active'             =>  $connection->is_active,
                'is_phone_verified'     =>  $is_phone_verified,
                'messages'              =>  $connection->messages,
                'status'                =>  $connection->status,
                'next_payment_date'     =>  $connection->next_payment_date
            );
        }

        return collect(['data' => $filterConnection]);


        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id){
        $authUser = auth('api')->user();

         //connection user
         $connection = Connection::where('id', '=', $id)
             ->with(['Seller' => function($q){
                $q->select('id', 'first_name', 'last_name', 'profile_image', 'username', 'about_me');
            }])
            
            ->select('id', 'user_id', 'seller_id', 'is_active','package_id')
            ->firstOrFail();
        
        $is_seller  = $connection->Seller->id == $authUser->id ? true: false;
        //connection user data
        if( $is_seller ){
            $user_data = $connection->user()->select('id', 'first_name', 'last_name', 'profile_image', 'about_me', 'username')->first();
        }else{
            $user_data = $connection->Seller()->select('id', 'first_name', 'last_name', 'profile_image', 'about_me', 'username')->first();
        }
        
        $connection->setRelation('message_with', $user_data);

    
        $reported = Report::where('user_id', '=', auth('api')->user()->id)->where('reported_user_id','=', $connection->Seller->id)->first();
        $blocked = BlockUsers::where('user_id', '=', auth('api')->user()->id)->where('block_user_id','=', $connection->Seller->id)->first();
        
        
        $is_reported  = $reported ? true: false;
        $is_blocked  = $blocked ? true: false;


        $collection = collect(
            [
                'reported'      => ['is_already_reported' => $is_reported],
                'blocked'       => ['is_already_blocked' => $is_blocked],
                'is_connection' => ['is_connection' => $is_seller],
            ]

        );

        $connection->setRelation('messages', $connection->messages()->with(
                ['Sender'=> function($q){
                    $q->select('id', 'first_name', 'last_name', 'profile_image', 'about_me', 'username');
                }]
            )->orderBy('id', 'desc')->paginate(env('JIVEMO_PAGINATION', 8)));

        $connection->setRelation('extra_data', $collection);
        
    
        return $connection;

    }

    /**
     * Cancel the specified resource from connection.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function postCancel(Request $request){
        $id = $request->id;
        $authUser = auth('api')->user();

        //connection user
        $connection = Connection::where('id', '=', $id)->where('user_id', $authUser->id)->first();
        if($connection){
            if( $connection->is_active ){
                $msg                    = "Your connection successfully canceled";
                $connection->is_active  = false;
                $status                 = false;
            } else{
                $msg                    = "Your connection successfully activated";
                $connection->is_active  = true;
                $status                 = true;
            }
            $connection->status = "ending";
            $connection->save();

            return response()->json(['message' => $msg, 'connection_id' => $id, 'conn_status' => $status ]);
        }

        return response()->json(['message' => "Connection not found" ], 403);
         
    }


}
