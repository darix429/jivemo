<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\InfluencerApplication;

class ApplicationController extends Controller
{
    /**
     * construct function
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  auth('api')->user()->InfluencerApplication;

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = auth('api')->user();
        if( $user->InfluencerApplication)
            return response()->json(['message' => 'Application Already Submited'], 403);
        
        $this->validate($request, [
            'facebook_url'          => 'required|url',
            'twitter_url'           => 'required|url',
            'instagram_url'         => 'required|url',
            'description'           => 'required|max:1000',
            'verification_image'    => 'required',
        ]);
        $request->merge(['user_id' => auth('api')->user()->id]);
        
        $data = $request->except(['verification_image']);
        //image upload
        if( $request->verification_image) {
            $name = time(). '.'. explode('/', explode(':', substr($request->verification_image , 0, strpos($request->verification_image , ';')))[1])[1];

            // open and resize an image file
            $img = \Image::make($request->verification_image )->resize(300, 200);
            // save the same file as jpg with default quality
            $path = public_path('images/uploads/').$name;
            $img->save($path);

            $data['verification_image']= $name;
        }
        
        return InfluencerApplication::create($data);

        return ['message' => 'create successfully'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //social links
        return InfluencerApplication::where('user_id', '=', $id)->firstOrFail();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'facebook_url'          => 'required|url',
            'twitter_url'           => 'required|url',
            'instagram_url'         => 'required|url',
            'description'           => 'required|max:1000',
            'verification_image'    => 'required',
        ]);
        
        $application = InfluencerApplication::findOrFail($id);
        $currentImage = $application->verification_image;
        if( $request->verification_image != $currentImage) {
            $name = time(). '.'. explode('/', explode(':', substr($request->verification_image , 0, strpos($request->verification_image , ';')))[1])[1];

            // open and resize an image file
            $img = \Image::make($request->verification_image )->resize(300, 200);
            // save the same file as jpg with default quality
            $path = public_path('images/uploads/').$name;
            $img->save($path);

            $request->merge(['verification_image' => $name]);
        }
        
        $application->update($request->all());

        return ['message' => "Application update successfully"];
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
