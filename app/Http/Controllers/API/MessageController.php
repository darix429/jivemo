<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Message;
use App\Connection;
use App\Transaction;
use App\PhoneLogs;
use DB;
use App\TwilioClient\TwilioElmClient;

class MessageController extends Controller
{
    /**
     * construct function
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request){
        
        $message = Message::where(function($q){
            // return $q->where('sender_id', '=', auth('api')->user()->id)
            //         ->orWhere('receiver_id', '=', auth('api')->user()->id);
        })->with('Sender')
        ->where('connection_id', '=', $request->get('conn', null))
        ->orderBy('id', 'desc');
        if( $last = $request->get('last')){
            $message->where('id', '>', $last);
        }

        return $message->paginate(env('JIVEMO_PAGINATION', 8));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request) {
        $this->validate($request, [
            'message' => 'required|max:320',
            'sender_id' => 'required',
            'receiver_id'   => 'required',
            'connection_id'   => 'required',
        ]);
        $connection = Connection::find($request->connection_id);
        if(!$connection){
            return response()->json(['message' => "Connection not found!"], 403);
        }

        $user = auth('api')->user();
        // validate message limit
        if( $valid = $connection->package->isMessageLimitReached($user->id)){
            return response()->json(['message' => "Your message limit reached!!", 'user_balance' => $user->balance], 403);
        }

        //validate message limit
        if( $user->id !== $connection->package->user->id){ // for buyer
            //check user fund
            // if( $user->balance < config('site_setting.msg_fee', config('jivemo.MessageFee')))
            //     return response()->json(['message' => "Insufficient fund!!", 'user_balance' => $user->balance], 403);    

            DB::transaction(function () use($request, $user, $connection) {
                $message = Message::create($request->all());
                // cutt user balance
                // $user->balance = $user->balance -config('site_setting.msg_fee', config('jivemo.MessageFee'));
                // $user->save();

                // trsnaction for sender
                // add transaction table
                Transaction::create([
                    'user_id'               => $user->id, 
                    'foreign_id'            => $message->id, 
                    'class'                 => "Message",
                    'transaction_type_id'   => config('jivemo.PhoneSmsMessageType.MessageSend'), 
                    'amount'                => config('site_setting.msg_fee', config('jivemo.MessageFee')),
                    'description'           =>'' 
                ]);

                // trsnaction for sender
                // add transaction table
                Transaction::create([
                    'user_id'               => $request->receiver_id, 
                    'foreign_id'            => $message->id, 
                    'class'                 => "Message",
                    'transaction_type_id'   => config('jivemo.PhoneSmsMessageType.MessageSend'), 
                    'amount'                => config('site_setting.msg_fee', config('jivemo.MessageFee')),
                    'description'           =>'' 
                ]);
                
                $this->twilio_send_sms($connection->phone, $connection->Seller->phone->phone_no, $message->message, '');
                //phone log
                $this->phone_logs($request->sender_id,$request->receiver_id,$request->message);
            });
        }else{
            // for seller
            $message = Message::create($request->all());
            $this->twilio_send_sms($connection->phone, $connection->user->phone->phone_no, $message->message, '');
            //phone log
            $this->phone_logs($request->sender_id,$request->receiver_id,$request->message);
        }
        
        return ['message' => 'Message Send Successfully', 'user_balance' => $user->balance];
    }


    //phone log section
    public function phone_logs($from, $to, $message){

        //phone log 
        PhoneLogs::create([
            'from'      => $from, #admin user id
            'to'        => $to,
            'message'   => $message,
        ]);

    }

    function twilio_send_sms($from, $to, $body, $media_url=""){
        // $to = "+9779802075711";
        // $from = "+14702883711";
        // $from = $to;
        // for test perpose 
        // if($from == '8022772715'){
            // $from = "14702883711";
        // }
        //end test perpose
        // dd($from ,$to, $body);
        $c = new TwilioElmClient();
        return $c->SendSms($from, $to, $body, $media_url);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
