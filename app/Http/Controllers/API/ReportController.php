<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
use App\Report;


class ReportController extends Controller
{
    /**
     * construct function
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Report::where('user_id', '=', auth('api')->user()->id)
            ->select('user_id','message','reported_user_id','created_at')
            ->with('user')
            ->latest()->paginate(env('JIVEMO_PAGINATION', 8));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /** get auth user */
        $user = auth('api')->user();

        /** user id */
        $already = Report::where('user_id', '=', $user->id)->first();
    
        /** already update */
        if( $already )
            return response()->json(['message' => 'Already Reported'], 403);
        
        //validate
        $this->validate($request, [
            'reported_user_id'              => 'required|integer',
            'message'                       => 'required|max:320',
        ]);

        //merge userid
        $request->merge(['user_id' => $user->id]);
        
        //created  block
        Report::create($request->all());

        return response()->json(['message' => 'Success']);
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
