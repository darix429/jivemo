<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Connection as Connections; 
use App\Report;
use App\BlockUsers;
use Carbon\Carbon;
use App\User;
class BillingController extends Controller
{
    /**
     * construct function
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = auth('api')->user();

        
        // $connections = Connections::where('user_id', '=', $authUser->id)
        //     ->orWhere('seller_id', '=', $authUser->id)
        
        $connections = Connections::where('user_id', '=', $authUser->id)
            ->whereHas('user', function($q){
                $q->where('is_active', '=', 1);
            })
            ->whereHas('seller', function($q){
                $q->where('is_active', '=', 1);
            })
            ->with([
                    'user',
                    'package' => function($q){
                        $q->where('is_active', 1);
                    }
                ])
            ->latest()
            ->paginate(env('JIVEMO_PAGINATION', 8));
        
        $user = User::find($authUser->id);
        return collect( ['validCards' =>$user->getStirpeCard(), 'connections' => $connections] );
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function updateCard(Request $request)
    {
        if(!$request->stripeToken){
            return response()->json(['message' => "Please reload page and try again!!" ], 403);    
        }

        $user = auth('api')->user();
        if($user->getStripeId()){
            $changeStripeCard = $user->changeStripeCard($request->stripeToken);
            if( isset($changeStripeCard['error']) ) return response()->json(['message' => $changeStripeCard['error'] ], 403);
        }else{
            $customer = $user->createStripeCustomer($request->stripeToken);
            if( isset($customer['error']) ) return response()->json(['message' => $customer['error'] ], 403);
        }
        
        return response()->json(['message' => "Your card successfully changed" ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
