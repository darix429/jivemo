<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Carbon\Carbon;
use DB;
use App\Package;
use App\Connection; 
use App\Message; 
use App\Transaction; 
use App\User;
use App\Notifications\PackageBuyNotification;


class PackageController extends Controller
{
    /**
     * constructor method
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:api');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $authUser = auth('api')->user();
        return Package::where('user_id', '=', $authUser->id)
                ->select('id', 'name', 'max_person', 'fee', 'description', 'no_of_message')
                ->withCount('Subscribers')
                ->with('Subscribers')
                ->latest()->paginate(env('JIVEMO_PAGINATION', 8));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required|string',
            'description' => 'required',
            'no_of_message' => 'required|integer'
        ]);
        $request->merge(['user_id' => auth('api')->user()->id]);
        
        // return Package::create($request->all());

        return ['message' => 'create successfully'];
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $authUser = auth('api')->user();
        //validate package subscrition with current user
        $connection = Connection::where('user_id', '=' , $authUser->id)->where('package_id', '=', $id)->first();
        if($connection){
            return response()->json(['error' => 'You already subscribed this package'], 208);
        }
        
        $package = Package::where('id', '=', $id)
            ->with([
                'user' => function($q){
                    return $q->select('id', 'username');
                }
            ])
            ->select('id', 'name',  'fee', 'user_id','max_person', 'no_of_message')
            ->withCount('Subscribers')
            ->firstOrFail();
                
        $package->setRelation('validCards',collect($authUser->getStirpeCard()) );
        
        return $package;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vdata = [
            'name'          => 'required|string',
            'description'   => 'required',
            'no_of_message' => 'required|integer',
            'fee'           => 'required|integer',
            'max_person'    => 'required'
        ];
        if( $request->get('steps') == 3){
            $vdata['term_and_conditions'] = 'required';
        }
        
        $package = Package::findOrFail($id);

        $this->validate($request, $vdata);
        
        if( $request->get('term_and_conditions', 0)){
            $request->merge(['is_active' => 1]);
        }
        // $package->update($request->all());

        return ['message' => 'updated'];

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $package = Package::findOrFail($id);
        // $package->delete();

        return ['message' => 'Successfully'];
    }
    
}
