<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $user = Auth::user();
        $data = array();

        if( $user ){
            $data = $user->only(['id', 'first_name', 'last_name', 'username', 'tagline', 'email', 'is_email_confirmed', 'is_active', 'profile_privacy', 'profile_image', 'is_influencer', 'is_varified_influencer','stripe_active', 'balance']);
        
            $data['is_phone_verified'] = 0; 
            if( $user->phone && $user->phone->is_verified){
                $data['is_phone_verified'] = 1;
            }
        }
        
        return view('frontend.home', ['auth_user' => $data]);
    }

    
}
