<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Connection;
use Auth;

class ConnectionController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the confirm page
     *
     * @since 1.0.0
     */
    public function confirm(Request $request){
        //dd($request);

        // $validatedData = $request->validate([
        //     'user_id'           => 'required|max:255',
        //     'seller'    => 'required|max:100',
        //     'jivemo_package'    => 'required'
        // ]);


        // //created the user
        // DB::table('connection')->insert(
        //     [   'user_id'           => $request->input('user_id'),
        //         'seller'    => $request->input('seller'),
        //         'pickage_id'        => $request->input('jivemo_package'),
        //     ]
        // );



        return view('frontend.confirm');
    }

}
