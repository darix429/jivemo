<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BlockUsers extends Model
{
    protected $fillable = ['user_id', 'block_user_id', 'message'];

    public function User(){
        return $this->belongsTo('App\User', 'block_user_id');
    }
}
