<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Package extends Model
{
    protected $fillable = ['user_id', 'name', 'description', 'no_of_message', 'fee', 'max_person', 'is_active'];


    public function User(){
        return $this->belongsTo('App\User', 'user_id');
    }

    public function Subscribers(){
        return $this->hasMany('App\Connection')->with(['user' => function($q){
            $q->select('id', 'username', 'first_name', 'last_name', 'balance');
        }])
        ->select('id', 'seller_id', 'user_id', 'is_active', 'phone', 'package_id', 'created_at', 'is_active');
    }

    public function isMessageLimitReached($user_id){
        $messageCount = $this->user->MessageCount;
        // $messageCount = $this->getMessageCount($user_id);

        if($this->no_of_message <= $messageCount) {
            return true;
        }
        
        return false;

    }


    //get message count
    public function getMessageCount($connection_id, $user_id){
        $package_id = $this->id;
        $msg_count = Message::where(function($q) use($user_id) {
                $q->where('sender_id', '=', $user_id)->orWhere('receiver_id', $user_id);
            })
            ->where('connection_id', $connection_id)
            ->whereHas('Connection', function($query) use($package_id) {
                $query->whereHas("package", function($q) use($package_id) {
                    $q->where('package_id', '=', $package_id);
                });
            })
            ->whereMonth('created_at', date('m'))
            ->whereYear('created_at', date('Y'))
            ->count();

        if( $msg_count > 0) return $msg_count - 1;
        
        return $msg_count;
    }
    
}
