<?php

namespace App\TwilioClient;


use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;

class TwilioElmClient {
    public $twilio;
    
    function __construct(){
        // Find your Account Sid and Auth Token at twilio.com/console
        
        $sid    = env('TWILLO_SID', '');
        $token  = env("TWILLO_TOKEN", "");

        $this->twilio = new Client($sid, $token);       

    }
    function checkPhoneNumber($phone){
        $findme   = '+1';
        $pos = strpos($phone, $findme);
        if( $pos == 0) return $phone;

        return "+1{$phone}";
    }
    function cleanNumber($num){
        $pos = strpos(strtolower($num), '+1');
        if ($pos !== false) { //Only process if text is what we said it would be
            $num = substr($num, 2, strlen($num));
        }
        return $num;
    }
    public function validatePhoneNumber($phone_number){
        $phone_number = $this->checkPhoneNumber($phone_number);
        try{
            $validate = $this->twilio->lookups->v1->phoneNumbers($phone_number)->fetch(array("countryCode" => "US"));
            return true;
        }catch(\Exception $e){
            return false;
        }
    }

    public function TwilioDeleteNumber($phone_number){

        
        // call data
        $data = array("phoneNumber" => $phone_number);
        // send text message

        //http://www.twilio.com/docs/api/rest/incoming-phone-numbers
        
        try{
            $response = $this->twilio->incomingPhoneNumbers->read($data);
        } catch (TwilioException $e) {
            $message = "TwilioElmClient: TwilioDeleteNumber : {$e->getMessage()} ";
            writeCustomLog("TwilioDeleteNumberRead", $message);
        }
        if( $response and count($response) > 0){
            $response = $response[0];
        }
        if(is_object($response))
            $response = $response->toArray();

        $incomingPhoneNumberSid = $response['sid'];
        if(!$incomingPhoneNumberSid) return false;
        
        try{
            $response = $this->twilio->incomingPhoneNumbers($incomingPhoneNumberSid)->delete();
        } catch (TwilioException $e) {
            $message = "TwilioElmClient: TwilioDeleteNumber : {$e->getMessage()} ";
            writeCustomLog("TwilioDeleteNumberDelete", $message);

            return false;
        }

        return true;
    }

    function SendSms($from, $to, $body, $media_url = null){
        $data = array(
            "from" => $from,  // Verified Outgoing Caller ID or Twilio number
            "body" => $body
        );

        if($media_url){
            $data["mediaUrl"] = $media_url;
        }

        
        writeCustomLog("twilio_send_sms_log", $to. print_r($data, true));
        try{
            $message = $this->twilio->messages->create($to, $data );
        } 
        catch (TwilioException $e) {
            $message = "ElmPhoneController: twilio_send_sms : {$e->getMessage()} ";
            writeCustomLog("twilio_send_sms", $message);
            return false;
        }
        return true;
    }


    function SpinUpNumber($data){
        // call data
        
        try{
            //http://www.twilio.com/docs/api/rest/incoming-phone-number
            $message = $this->twilio->incomingPhoneNumbers->create($data );
            $message = $message->toArray();
       } catch (TwilioException $e) {
            $message = "ElmPhoneController: twilio_send_sms : {$e->getMessage()} ";
            writeCustomLog("twilio_send_sms", $message);
            //notify admin about error message            

            return false;
       }

       
       return $message['phoneNumber'];
    }

    public function getSidByPhone($phone) {
        $checkPhone = "+1".$phone;
        $lookup = $this->twilio->incomingPhoneNumbers
        ->read(array("phoneNumber" => $checkPhone),20);

        if (count($lookup) > 0) {
            $record = $lookup[0];
            return $record->sid;
        } else {
            return null;
        }
    }

    function releasePhone($phone) {
        try {
            $sid = $this->getSidByPhone($phone);
            $this->twilio->incomingPhoneNumbers($sid)->delete();
            return true;
        } catch (TwilioException $e) {
            $message = "ElmPhoneController: delete_twilio_phone : {$e->getMessage()} ";
            writeCustomLog("delete_twilio_phone", $message);
            return false;
       }
    }
}