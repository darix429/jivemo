<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['message', 'sender_id', 'receiver_id', 'connection_id'];

    public function Sender(){
        return $this->belongsTo("App\User", 'sender_id');
    }

    public function Receiver(){
        return $this->belongsTo("App\User", 'receiver_id');
    }

    public function Connection(){
        return $this->belongsTo('App\Connection', 'connection_id');
    }

    static function isFull($connection, $sender_id){
        $package = $connection->package();

        $msgCount = Message::where('sender_id', '=', $sender_id)
        ->where('conneciton_id', '=', $connection->id)
        ->whereMonth('created_at', '=', date('m'))
        ->count();

        if( $package->no_of_message == $msgCount) return true;

        return false;
    }


}
