<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Invoices extends Model
{
    protected $table = "Invoices";
    protected $fillable = ['id', 'user_id', 'invoice_id','invoice_dump','status'];

}
