<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Connection; 
use Carbon\Carbon;
use App\Invoices;

class CreateInvoice extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'JiveMo:CreateInvoice';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create user invoice for next payment';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->invoiceUserList();
    }

    /**
     * User List Invoice create
     */
    public function invoiceUserList(){
        //$dt = Carbon::create(2012, 1, 31, 0);
        $currentDate = Carbon::today();

       // echo $nextMonthInvoiceDate;
        $connections =  Connection::where('next_payment_date','<=' , $currentDate)
            ->where('is_active', 1)
            ->whereHas('user')
            ->with(['user','package'])
            ->get();

        // echo count($connections). ' Connections found!'.PHP_EOL;

        foreach($connections as $connection){
            // echo 'Creating invoice for '. $connection->user->username. PHP_EOL;
            //create invoice of users
            $price = $connection->package->fee;
            $invoice = $connection->user->createInvoiceAndPay(array('amount' => $price, 'description' => "JiveMo Package #".$connection->package->name));
            // dd($invoice);
            if( $invoice ) {
                // echo 'Invoice created for '. $connection->user->username. PHP_EOL;
                
                //is error
                if(  !array_key_exists("error",$invoice )   ){
                    echo 'Invoice paid for '. $connection->user->username. PHP_EOL;
                    //create log on invoice
                    Invoices::create([
                        'user_id'       => $connection->user->id,
                        'invoice_id'    => $invoice['id'],
                        'invoice_dump'  => json_encode( $invoice ),
                    ]);
                    // $nextMonthInvoiceDate = Carbon::createFromTimestamp($invoice['period_end'])->toDateTimeString();
                    $nextMonthInvoiceDate = date("Y-m-d h:i:s", strtotime("+1 day", strtotime($currentDate)));
                    //next payment date update
                    $connection->update([
                        'next_payment_date' => $nextMonthInvoiceDate,
                        'invoice_status'    => 'paid',
                        'invoice_error'     =>  0,
                        'is_active'         => 1
                    ]);

                    //transaction recored 
                    Transaction::create([
                        'user_id'               => $connectoin->user->id, 
                        'foreign_id'            => 0, 
                        'class'                 => "Connection Renewal", 
                        'transaction_type_id'   => 4, 
                        'amount'                => $price,
                        'description'           => "Connection Renewed!", 
                        'admin_notes'           => '' ,
                        'invoice_id'            => $invoice['id'],
                        'connection_id'         => $connection->id
                    ]);

                }else{
                    echo 'Invoice unpaid for '. $connection->user->username. PHP_EOL;
                    //next payment date update
                    if( !$connection->invoice_error ){
                        $invoice_error = 1;
                    }else{
                        $invoice_error = $connection->invoice_error + 1;
                    }

                    //invoice status
                    $connection->update([
                        'invoice_status'    => 'unpaid',
                        'invoice_error'     => $invoice_error,
                        'is_active'         => 0
                    ]);

                }               

            }
        }

    }

}
