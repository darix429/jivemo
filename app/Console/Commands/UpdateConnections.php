<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Carbon\Carbon;
use Cartalyst\Stripe\Stripe;
use App\User;
use App\Connection; 
use App\Invoices;
use App\Transaction; 
use App\Package; 
use App\NotificationEvents;
use App\TwilioClient\TwilioElmClient;
use Twilio\Rest\Client;
use Twilio\Exceptions\TwilioException;
use App\Notifications\SendEmailNotification;

class UpdateConnections extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'JiveMo:UpdateConnections';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'update ending connections and connection renewal';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $sid    = env('TWILLO_SID', '');
        $token  = env("TWILLO_TOKEN", "");
        $this->twilio = new Client($sid, $token);    
        $this->middleware('auth:api')->except('process_call');

        $this->updateConnections();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function updateConnections(){
        $currentDate = Carbon::today();
        $stripe = Stripe::make(env('STRIPE_SECRET'));

        // cancelled
        $connections =  Connection::where('next_payment_date', $currentDate)
            ->where('is_active', 0)
            ->get();
        foreach($connections as $connection){
            $connection->delete();
            $this->sendCancelNotification($connection);
        }


        // renew
        $connections =  Connection::where('next_payment_date', "<=", $currentDate)
            ->where('is_active', 1)
            ->get();
        foreach($connections as $connection){
            $user = $connection->user;
            if(!$user->stripe_id){
                $connection->invoice_error = "No payment method set for user.";
                $connection->save();
            } else {
                // pay
                $price = $connection->package->fee;
                $user = $connection->user;
                $data = array('currency'=>'usd','amount' => $price, 'description' => "JiveMo Package #".$connection->package->name);
                
    
                try{
                    $item = $stripe->InvoiceItems()->create($user->getStripeId(), $data);
                    $invoice = $stripe->Invoices()->create($user->getStripeId(), ['tax_percent' => $user->getTaxPercent()]);
                    $pay = $stripe->Invoices()->pay($invoice['id']);
    
                    
                    if(  !array_key_exists("error",$invoice )   ){
                        $invoice_log = Invoices::create([
                            'user_id'       => $connection->user->id,
                            'invoice_id'    => $invoice['id'],
                            'invoice_dump'  => json_encode( $invoice ),
                        ]);
                        Transaction::create([
                            'user_id'               => $user->id, 
                            'foreign_id'            => 0, 
                            'class'                 => "Connection Renewal", 
                            'transaction_type_id'   => 4, 
                            'amount'                => $price,
                            'description'           => "Connection Renewed!", 
                            'admin_notes'           => '' ,
                            'invoice_id'            => $invoice['id'],
                            'connection_id'         => $connection->id
                        ]);
    
                        $connection->message_count = 0;
                        $connection->next_payment_date = date("Y-m-d h:i:s", strtotime("+1 month", strtotime($currentDate)));
                        $connection->invoice_id = $invoice_log->id;
                        $connection->save();

                        $this->sendEmailNotifications($connection);
                        $this->sendSmsNotifications($connection);
    
    
                    } else {
                        $connection->invoice_error = "Invoice Error: ".$invoice["error"];
                        $connection->save();
                        $connection->delete();
                    }
                    
                }catch(\Exception $e){
                    $connection->invoice_error = $e->getMessage();
                    $connection->save();
                    $connection->delete();
                    echo $e->getMessage();
                }
            }
        }
    }

    public function sendCancelNotification($connection) {
        $event = NotificationEvents::find(9); // cancel event
        $template = Template::find($event->template_id);
        if ($template) {
            $message = $template->template;        
            $variables = array(
                "buyer_username"=>$connection->user->username
            );
            foreach($variables as $key=>$value) {
                $message = str_replace("[".$key."]", $value, $message);
            }
            
            try {
                $send = $this->twilio->messages->create($connection->Seller->phone->phone_no,array("body" => $message,"from" => $connection->phone));
            } catch (TwilioException $e) {
                die($e->getMessage());
            }
        }
    }

    public function sendSmsNotifications($connection) {
        $seller = $connection->Seller;
        $buyer = $connection->user;

        $variables = array(
            "influencer_username"=>$seller->username,
            "buyer_username"=>$buyer->username,
            "message_count"=>$connection->package->no_of_message,
        );
        
        // buyer
        $buyerEvent = NotificationEvents::find(10);
        if (!empty($buyerEvent->template_id)) {
            $template = Template::find($buyerEvent->template_id);
            $message = $template->template;        
            foreach($variables as $key=>$value) {
                $message = str_replace("[".$key."]", $value, $message);
            }
            
            try {
                $send = $this->twilio->messages->create($buyer->phone->phone_no,array("body" => $message,"from" => $connection->phone));
            } catch (TwilioException $e) {
                die($e->getMessage());
            }
        }

        // influencer
        $influencerEvent = NotificationEvents::find(12);
        if (!empty($influencerEvent->template_id)) {
            $template = Template::find($influencerEvent->template_id);
            $message = $template->template;        
            foreach($variables as $key=>$value) {
                $message = str_replace("[".$key."]", $value, $message);
            }
            try {
                $send = $this->twilio->messages->create($seller->phone->phone_no,array("body" => $message,"from" => $connection->phone));
            } catch (TwilioException $e) {
                die($e->getMessage());
            }
        }
    }

    public function sendEmailNotifications($connection) {
        $seller = $connection->Seller;
        $buyer = $connection->user;

        $variables = array(
            "influencer_username"=>$seller->username,
            "buyer_username"=>$buyer->username,
            "message_count"=>$connection->package->no_of_message,
            "package_name"=>$connection->package->name,
            "connection_number"=>$connection->phone,
            "cost"=>$connection->package->fee,
            "next_charge_date"=>$connection->next_payment_date
        );
        
        // buyer
        $buyerEvent = NotificationEvents::find(11);
        if (!empty($buyerEvent->template_id)) {
            $template = Template::find($buyerEvent->template_id);
            $subject = $template->subject;
            $message = $template->template;        
            foreach($variables as $key=>$value) {
                $message = str_replace("[".$key."]", $value, $message);
                $subject = str_replace("[".$key."]", $value, $subject);
            }

            $data = array(
                "subject" => $subject,
                "message" => $message
            );
            $buyer->notify(new SendEmailNotification($data));
        }

        // influencer
        $influencerEvent = NotificationEvents::find(13);
        if (!empty($influencerEvent->template_id)) {
            $template = Template::find($buyerEvent->template_id);
            $subject = $template->subject;
            $message = $template->template;        
            foreach($variables as $key=>$value) {
                $message = str_replace("[".$key."]", $value, $message);
                $subject = str_replace("[".$key."]", $value, $subject);
            }
            $data = array(
                "subject" => $subject,
                "message" => $message
            );
            $seller->notify(new SendEmailNotification($data));
        }
    }
}
