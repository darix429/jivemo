<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Connection; 
use Carbon\Carbon;
use App\Notifications\ExpireConnectionNotification;

class SendMailExpireConnection extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'JiveMo:SendMailExpire';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Expire connection email notification';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->SendMailExpire();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function SendMailExpire(){

        $currentDate = Carbon::today();
        $expireDate = $currentDate->addDay(7); 

       // echo $nextMonthInvoiceDate;
        $connections =  Connection::whereBetween('next_payment_date', array( Carbon::today() , $expireDate))
            ->where('is_active', 1)
            ->whereHas('user')
            ->with(['user','package'])
            ->latest()
            ->get();

        echo count($connections). " connection expireing soon". PHP_EOL;

        foreach($connections as $connection){
            //create invoice of users
            $user           = $connection->user;
            $user->subject  = 'Expire Connection #'.$connection->package->name;
            $user->message  = 'Your JiveMo Package is expire 7 days after '.$connection->package->name;
            $user->notify(new ExpireConnectionNotification());

            echo $connection->user->email. " mail send!". PHP_EOL;
        }
    }
}
