<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\User;
use App\Connection; 
use Carbon\Carbon;
use App\TwilioClient\TwilioElmClient;

class CancelSubscriptionNotification extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'JiveMo:CancelSubscriptionNotification';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Send cancelled subscription notification to influencer';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->sendNotification();
    }


    /**
     * Execute the console command.
     *
     * @return mixed
     */


     public function sendNotification() {
        $currentDate = Carbon::today();
        $expireDate = $currentDate->addDay(2); 

        $connections =  Connection::whereBetween('next_payment_date', array( Carbon::today() , $expireDate))
                     ->where("is_active", 0)
                     ->get();
             
        $event = NotificationEvents::find(9); // cancel event
        $template = Template::find($event->template_id);
        if ($template) {
            foreach($connections as $connection) {
                $message = $template->template;        
                $variables = array(
                    "buyer_username"=>$connection->user->username
                );
                foreach($variables as $key=>$value) {
                    $message = str_replace("[".$key."]", $value, $message);
                }
                twilio_send_sms($connection->phone, $connection->Seller->phone->phone_no, $message, "", "", true);
                echo "Cancelled subscription notification sent to " . $connection->Seller->username . " (". $connection->Seller->phone->phone_no .")" . PHP_EOL;
            }
        }
     }
}
