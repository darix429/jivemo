<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        Commands\CreateInvoice::class,
        Commands\SendMailExpireConnection::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     *  // * * * * * cd /path-to-your-project && php artisan schedule:run >> /dev/null 2>&1
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('JiveMo:SendMailExpire')->daily()->emailOutputTo('cyberkishor@gmail.com');
        // $schedule->command('JiveMo:CreateInvoice')->daily()->emailOutputTo('cyberkishor@gmail.com');
        $schedule->command('JiveMo:UpdateConnections')->daily();
        // $schedule->command('JiveMo:SendMailExpire')->everyMinute();
        // $schedule->command('JiveMo:CreateInvoice')->everyMinute();
        // $schedule->command('JiveMo:CancelSubscriptionNotification')->everyMinute();
        
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
