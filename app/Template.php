<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Template extends Model
{
    //
    use Notifiable;
    
    protected $fillable = ['id', 'type', 'name', 'subject', 'template', 'status'];
}
