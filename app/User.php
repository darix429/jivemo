<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Cartalyst\Stripe\Stripe;
use App\Transaction;
use App\Message;
use App\Package;
use Carbon\Carbon;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;
    protected $dates = ['deleted_at'];
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'phone_no', 'password', 'is_influencer', 'first_name','last_name','username','email', 'is_email_confirmed', 'email_verified_at', 'about_me','talk_about','profile_image','is_active','profile_privacy','content_category_id','payment_id','payment_method','is_varified_influencer'
    ];


    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getNameAttribute(){
        if( $this->first_name)
            return $this->first_name . " ". $this->last_name;
        return $this->username;
    }

    public function getMessageCountAttribute(){
        $user_id = $this->id;
        return $message_count = Message::where(function($q) use($user_id){
            $q->where('sender_id', $user_id)->orWhere('receiver_id', $user_id);
        })
        ->whereMonth('created_at', date('m'))
        ->whereYear('created_at', date('Y'))
        ->count();

        if($message_count > 0) return $message_count - 1;
        return $message_count;
    }

    public function ContentType(){
        return $this->belongsTo('App\ContentCategory', 'content_category_id');
    }

    /**
     * Get all of the posts for the user.
     */
    public function packages()
    {
        return $this->hasMany('App\Package');
    }

     /**
     * Get all phone number
     */
    public function phone()
    {
        return $this->hasOne('App\Phone','user_id');
    }
    public function InfluencerApplication()
    {
        return $this->hasOne('App\InfluencerApplication','user_id');
    }

    public function sentMessages($package_id = null){
        if( $package_id ){
            return $this->hasMany('App\Message', 'sender_id')->whereHas('Connection', function($q) use( $package_id ) {
                $q->whereHas('package', function( $qq ) use($package_id ) {
                    $qq->where('package_id', '=', $package_id);
                });
            });
        }

        return $this->hasMany('App\Message', 'sender_id');

    }
     /**
     * Get all user 
     */
    public function usersocial()
    {
        return $this->hasOne('App\UserSocial','user_id');
    }


     /**
     * Get user connections 
     */
    public function connection()
    {
        return $this->hasMany('App\Connection','user_id');
    }

    public function unNotifiedConnections(){
        return Connection::where('notified', 0)->where(function($q){
            $q->where('user_id', $this->id)->orWhere('seller_id', $this->id);
        })->get();
    }


    /**
     * Get all user 
     */
    public function getPayoutInfo($type = null){
        $user_id = $this->id;
        $connection   = Connection::where('seller_id', $user_id)->where(function($q){
            $q->where('status', 'active')->orWhere('status', 'ending');
        })
        ->whereNull("deleted_at");
        
        $amount = $connection->sum('price');
        
        if( $type == 'amount') return round($amount, 2);

        $connection_count   = $connection->count();
                            
        $connection_fee     = config('site_setting.connection_fee', config('jivemo.PhoneConnectonFee')) * $connection_count ;
        $message_count      = $this->messageCount;
        if( $connection_count > 1){
            /**
             * note: $message_count already -1 and we have multiple connection so we have to minus admin message from each connection * 
             * so $message_count = $message_count + 1(which is already -1 from attribute) - $connection_count;
             **/
            $message_count = $message_count + 1 - $connection_count;
        }
        
        $message_fee        = config('site_setting.msg_fee', config('jivemo.MessageFee')) * $message_count;
        $processing_fee     = ($amount * config('site_setting.processing_fee_percent', config('jivemo.ProcessingFee')))/100; // processing fee 4%
        $total_fee          = $connection_fee + $message_fee + $processing_fee;
        
        $income             = $amount - $total_fee;
        
        //payment logo
        if( $type == 'fee' ){
            return round($total_fee, 2);
        }
        elseif( $type == 'income' ){
            return round($income, 2);
        }
        elseif($type == 'remarks'){
            return "Total Connection: ". $connection_count. "\n Connection amount: ". $amount. "\n Connection fee: ".$connection_fee. " \n Message count: ". $message_count. "\n Message fee: " . $message_fee. " \n Processing fee: ". $processing_fee. "\n Total fee: ". $total_fee. "\n Income :". $income;
        }
        // return "0";
        if ($amount == 0)
        return null;
        else
        return compact($amount, $total_fee, $income);
    }

    public function getCurrentPayout() {
        $today = strtotime(Carbon::now()->toDateTimeString());
        $dateFrom = strtotime(implode("-",[date("Y",$today),date("m",$today),"05"]));
        $dateTo = strtotime("+1 month", $dateFrom);
        if(date("d",$today) <= 5 ) {
            $dateTo = $dateFrom;
            $dateFrom = strtotime("-1 month", $dateTo);
        }
        $dateFrom = date("Y-m-d", $dateFrom);
        $dateTo = date("Y-m-d", $dateTo);

        $connections = Connection::where("seller_id", $this->id)
                        ->whereBetween("created_at", [$dateFrom, $dateTo])
                        ->whereNotIn("seller_id", Payouts::select("user_id")->where("remarks", $dateTo))
                        ->get();
        $revenue = $fee = $income = 0;
        if($connections->count() > 0) {
            foreach($connections as $connection) {
                $details = $connection->getPayoutDetails();
                $revenue += $details["revenue"];
                $fee += $details["fee"];
                $income += $details["income"];
            }
        }

        return array(
            "date_from"=>$dateFrom,
            "date_to"=>$dateTo,
            "revenue"=>$revenue,
            "fee"=>$fee,
            "income"=>$income,
            "connection_count"=>$connections->count()
        );
        
        

    }

    public function getCurrentInvoice($date){
        return "inv_".date('Ym', strtotime($date))."_".$this->id;
    }

    /**
     * ========================= STRIPE ==================
     */

    /**
     * Get the Stripe ID for the entity.
     *
     * @return string
     */
    public function getStripeId() {
        return $this->stripe_id;
    }

    /**
     * Set whether the entity has a current Stripe subscription.
     *
     * @param  bool  $active
     */
    public function setStripeIsActive($active = true)
    {
        $this->stripe_active = $active;

        return $this;
    }

    /**
     * Set the Stripe ID for the entity.
     *
     * @param  string  $stripe_id
     */
    public function setStripeId($stripe_id)
    {
        $this->stripe_id = $stripe_id;

        return $this;
    }
    /**
     * Set the last four digits of the entity's credit card.
     *
     */
    public function setLastFourCardDigits($digits) {
        $this->last_four = $digits;

        return $this;
    }
    /**
     * Get the e-mail address where password reminders are sent.
     *
     * @return string
     */
    public function getReminderEmail()
    {
        return $this->email;
    }

    public function getTaxPercent() {
        if (strtoupper($this->addressCountryCode) == "US" && strtoupper($this->addressState) == "CA") {
            return env("TAX_PERCENT");
        }
        return 0;
    }

    /**
     * create stripe customer
     * @return mixed
     */

    public function createStripeCustomer( $token, $plan = null, $coupon = null ) {
        $stripe = Stripe::make(env('STRIPE_SECRET'));

        $data = array(
            'email'     => $this->getReminderEmail(),
            'source'    => $token
        );

        if( $plan ) $data['plan'] = $plan;
        if( $coupon ) $data['coupon']  = strtolower($coupon);

        if ($this->getTaxPercent()) {
			$data['tax_percent']		= $this->getTaxPercent();
		}

		try {
	        $customer = $stripe->customers()->create($data);
		} catch (\Exception $e) {
			$message = $e->getMessage();
			return ['error' => $message];
		}

        if(!$customer){ return false; }

        $this->setStripeIsActive(true);
        $this->setStripeId($customer['id']);

        $this->setLastFourCardDigits($customer['sources']['data'][0]['last4']);

        $this->save();

        return $customer;
    }
	
	
	/**
     * create stripe customer subscription
     * @return mixed
     */

    public function createStripeCustomerSubscription( $token, $plan = null, $coupon = null ) {
        $stripe = Stripe::make(env('STRIPE_SECRET'));

        $data = array(
            'source'    => $token
        );
		
		
		if( $plan ) $data['plan'] = $plan;
        if( $coupon ) $data['coupon']  = strtolower($coupon);
		
		if ($this->getTaxPercent()) {
			$data['tax_percent']		= $this->getTaxPercent();
		}
		
		

		try {
	        $subscription = $stripe->subscriptions()->create($this->stripe_id, $data);
		} catch (\Exception $e) {
			$message = $e->getMessage();
			echo $message;
			exit;
		}

        if(!$subscription){ return false; }

        $this->setStripeIsActive(true);
        $this->setStripeId($subscription['customer']);
        
		$this->stripe_subscription = $subscription['id'];

		$end_date = Carbon::createFromTimestamp($subscription['current_period_end'])->toDateTimeString();

		// $this->setSubscriptionEndDate($end_date);
		// $this->setNextBillingDate($end_date);
		// $this->next_bill_date = $end_date;
        

        if($plan) {
            $this->setStripePlan($plan);
			$this->stripe_plan = $plan;
		}

        $this->save();

        return $subscription;
    }

    public function chargeCustomer($price = 50.00){
        $stripe = Stripe::make(env('STRIPE_SECRET'));

        try {
	        $charge = $stripe->charges()->create([
                'customer' => $this->getStripeId(),
                'currency' => 'USD',
                'amount'   => $price,
            ]);
		} catch (\Exception $e) {
			$message = $e->getMessage();
			return ['error' => $message ];
        }
        
        return ['id' => $charge['id']];
    }

    /**
     * createInvoiceAndPay
     *
     * @param  mixed $data
     * $user->createInvoiceAndPay(array('amount' => $price, 'description' => "JiveMo Package "));
     *
     * @return void
     */
    public function createInvoiceAndPay($data){
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        $default = [
            'currency'          => 'usd',
        ];

        $data = array_merge($default, $data);
        try{
            $item = $stripe->InvoiceItems()->create($this->getStripeId(), $data);
            
            //create invoice 
            // cus_EJFbkcfXGigSSg
            $invoice = $stripe->Invoices()->create($this->getStripeId(), ['tax_percent' => $this->getTaxPercent()]);
            //pay
            $pay = $stripe->Invoices()->pay($invoice['id']);
            return $pay;
            
        }catch(\Exception $e){
            return array("error" => $e->getMessage());
        }
        
    }
    public function createInvoice($data){
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        $default = [
            'currency'          => 'usd',
        ];

        $data = array_merge($default, $data);
        try{
            $item = $stripe->InvoiceItems()->create($this->getStripeId(), $data);
            
            //create invoice 
            // cus_EJFbkcfXGigSSg
            return $stripe->Invoices()->create($this->getStripeId(), ['tax_percent' => $this->getTaxPercent()]);
            
            
        }catch(\Exception $e){
            return array("error" => $e->getMessage());
        }
        
    }

    /**
     * getInvoiceList
     *
     * @return void
     */
    public function getInvoiceList(){
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        return $stripe->Invoices()->all();
    }

    /**
     * get all and single stripe cards info
     *
     * @param null $first_card
     * @return mixed
     */
    public function getStripeCards($first_card=null){
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        $cards = $stripe->cards()->all($this->stripe_id);

        if($first_card) {
            return (isset($cards['data'][0]))?$cards['data'][0]:NULL;
		}

        return $cards['data'];
    }

    /**
     * create stripe card and return card object
     * @param $token
     * @return mixed
     */
    public function createStripeCard($token){
        $stripe = Stripe::make(env('STRIPE_SECRET'));

        $card = $stripe->cards()->create(
            $this->getStripeId() , $token
        );

        if(!$card) return false;

        $this->setLastFourCardDigits($card['last4']);

        return $card;
    }

    /**
     * delete stripe card
     * @param $card
     * @return mixed
     */
    public function deleteStripeCard($card){
        $stripe = Stripe::make(env('STRIPE_SECRET'));
        $card = $stripe->cards()->delete($this->stripe_id, $card);
        return $card;
    }
    /**
     * change stripe card
     * get previous first_card,
     * crate new card
     * and and then delete previous card
     *
     * @param $token
     */
    public function changeStripeCard($token){
        try {
            $first_cad = $this->getStripeCards(true);
            if( $this->createStripeCard($token) ){
                $this->deleteStripeCard($first_cad['id']);
                $this->save();
            }
		} catch (\CardErrorException $e) {
			return array("error" => $e->getMessage());
		} catch (\Exception $e) {
			return array("error" => $e->getMessage());
        }
    }
    /**
     * getStirpeCard
     *
     * @return array
     */
    public function getStirpeCard(){
        if( !$this->getStripeId()) return array();
        $validCards	= array();
        $stripe 	= Stripe::make(env('STRIPE_SECRET'));
        $cards 		= $stripe->cards()->all($this->getStripeId());
        
        if (is_array($cards['data'])) {
            foreach ($cards['data'] as $index => $cardData) {
                if ('pass' != $cardData['cvc_check']) { // cvc check failed
                    continue;
                }
                $expMonth	= $cardData['exp_month'];
                $expYear	= $cardData['exp_year'];
                $cardTime	= strtotime($expYear.'-'.$expMonth.'-01');
                $nowTime	= strtotime(date('Y-m-01'));
                if ($cardTime <= $nowTime) { // card expired
                    continue;
                }
                $i 			= count($validCards);
                $validCards[$i]['id'] 			= $cardData['id'];
                $validCards[$i]['last4'] 		= $cardData['last4'];
                $validCards[$i]['expireMonth'] 	= $cardData['exp_month'];
                $validCards[$i]['expireYear'] 	= $cardData['exp_year'];
                $validCards[$i]['fingerprint'] 	= $cardData['fingerprint'];
                $validCards[$i]['brand'] 		= $cardData['brand'];
                $validCards[$i]['name'] 		= $cardData['name'];
            }
        }
        
        return $validCards;
    }
}
