<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    /**
     * The event map for the model.
     *
     * @var array
     */
    protected $dispatchesEvents = [
        // 'created' => TransactionObserver::class,
    ];

    protected $fillable = ['created_at','user_id', 'foreign_id', 'class', 'transaction_type_id', 'amount','description', 'admin_notes','payout', 'connection_id', 'payout_id', 'invoice_id' ];

    public function User(){
        return $this->belongsTo("App\User", 'user_id');
    }

    public function TransactionType(){
        return $this->belongsTo("App\TransactionType", "transaction_type_id");
    }

    public function Invoice(){
        return $this->belongsTo("App\Invoice", "invoice_id");
    }

    public function Payout(){
        return $this->belongsTo("App\Payouts", "payout_id");
    }

    public function Connection(){
        return $this->belongsTo("App\Connection", "connection_id");
    }

    /**
     * return Message(direct)
     * class DiscussionMessage
     * foreign_id = Message->id
     */
    public function Message(){
        return $this->belongsTo("App\Message", 'foreign_id');
    }
}
