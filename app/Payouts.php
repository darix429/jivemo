<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Payouts extends Model
{
    protected $fillable = ['id','user_id', 'invoice_no', 'amount', 'income', 'fee', 'status','remarks', 'remember_token', 'created_at', 'updated_at',];


    /**
      * Get all of the user
     */
    public function User()
    {
        return $this->belongsTo('App\User','user_id');
    }


}
