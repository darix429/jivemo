<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Settings extends Model
{
    protected $fillable  = ['page_title', 'page_description', 'page_metatag', 'copyright', 'logo', 'admin_logo', 'msg_fee', 'connection_fee','processing_fee_percent','admin_phone', 'admin_phone2', 'created_at', 'updated_at'];
}
