<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['user_id', 'reported_user_id', 'message'];

    public function User(){
        return $this->belongsTo('App\User', 'reported_user_id');
    }
}
