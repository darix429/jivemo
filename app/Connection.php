<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Message;
use App\Payouts;
use App\Settings;

class Connection extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_id','phone','seller_id', 'package_id', 'is_active','deleted_at', 'invoice_id','invoice_status','invoice_error', 'customer_id', 'next_payment_date', 'status', 'price'
    ];

    //user
    public function User(){
        return $this->belongsTo('App\User');
    }

    //package
    public function Package(){
        return $this->belongsTo('App\Package', 'package_id');
    }
    
    /**
     * return relation of connected user info
     */
    public function Seller(){
        return $this->belongsTo('App\User', 'seller_id');
    }

    /**
     * return relation of connected user info
     */
    public function UserReport()
    {
        return $this->hasOne('App\Report', 'user_id'); // links this->id to events.course_id
    }

    /**
     * return all message for this connection
     */
    public function Messages(){
        return $this->hasMany('App\Message');
    }
    function getFirstMsgCreatedAttribute(){
        $first_msg = $this->hasMany('App\Message')->orderBy('id', 'asc')->select('created_at')->first();
        if( $first_msg) return $first_msg->created_at;
        return '';
    }

    function getLatestMsgCreatedAttribute(){
        $first_msg = $this->hasMany('App\Message')->orderBy('id', 'desc')->select('created_at')->first();
        if( $first_msg) return $first_msg->created_at;
        return '';
    }
    
    static function GetConnectionByPhone($phone){
        return Connection::where('phone', '=', $phone)->first();
    }

    public function phone_logs($from, $message){
        if( $from == "buyer"){
            $from = $this->user_id;
            $to = $this->seller_id;
        }else{
            $to = $this->user_id;
            $from = $this->seller_id;
        }
        //phone log 
        PhoneLogs::create([
            'from'      => $from, #admin user id
            'to'        => $to,
            'message'   => $message,
        ]);
    }

    public function getPayoutDetails() {
        $settings = Settings::find(1);
        $package = Package::where("id",$this->package_id)->first();
        $subscriber = User::where("id",$this->user_id)->first();
        $messages = Message::where("connection_id", $this->id)->get();
        
        $createDate = strtotime($this->created_at);
        $dateFrom = strtotime(implode("-",[date("Y",$createDate),date("m",$createDate),"05"]));
        $dateTo = strtotime("+1 month", $dateFrom);

        if(date("d",$createDate) <= 5 ) {
            $dateTo = $dateFrom;
            $dateFrom = strtotime("-1 month", $dateTo);
        }
        $dateFrom = date("Y-m-d", $dateFrom);
        $dateTo = date("Y-m-d", $dateTo);
        

        $packageSubscribers = Connection::where("package_id", $this->package_id)
                                ->whereBetween("created_at", [$dateFrom, $dateTo]);

        // $connectionFee = config('site_setting.connection_fee', config('jivemo.PhoneConnectonFee')) * $packageSubscribers->count();
        // $messageFee = config('site_setting.msg_fee', config('jivemo.MessageFee')) * ($package?$package->no_of_message:1);
        // $connectionFee = $settings->connection_fee * $packageSubscribers->count();
        $connectionFee = $settings->connection_fee;
        $messageFee = $settings->msg_fee * ($package?$package->no_of_message:1);
        $processFee = (config('site_setting.processing_fee_percent', config('jivemo.ProcessingFee'))/100) * ($package?$package->fee:1);

        // $revenue = ($package?$package->fee:1) * $packageSubscribers->count();
        $revenue = ($package?$package->fee:1);
        $fee = $connectionFee + $messageFee + $processFee;
        $income = $revenue - $fee;

        $charge_date = strtotime($this->next_payment_date);
        $charge_date = date("Y-m-d",strtotime("-1 month", $charge_date));
        $lastTransaction = Transaction::where("connection_id", $this->id)
                            ->orderBy("created_at", "DESC")
                            ->first();
        if ($lastTransaction->count() > 0) {
            $charge_date = date("Y-m-d", strtotime($lastTransaction->created_at));
        }

        return array(
            "date_from"=>$dateFrom,
            "date_to"=>$dateTo,
            "subscriber" => ($subscriber? $subscriber->username : "Unknown"),
            "package" => $package? $package->name : "Unknown",
            "charge_date" => $charge_date,
            "revenue" => $revenue,
            "fee" => $fee,
            "income" => $income,
            "connection_fee" => $connectionFee,
            "message_fee" => $messageFee,
            "process_fee" => $processFee
        );
    }
}
