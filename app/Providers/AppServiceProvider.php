<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Database\QueryException;
use Auth;
use App\Settings;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->share('auth_user', Auth::user());

        try{
            \Config::set('site_setting', Settings::find(1));
        }catch(QueryException $e){

        }
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
